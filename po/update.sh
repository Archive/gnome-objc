#!/bin/sh

xgettext --default-domain=gnome-objc --directory=.. \
  --add-comments --keyword=_ --keyword=N_ \
  --files-from=./POTFILES.in \
&& test ! -f gnome-objc.po \
   || ( rm -f ./gnome-objc.pot \
    && mv gnome-objc.po ./gnome-objc.pot )
