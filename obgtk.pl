#!/usr/bin/perl -w
## Script to write Objective C wrappers to Gtk/Gnome API
## by Richard Hestilow <hestgray@ionet.net>
## subs:
# get_arg_list -- Splits up a function into a list of alternating type/name
## vars used: 
# $pkg 	-- Usually Gtk or Gnome 
# $class_name -- widget name (Box, button, etc)
# $class -- GtkButton, for example. 
# $header1, $header2 --  the two possible header files
# $func_prefix 	-- the standard prefix used for C member functions
# SOURCE -- the .m implementation file
# HEADER -- the .h declaration file
# $istart -- first argument of non-constructor gtk member functions is
# 	the widget to operate on. We want to skip this in objc member decls
# 	so we set $istart to the element number ahead of it
# %funcs -- member functions (key is func name, value is 1)
# @args -- used in main loop for arguments of function
# @widgets -- gtk widgets referenced in function params
# @mywidgets -- obgtk widgets referenced in member params

if (scalar(@ARGV) < 2) {
  die "Not enough args! invoke as obgtk.pl Pkg PkgBase ... Widget
	Example: obgtk.pl Gnome Gtk Button\n"
}
@bases = ();
%widgets = ();

$parent = "Object";
$pkg = $ARGV[0];
push @bases, $pkg;
for($i=2;$i<scalar(@ARGV)-1;$i++) {
  push @bases, $ARGV[$i];
}

$class_name = $ARGV[$i]; 

$class = $pkg . $class_name; 
# $header = lc($class); # for gtk: gtkbutton.h

chomp($header = `basename $ARGV[1] .h`);
$header2 = lc( $pkg . "-" . $class_name); # for gnome: gnome-dialog.h
*INHEADER=*STDIN;

$func_prefix = get_func_prefix($pkg, $class_name);

# reads in header, grabs the parent class and the method functions
print "$func_prefix\n";

READLOOP:
while ($line = <INHEADER>) {
  chomp $line;

#  if($line =~ /parent_class/) {
#    print "The line is: ".$line."\n";
#  } else {
#    print "The line is not: ".$line."\n";
#  }

  if($line =~ /^\#/) {
    while(<INHEADER> =~ /\\$/) { }
    next;
  }

  if($line =~ /$pkg(\w+)\s+parent_class/) {
    $cn = $1;
    if ($cn =~ /(\w+)Class/) {
      $parent = $1;
    } 
  } elsif ($line =~ /^\/\*/) {
    while (not $line =~ /\*\//) {
      $line = <INHEADER>;
      if (not defined($line)) { last READLOOP;}
      chomp $line;
    }
  } elsif ($line =~ /$func_prefix/) {
    $line =~ s/\/\*.*\*\///g;
    while (not $line =~ /\)\;/) {
      $line .= <INHEADER>; 
      $line =~ s/\/\*.*\*\///g;
      chomp $line;
    }
    $line =~ s/\s+/ /g;
    $tmp = $line;
    foreach $base (@bases) {
      while ($tmp =~ s/$base(\w+)//) {
	if (-e ("../ob" . lc($base) . "/ob" . lc($base)
		. $1 . ".h") and
	    not $1 eq $class_name and 
	    not grep /$1/, keys %widgets) {
	  $widgets{$1} = $base;
	}
      }
    }
    $funcs{$line} = 1;
  }
}
close INHEADER;

# time to write our files
open(HEADER, ">ob" . lc($pkg) . $class_name . ".h") or die;
open(SOURCE, ">ob" . lc($pkg) . $class_name . ".m") or die;

$header_def = "OB" . uc($pkg) . "_" . uc($class_name) . "_H";  # OBGTK_BUTTON_H

# some random gunk we can figure out on our own
print HEADER "#ifndef $header_def\n";
print HEADER "#define $header_def 1\n";

foreach $widget (keys %widgets) {
  print HEADER "#include \<ob" . lc($widgets{$widget}) . "/" . "ob" .
    lc($widgets{$widget}) .	"$widget.h\>\n";
}

print HEADER "#include \<ob" . lc($pkg) . "/" . "ob" . lc($pkg) . 
  "$parent.h\>\n";

if ($pkg eq 'Gtk') {
  print HEADER "#include \<" . lc($pkg) . "/$header.h\>\n";
} else {
  print HEADER "#include \<lib" . lc($pkg) . "ui/$header2.h\>\n";
}

print HEADER "\n\@interface $pkg\_$class_name : $pkg\_$parent\n";
print HEADER "{\n";
print HEADER "\@public\n";
print HEADER "  $class \*" . lc($class) . ";\n";
print HEADER "}\n";
print HEADER "- cast$class : ($class *) castitem;\n";
print HEADER "+ (id)make$class : ($class *) castitem;\n";


print SOURCE "#include \"ob" . lc($pkg) . $class_name . ".h\"\n";
print SOURCE "\n\@implementation $pkg\_$class_name\n";
print SOURCE "\n- cast$class : ($class *) castitem\n";
print SOURCE "{\n";
print SOURCE "  " . lc($class) . " = castitem;\n";
print SOURCE "  return [super cast$pkg$parent:" . uc(get_func_prefix($pkg, $parent)) .
  "(castitem)];\n";
print SOURCE "}\n\n";
print SOURCE "+ (id) make$class : ($class *) castitem\n";
print SOURCE "{\n  id retval;\n";
print SOURCE "  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];\n";
print SOURCE "  if(!retval && castitem)\n    retval = [[self alloc] cast$class:castitem];\n\n";
print SOURCE "  return retval;\n}\n\n";

$fn="ob".lc($pkg).$class_name.".inc";
if(-e $fn) {
  print SOURCE `cat $fn`;
  print SOURCE "\n";
}

# the gunk stops here

# the main loop
foreach $func (keys %funcs) {
  $varargs = 0;
#  print "Objectivizing $func...\n";
  @args =	get_arg_list($func);
  if ($args[1] eq $func_prefix . "_get_type") {
    next; # internal Gtk/Gnome use only
  }
  
  if (scalar(@args) > 2) {
    if ((not $args[2] eq $class . ' *') and 
	(not $args[1] =~ /new/)) {
      next;
    }
  }
  
  foreach $arg (@args) {
    if ($arg eq '...') {
      $varargs = 1;
      last;
    }
  }
  
  if ($varargs) {
    next;
  }

# the member function prototype
  $rettype = $args[0];
  print HEADER "- ";
  print SOURCE "- ";
  if (not $rettype eq "void" and not $args[1] =~ /_new/) {
    $foo = $pkg."_";
    if($rettype =~ /^$foo(\w+)/
      && $widgets{$1}) {
      $rettype = "id";
    }
    print HEADER "(" . $rettype . ") ";
    print SOURCE "(" . $rettype . ") ";
  }
  $stuff = $func_prefix . '_';				       # strips
  $tmp = $args[1];					       # function prefix
  $tmp =~ s/$stuff//;
  $pass_widget = 1;
  if ($tmp =~ /new/) {					       # constructor?
# capitalize, turn new into init
    $tmp =~ s/new//;
    if ($tmp =~ /_\w/) { 
      $tmp =~ s/\_(\w)/uc($1)/eg;
      print HEADER "init" . $tmp;
      print SOURCE "init" . $tmp;
    } elsif ($args[3] && $args[3] ne "void") {
      printf HEADER "initWith%sInfo", $class_name;
      printf SOURCE "initWith%sInfo", $class_name;
    } else {
      print HEADER "init";
      print SOURCE "init";
    }
    $istart = 2;       # constructors don't take widget arg
    $pass_widget=0;
  } else {
    print HEADER $tmp;
    print SOURCE $tmp;
    $istart = 4;     # everything else does
  }
  
# if there are arguments, and its not something like foo(void)
  if (scalar(@args) > $istart and not $args[2] eq 'void') {
# argument list
    for($i=$istart;$i<scalar(@args);$i+=2) {
      print HEADER "\n : (";
      print SOURCE "\n : (";
      $re = quotemeta($args[$i]);
      if (grep /$re/, @mywidgets) {
# if we wrap something, we dynamically type
# it in the function prototypes
	print HEADER "id";
	print SOURCE "id";
      } else {
	print HEADER $args[$i];
	print SOURCE $args[$i];
      }
      print HEADER ") " . $args[$i+1];
      print SOURCE ") " . $args[$i+1];
    }
    print HEADER ";\n";
  } else { print HEADER ";\n";}
  
# implementation
  print SOURCE "\n{\n";
  if (scalar(@args) > 2) {
    for($i=2;$i<scalar(@args);$i++) {
      $re = quotemeta($args[$i]);
      if (grep /$re/, @mywidgets) {
	print SOURCE "  g_return_val_if_fail([";
	print SOURCE $args[$i+1];
	print SOURCE " isKindOf: [";
	$tmp = $args[$i];
	$tmp =~ s/ \*//;
	print SOURCE "$tmp class]], ";
	print SOURCE "0";
	print SOURCE ");\n";
      }
    }
  }

  $need_return_close = undef;

  if ($args[1] =~ /new/) {				       # constructor?
    print SOURCE "  return [self cast$class:";
    print SOURCE uc($func_prefix) . "(";
  } elsif ($args[0] ne "void") {
    print SOURCE "  return ";
    $foo = $pkg."_";
    if($args[0] =~ /^$foo(\w+)/
      && $widgets{$1}) {
      $need_return_close = $1;

      print SOURCE "[[".$pkg."_".$need_return_close." class] make".$pkg.$need_return_close.":";
    }
  } else {
    print SOURCE "  "; # indent
  }

  print SOURCE $args[1] . "(";
  
# same list, but this time no types, just names
  if (scalar(@args) > 2 and $args[2] ne 'void') {
    if($pass_widget) {
      print SOURCE lc($class);
    }

    for($i=$istart;$i<scalar(@args);$i+=2) {
      if ($pass_widget
	  or ($i != $istart)) {
	print SOURCE ", ";
      }

      $re = quotemeta($args[$i]);

      if (grep /$re/, @mywidgets) {
# did we wrap it? we need to send 
# the gtk widget member
	print SOURCE "((";
	print SOURCE $args[$i];
	print SOURCE ")"; 
	print SOURCE $args[$i+1];
	print SOURCE ")->";
	$tmp = $args[$i];
	$tmp =~ s/[ \_\*]//g;
	print SOURCE lc($tmp);
      } else {
	print SOURCE $args[$i+1];
      }
    }
  }
  print SOURCE ")"; 
  if ($args[1] =~ /new/) {				       # for the GTK_FOO(blah_new()) wrapper
    print SOURCE ")";
    print SOURCE "]";
  } elsif($need_return_close) {
    print SOURCE "]";
  }
  print SOURCE ";\n";
  
  if ($rettype eq 'void') {
    print SOURCE "  return self;\n";			       # used in objc for
							       # nesting
  }
  print SOURCE "}\n\n";
}

print HEADER "\@end\n";
print HEADER "\n#endif /* $header_def */";
close HEADER;
print SOURCE "\@end\n";
close SOURCE;

exit 0;
# El fin


# returns list: 1st element return type, then name (properly formatted), 
# then for each param: type, then name
sub get_arg_list() {
  my($line, @garbage) = @_;
  my @retval;
  my $token;
# evil, but it works. We pretend
# it is a C string so we can iterate easily
  my @string = split //, $line;
  my $i;
  
# iterates through, splits on (, ), ",", and space 
# (except for const, static, etc modifiers)
  for($i=0;$i<scalar(@string)-2;$i++) { 
    if ($string[$i] eq '(') { next; }
    if (not $string[$i] eq ' ') { $token .= $string[$i];}
    if ($string[$i+1] =~ /[\(\)\, ]/) { 
      if (not $token eq "const") {
# is this type a pointer? 
	if ($string[$i+2] eq '*') { 
	  while ($string[$i+2] eq '*') {
	    $token .= ' *'; $i++;
	  }
	}
	if($token =~ /$pkg(\w+)/) {
	  $tmp = $1;
	  # Is this a widget we have wrapped? 
	  if (grep (/$tmp/, keys %widgets)) {
	    $token = $widgets{$tmp} . "_" . $tmp . " *";
	    push @mywidgets, $token;
	  }
	}

	if($token =~ /\[\]$/) {
	  $token =~ s/\[\]$//;
	  $retval[scalar(@retval) - 1] .= "*"; 
	}
	push @retval, $token;
	$token = "";
	$i++;
      } else {
	$token .= ' ';
      }
    } 
  }
  return @retval;
}

sub get_func_prefix {
  my($pkg, $class_name) = @_;
  my $prefix = lc($pkg);
  $class_name =~ s/([A-Z]+)/\_$1/g;
  $prefix .= lc($class_name);
  return $prefix;
}
