#ifndef OBGTK_ACCELLABEL_H
#define OBGTK_ACCELLABEL_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkLabel.h>
#include <gtk/gtkaccellabel.h>

@interface Gtk_AccelLabel : Gtk_Label
{
@public
  GtkAccelLabel *gtkaccellabel;
}
- castGtkAccelLabel : (GtkAccelLabel *) castitem;
+ (id)makeGtkAccelLabel : (GtkAccelLabel *) castitem;
- set_accel_widget
 : (id) accel_widget;
- initWithAccelLabelInfo
 : (const gchar *) string;
- (guint) get_accel_width;
- (gboolean) refetch;
@end

#endif /* OBGTK_ACCELLABEL_H */
