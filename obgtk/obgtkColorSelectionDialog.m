#include "obgtkColorSelectionDialog.h"

@implementation Gtk_ColorSelectionDialog

- castGtkColorSelectionDialog : (GtkColorSelectionDialog *) castitem
{
  gtkcolorselectiondialog = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkColorSelectionDialog : (GtkColorSelectionDialog *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkColorSelectionDialog:castitem];

  return retval;
}

- initWithColorSelectionDialogInfo
 : (const gchar *) title
{
  return [self castGtkColorSelectionDialog:GTK_COLOR_SELECTION_DIALOG(gtk_color_selection_dialog_new(title))];
}

@end
