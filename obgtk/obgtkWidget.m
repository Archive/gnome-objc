#include "obgtkWidget.h"

@implementation Gtk_Widget

- castGtkWidget : (GtkWidget *) castitem
{
  gtkwidget = castitem;
  return [super castGtkObject:GTK_OBJECT(castitem)];
}

+ (id) makeGtkWidget : (GtkWidget *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkWidget:castitem];

  return retval;
}

- ensure_style
{
  gtk_widget_ensure_style(gtkwidget);
  return self;
}

- unlock_accelerators
{
  gtk_widget_unlock_accelerators(gtkwidget);
  return self;
}

- queue_clear_area
 : (gint) x
 : (gint) y
 : (gint) width
 : (gint) height
{
  gtk_widget_queue_clear_area(gtkwidget, x, y, width, height);
  return self;
}

- reset_shapes
{
  gtk_widget_reset_shapes(gtkwidget);
  return self;
}

- draw
 : (GdkRectangle *) area
{
  gtk_widget_draw(gtkwidget, area);
  return self;
}

- (GtkWidget*) get_toplevel
{
  return gtk_widget_get_toplevel(gtkwidget);
}

- (guint) accelerator_signal
 : (id) accel_group
 : (guint) accel_key
 : (guint) accel_mods
{
  g_return_val_if_fail([accel_group isKindOf: [Gtk_AccelGroup class]], 0);
  return gtk_widget_accelerator_signal(gtkwidget, ((Gtk_AccelGroup *)accel_group)->gtkaccelgroup, accel_key, accel_mods);
}

- realize
{
  gtk_widget_realize(gtkwidget);
  return self;
}

- draw_focus
{
  gtk_widget_draw_focus(gtkwidget);
  return self;
}

- unrealize
{
  gtk_widget_unrealize(gtkwidget);
  return self;
}

- class_path
 : (guint *) path_length
 : (gchar * *) path
 : (gchar * *) path_reversed
{
  gtk_widget_class_path(gtkwidget, path_length, path, path_reversed);
  return self;
}

- add_events
 : (gint) events
{
  gtk_widget_add_events(gtkwidget, events);
  return self;
}

- set_usize
 : (gint) width
 : (gint) height
{
  gtk_widget_set_usize(gtkwidget, width, height);
  return self;
}

- set_parent_window
 : (GdkWindow *) parent_window
{
  gtk_widget_set_parent_window(gtkwidget, parent_window);
  return self;
}

- set_visual
 : (GdkVisual *) visual
{
  gtk_widget_set_visual(gtkwidget, visual);
  return self;
}

- (gint) get_events
{
  return gtk_widget_get_events(gtkwidget);
}

- reparent
 : (GtkWidget *) new_parent
{
  gtk_widget_reparent(gtkwidget, new_parent);
  return self;
}

- (GdkVisual*) get_visual
{
  return gtk_widget_get_visual(gtkwidget);
}

- show_all
{
  gtk_widget_show_all(gtkwidget);
  return self;
}

- popup
 : (gint) x
 : (gint) y
{
  gtk_widget_popup(gtkwidget, x, y);
  return self;
}

- show
{
  gtk_widget_show(gtkwidget);
  return self;
}

- destroyed
 : (GtkWidget * *) widget_pointer
{
  gtk_widget_destroyed(gtkwidget, widget_pointer);
  return self;
}

- set_state
 : (GtkStateType) state
{
  gtk_widget_set_state(gtkwidget, state);
  return self;
}

- (gint) intersect
 : (GdkRectangle *) area
 : (GdkRectangle *) intersection
{
  return gtk_widget_intersect(gtkwidget, area, intersection);
}

- set_extension_events
 : (GdkExtensionMode) mode
{
  gtk_widget_set_extension_events(gtkwidget, mode);
  return self;
}

- getv
 : (guint) nargs
 : (GtkArg *) args
{
  gtk_widget_getv(gtkwidget, nargs, args);
  return self;
}

- remove_accelerator
 : (id) accel_group
 : (guint) accel_key
 : (guint) accel_mods
{
  g_return_val_if_fail([accel_group isKindOf: [Gtk_AccelGroup class]], 0);
  gtk_widget_remove_accelerator(gtkwidget, ((Gtk_AccelGroup *)accel_group)->gtkaccelgroup, accel_key, accel_mods);
  return self;
}

- queue_clear
{
  gtk_widget_queue_clear(gtkwidget);
  return self;
}

- set_colormap
 : (GdkColormap *) colormap
{
  gtk_widget_set_colormap(gtkwidget, colormap);
  return self;
}

- set_name
 : (const gchar *) name
{
  gtk_widget_set_name(gtkwidget, name);
  return self;
}

- get
 : (GtkArg *) arg
{
  gtk_widget_get(gtkwidget, arg);
  return self;
}

- (gint) event
 : (GdkEvent *) event
{
  return gtk_widget_event(gtkwidget, event);
}

- queue_draw
{
  gtk_widget_queue_draw(gtkwidget);
  return self;
}

- set_events
 : (gint) events
{
  gtk_widget_set_events(gtkwidget, events);
  return self;
}

- setv
 : (guint) nargs
 : (GtkArg *) args
{
  gtk_widget_setv(gtkwidget, nargs, args);
  return self;
}

- show_now
{
  gtk_widget_show_now(gtkwidget);
  return self;
}

- add_accelerator
 : (const gchar *) accel_signal
 : (id) accel_group
 : (guint) accel_key
 : (guint) accel_mods
 : (GtkAccelFlags) accel_flags
{
  g_return_val_if_fail([accel_group isKindOf: [Gtk_AccelGroup class]], 0);
  gtk_widget_add_accelerator(gtkwidget, accel_signal, ((Gtk_AccelGroup *)accel_group)->gtkaccelgroup, accel_key, accel_mods, accel_flags);
  return self;
}

- (gint) is_ancestor
 : (GtkWidget *) ancestor
{
  return gtk_widget_is_ancestor(gtkwidget, ancestor);
}

- reset_rc_styles
{
  gtk_widget_reset_rc_styles(gtkwidget);
  return self;
}

- set_composite_name
 : (gchar *) name
{
  gtk_widget_set_composite_name(gtkwidget, name);
  return self;
}

- set_app_paintable
 : (gboolean) app_paintable
{
  gtk_widget_set_app_paintable(gtkwidget, app_paintable);
  return self;
}

- (GtkStyle*) get_style
{
  return gtk_widget_get_style(gtkwidget);
}

- grab_default
{
  gtk_widget_grab_default(gtkwidget);
  return self;
}

- lock_accelerators
{
  gtk_widget_lock_accelerators(gtkwidget);
  return self;
}

- (gboolean) activate
{
  return gtk_widget_activate(gtkwidget);
}

- (GdkWindow *) get_parent_window
{
  return gtk_widget_get_parent_window(gtkwidget);
}

- size_allocate
 : (GtkAllocation *) allocation
{
  gtk_widget_size_allocate(gtkwidget, allocation);
  return self;
}

- (GdkColormap*) get_colormap
{
  return gtk_widget_get_colormap(gtkwidget);
}

- (gint) hide_on_delete
{
  return gtk_widget_hide_on_delete(gtkwidget);
}

- destroy
{
  gtk_widget_destroy(gtkwidget);
  return self;
}

- queue_draw_area
 : (gint) x
 : (gint) y
 : (gint) width
 : (gint) height
{
  gtk_widget_queue_draw_area(gtkwidget, x, y, width, height);
  return self;
}

- set_rc_style
{
  gtk_widget_set_rc_style(gtkwidget);
  return self;
}

- hide_all
{
  gtk_widget_hide_all(gtkwidget);
  return self;
}

- (gboolean) set_scroll_adjustments
 : (id) hadjustment
 : (id) vadjustment
{
  g_return_val_if_fail([hadjustment isKindOf: [Gtk_Adjustment class]], 0);
  g_return_val_if_fail([vadjustment isKindOf: [Gtk_Adjustment class]], 0);
  return gtk_widget_set_scroll_adjustments(gtkwidget, ((Gtk_Adjustment *)hadjustment)->gtkadjustment, ((Gtk_Adjustment *)vadjustment)->gtkadjustment);
}

- set_sensitive
 : (gboolean) sensitive
{
  gtk_widget_set_sensitive(gtkwidget, sensitive);
  return self;
}

- shape_combine_mask
 : (GdkBitmap *) shape_mask
 : (gint) offset_x
 : (gint) offset_y
{
  gtk_widget_shape_combine_mask(gtkwidget, shape_mask, offset_x, offset_y);
  return self;
}

- queue_resize
{
  gtk_widget_queue_resize(gtkwidget);
  return self;
}

- (GdkExtensionMode) get_extension_events
{
  return gtk_widget_get_extension_events(gtkwidget);
}

- size_request
 : (GtkRequisition *) requisition
{
  gtk_widget_size_request(gtkwidget, requisition);
  return self;
}

- (GtkWidget*) get_ancestor
 : (GtkType) widget_type
{
  return gtk_widget_get_ancestor(gtkwidget, widget_type);
}

- hide
{
  gtk_widget_hide(gtkwidget);
  return self;
}

- grab_focus
{
  gtk_widget_grab_focus(gtkwidget);
  return self;
}

- (gchar*) get_name
{
  return gtk_widget_get_name(gtkwidget);
}

- map
{
  gtk_widget_map(gtkwidget);
  return self;
}

- get_pointer
 : (gint *) x
 : (gint *) y
{
  gtk_widget_get_pointer(gtkwidget, x, y);
  return self;
}

- unmap
{
  gtk_widget_unmap(gtkwidget);
  return self;
}

- set_uposition
 : (gint) x
 : (gint) y
{
  gtk_widget_set_uposition(gtkwidget, x, y);
  return self;
}

- initWithWidgetInfo
 : (GtkType) type
 : (guint) nargs
 : (GtkArg *) args
{
  return [self castGtkWidget:GTK_WIDGET(gtk_widget_newv(type, nargs, args))];
}

- modify_style
 : (GtkRcStyle *) style
{
  gtk_widget_modify_style(gtkwidget, style);
  return self;
}

- unparent
{
  gtk_widget_unparent(gtkwidget);
  return self;
}

- (gchar*) get_composite_name
{
  return gtk_widget_get_composite_name(gtkwidget);
}

- get_child_requisition
 : (GtkRequisition *) requisition
{
  gtk_widget_get_child_requisition(gtkwidget, requisition);
  return self;
}

- draw_default
{
  gtk_widget_draw_default(gtkwidget);
  return self;
}

- restore_default_style
{
  gtk_widget_restore_default_style(gtkwidget);
  return self;
}

- set_style
 : (GtkStyle *) style
{
  gtk_widget_set_style(gtkwidget, style);
  return self;
}

- path
 : (guint *) path_length
 : (gchar * *) path
 : (gchar * *) path_reversed
{
  gtk_widget_path(gtkwidget, path_length, path, path_reversed);
  return self;
}

- ref
{
  gtk_widget_ref(gtkwidget);
  return self;
}

- remove_accelerators
 : (const gchar *) accel_signal
 : (gboolean) visible_only
{
  gtk_widget_remove_accelerators(gtkwidget, accel_signal, visible_only);
  return self;
}

- (gboolean) accelerators_locked
{
  return gtk_widget_accelerators_locked(gtkwidget);
}

- set_parent
 : (GtkWidget *) parent
{
  gtk_widget_set_parent(gtkwidget, parent);
  return self;
}

- unref
{
  gtk_widget_unref(gtkwidget);
  return self;
}

@end
