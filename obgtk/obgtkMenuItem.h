#ifndef OBGTK_MENUITEM_H
#define OBGTK_MENUITEM_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkItem.h>
#include <gtk/gtkmenuitem.h>

@interface Gtk_MenuItem : Gtk_Item
{
@public
  GtkMenuItem *gtkmenuitem;
}
- castGtkMenuItem : (GtkMenuItem *) castitem;
+ (id)makeGtkMenuItem : (GtkMenuItem *) castitem;
- initWithLabel
 : (const gchar *) label;
- configure
 : (gint) show_toggle_indicator
 : (gint) show_submenu_indicator;
- set_placement
 : (GtkSubmenuPlacement) placement;
- init;
- deselect;
- set_submenu
 : (id) submenu;
- right_justify;
- remove_submenu;
- select;
- activate;
@end

#endif /* OBGTK_MENUITEM_H */
