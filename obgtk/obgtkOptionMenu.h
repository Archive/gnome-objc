#ifndef OBGTK_OPTIONMENU_H
#define OBGTK_OPTIONMENU_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkButton.h>
#include <gtk/gtkoptionmenu.h>

@interface Gtk_OptionMenu : Gtk_Button
{
@public
  GtkOptionMenu *gtkoptionmenu;
}
- castGtkOptionMenu : (GtkOptionMenu *) castitem;
+ (id)makeGtkOptionMenu : (GtkOptionMenu *) castitem;
- (id) get_menu;
- set_history
 : (guint) index;
- remove_menu;
- init;
- set_menu
 : (id) menu;
@end

#endif /* OBGTK_OPTIONMENU_H */
