#include "obgtkTooltips.h"

@implementation Gtk_Tooltips

- castGtkTooltips : (GtkTooltips *) castitem
{
  gtktooltips = castitem;
  return [super castGtkData:GTK_DATA(castitem)];
}

+ (id) makeGtkTooltips : (GtkTooltips *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkTooltips:castitem];

  return retval;
}

- disable
{
  gtk_tooltips_disable(gtktooltips);
  return self;
}

- force_window
{
  gtk_tooltips_force_window(gtktooltips);
  return self;
}

- enable
{
  gtk_tooltips_enable(gtktooltips);
  return self;
}

- set_tip
 : (id) widget
 : (const gchar *) tip_text
 : (const gchar *) tip_private
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_tooltips_set_tip(gtktooltips, ((Gtk_Widget *)widget)->gtkwidget, tip_text, tip_private);
  return self;
}

- init
{
  return [self castGtkTooltips:GTK_TOOLTIPS(gtk_tooltips_new())];
}

- set_delay
 : (guint) delay
{
  gtk_tooltips_set_delay(gtktooltips, delay);
  return self;
}

- set_colors
 : (GdkColor *) background
 : (GdkColor *) foreground
{
  gtk_tooltips_set_colors(gtktooltips, background, foreground);
  return self;
}

@end
