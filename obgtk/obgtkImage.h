#ifndef OBGTK_IMAGE_H
#define OBGTK_IMAGE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkMisc.h>
#include <gtk/gtkimage.h>

@interface Gtk_Image : Gtk_Misc
{
@public
  GtkImage *gtkimage;
}
- castGtkImage : (GtkImage *) castitem;
+ (id)makeGtkImage : (GtkImage *) castitem;
- initWithImageInfo
 : (GdkImage *) val
 : (GdkBitmap *) mask;
- get
 : (GdkImage * *) val
 : (GdkBitmap * *) mask;
- set
 : (GdkImage *) val
 : (GdkBitmap *) mask;
@end

#endif /* OBGTK_IMAGE_H */
