#include "obgtkRange.h"

@implementation Gtk_Range

- castGtkRange : (GtkRange *) castitem
{
  gtkrange = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkRange : (GtkRange *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkRange:castitem];

  return retval;
}

- draw_step_back
{
  gtk_range_draw_step_back(gtkrange);
  return self;
}

- (id) get_adjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_range_get_adjustment(gtkrange)];
}

- (gint) default_vtrough_click
 : (gint) x
 : (gint) y
 : (gfloat *) jump_perc
{
  return gtk_range_default_vtrough_click(gtkrange, x, y, jump_perc);
}

- draw_background
{
  gtk_range_draw_background(gtkrange);
  return self;
}

- (gint) default_htrough_click
 : (gint) x
 : (gint) y
 : (gfloat *) jump_perc
{
  return gtk_range_default_htrough_click(gtkrange, x, y, jump_perc);
}

- default_vslider_update
{
  gtk_range_default_vslider_update(gtkrange);
  return self;
}

- draw_slider
{
  gtk_range_draw_slider(gtkrange);
  return self;
}

- default_vmotion
 : (gint) xdelta
 : (gint) ydelta
{
  gtk_range_default_vmotion(gtkrange, xdelta, ydelta);
  return self;
}

- default_hslider_update
{
  gtk_range_default_hslider_update(gtkrange);
  return self;
}

- default_hmotion
 : (gint) xdelta
 : (gint) ydelta
{
  gtk_range_default_hmotion(gtkrange, xdelta, ydelta);
  return self;
}

- (gint) trough_click
 : (gint) x
 : (gint) y
 : (gfloat *) jump_perc
{
  return gtk_range_trough_click(gtkrange, x, y, jump_perc);
}

- set_update_policy
 : (GtkUpdateType) policy
{
  gtk_range_set_update_policy(gtkrange, policy);
  return self;
}

- set_adjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_range_set_adjustment(gtkrange, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- slider_update
{
  gtk_range_slider_update(gtkrange);
  return self;
}

- draw_step_forw
{
  gtk_range_draw_step_forw(gtkrange);
  return self;
}

- clear_background
{
  gtk_range_clear_background(gtkrange);
  return self;
}

- draw_trough
{
  gtk_range_draw_trough(gtkrange);
  return self;
}

@end
