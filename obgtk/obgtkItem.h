#ifndef OBGTK_ITEM_H
#define OBGTK_ITEM_H 1
#include <obgtk/obgtkBin.h>
#include <gtk/gtkitem.h>

@interface Gtk_Item : Gtk_Bin
{
@public
  GtkItem *gtkitem;
}
- castGtkItem : (GtkItem *) castitem;
+ (id)makeGtkItem : (GtkItem *) castitem;
- deselect;
- toggle;
- select;
@end

#endif /* OBGTK_ITEM_H */
