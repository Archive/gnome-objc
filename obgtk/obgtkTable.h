#ifndef OBGTK_TABLE_H
#define OBGTK_TABLE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtktable.h>

@interface Gtk_Table : Gtk_Container
{
@public
  GtkTable *gtktable;
}
- castGtkTable : (GtkTable *) castitem;
+ (id)makeGtkTable : (GtkTable *) castitem;
- set_col_spacing
 : (guint) column
 : (guint) spacing;
- set_col_spacings
 : (guint) spacing;
- set_row_spacing
 : (guint) row
 : (guint) spacing;
- attach
 : (id) child
 : (guint) left_attach
 : (guint) right_attach
 : (guint) top_attach
 : (guint) bottom_attach
 : (GtkAttachOptions) xoptions
 : (GtkAttachOptions) yoptions
 : (guint) xpadding
 : (guint) ypadding;
- attach_defaults
 : (id) widget
 : (guint) left_attach
 : (guint) right_attach
 : (guint) top_attach
 : (guint) bottom_attach;
- initWithTableInfo
 : (guint) rows
 : (guint) columns
 : (gboolean) homogeneous;
- set_homogeneous
 : (gboolean) homogeneous;
- set_row_spacings
 : (guint) spacing;
- resize
 : (guint) rows
 : (guint) columns;
@end

#endif /* OBGTK_TABLE_H */
