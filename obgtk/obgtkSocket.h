#ifndef OBGTK_SOCKET_H
#define OBGTK_SOCKET_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtksocket.h>

@interface Gtk_Socket : Gtk_Container
{
@public
  GtkSocket *gtksocket;
}
- castGtkSocket : (GtkSocket *) castitem;
+ (id)makeGtkSocket : (GtkSocket *) castitem;
- init;
- steal
 : (guint32) wid;
@end

#endif /* OBGTK_SOCKET_H */
