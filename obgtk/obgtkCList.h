#ifndef OBGTK_CLIST_H
#define OBGTK_CLIST_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtkclist.h>

@interface Gtk_CList : Gtk_Container
{
@public
  GtkCList *gtkclist;
}
- castGtkCList : (GtkCList *) castitem;
+ (id)makeGtkCList : (GtkCList *) castitem;
- initWithCListInfo
 : (gint) columns;
- column_title_passive
 : (gint) column;
- column_title_active
 : (gint) column;
- set_sort_type
 : (GtkSortType) sort_type;
- set_cell_style
 : (gint) row
 : (gint) column
 : (GtkStyle *) style;
- set_selectable
 : (gint) row
 : (gboolean) selectable;
- set_column_width
 : (gint) column
 : (gint) width;
- set_column_min_width
 : (gint) column
 : (gint) min_width;
- set_column_justification
 : (gint) column
 : (GtkJustification) justification;
- set_sort_column
 : (gint) column;
- set_column_max_width
 : (gint) column
 : (gint) max_width;
- set_shadow_type
 : (GtkShadowType) type;
- (gint) get_text
 : (gint) row
 : (gint) column
 : (gchar * *) text;
- set_row_data_full
 : (gint) row
 : (gpointer) data
 : (GtkDestroyNotify) destroy;
- (id) get_vadjustment;
- (GtkVisibility) row_is_visible
 : (gint) row;
- set_selection_mode
 : (GtkSelectionMode) mode;
- (gchar *) get_column_title
 : (gint) column;
- moveto
 : (gint) row
 : (gint) column
 : (gfloat) row_align
 : (gfloat) col_align;
- set_pixtext
 : (gint) row
 : (gint) column
 : (const gchar *) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask;
- undo_selection;
- set_pixmap
 : (gint) row
 : (gint) column
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask;
- unselect_row
 : (gint) row
 : (gint) column;
- (gpointer) get_row_data
 : (gint) row;
- (gboolean) get_selectable
 : (gint) row;
- (id) get_column_widget
 : (gint) column;
- set_row_data
 : (gint) row
 : (gpointer) data;
- (GtkStyle *) get_cell_style
 : (gint) row
 : (gint) column;
- clear;
- set_column_auto_resize
 : (gint) column
 : (gboolean) auto_resize;
- set_column_widget
 : (gint) column
 : (id) widget;
- (gint) get_pixtext
 : (gint) row
 : (gint) column
 : (gchar * *) text
 : (guint8 *) spacing
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask;
- (gint) append
 : (gchar **) text;
- (gint) columns_autosize;
- select_all;
- set_text
 : (gint) row
 : (gint) column
 : (const gchar *) text;
- column_titles_hide;
- column_titles_passive;
- column_titles_active;
- set_auto_sort
 : (gboolean) auto_sort;
- freeze;
- column_titles_show;
- (id) get_hadjustment;
- set_column_resizeable
 : (gint) column
 : (gboolean) resizeable;
- set_foreground
 : (gint) row
 : (GdkColor *) color;
- set_row_height
 : (guint) height;
- set_background
 : (gint) row
 : (GdkColor *) color;
- set_shift
 : (gint) row
 : (gint) column
 : (gint) vertical
 : (gint) horizontal;
- construct
 : (gint) columns
 : (gchar **) titles;
- set_hadjustment
 : (id) adjustment;
- (GtkStyle *) get_row_style
 : (gint) row;
- set_column_visibility
 : (gint) column
 : (gboolean) visible;
- (gint) optimal_column_width
 : (gint) column;
- (gint) find_row_from_data
 : (gpointer) data;
- (gint) get_selection_info
 : (gint) x
 : (gint) y
 : (gint *) row
 : (gint *) column;
- swap_rows
 : (gint) row1
 : (gint) row2;
- thaw;
- (GtkCellType) get_cell_type
 : (gint) row
 : (gint) column;
- row_move
 : (gint) source_row
 : (gint) dest_row;
- select_row
 : (gint) row
 : (gint) column;
- set_vadjustment
 : (id) adjustment;
- unselect_all;
- set_button_actions
 : (guint) button
 : (guint8) button_actions;
- (gint) get_pixmap
 : (gint) row
 : (gint) column
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask;
- (gint) insert
 : (gint) row
 : (gchar **) text;
- remove
 : (gint) row;
- (gint) prepend
 : (gchar **) text;
- set_row_style
 : (gint) row
 : (GtkStyle *) style;
- sort;
- initWithTitles
 : (gint) columns
 : (gchar **) titles;
- set_column_title
 : (gint) column
 : (const gchar *) title;
- set_use_drag_icons
 : (gboolean) use_icons;
- set_compare_func
 : (GtkCListCompareFunc) cmp_func;
- set_reorderable
 : (gboolean) reorderable;
@end

#endif /* OBGTK_CLIST_H */
