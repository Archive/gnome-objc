#ifndef OBGTK_RADIOBUTTON_H
#define OBGTK_RADIOBUTTON_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkCheckButton.h>
#include <gtk/gtkradiobutton.h>

@interface Gtk_RadioButton : Gtk_CheckButton
{
@public
  GtkRadioButton *gtkradiobutton;
}
- castGtkRadioButton : (GtkRadioButton *) castitem;
+ (id)makeGtkRadioButton : (GtkRadioButton *) castitem;
- initWithLabel
 : (GSList *) group
 : (const gchar *) label;
- set_group
 : (GSList *) group;
- initWithLabelFromWidget
 : (GtkRadioButton *) group
 : (const gchar *) label;
- (GSList*) group;
- initWithRadioButtonInfo
 : (GSList *) group;
- initFromWidget
 : (GtkRadioButton *) group;
@end

#endif /* OBGTK_RADIOBUTTON_H */
