#include "obgtkCurve.h"

@implementation Gtk_Curve

- castGtkCurve : (GtkCurve *) castitem
{
  gtkcurve = castitem;
  return [super castGtkDrawingArea:GTK_DRAWING_AREA(castitem)];
}

+ (id) makeGtkCurve : (GtkCurve *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkCurve:castitem];

  return retval;
}

- get_vector
 : (int) veclen
 : (gfloat*) vector
{
  gtk_curve_get_vector(gtkcurve, veclen, vector);
  return self;
}

- set_gamma
 : (gfloat) gamma
{
  gtk_curve_set_gamma(gtkcurve, gamma);
  return self;
}

- reset
{
  gtk_curve_reset(gtkcurve);
  return self;
}

- set_curve_type
 : (GtkCurveType) type
{
  gtk_curve_set_curve_type(gtkcurve, type);
  return self;
}

- init
{
  return [self castGtkCurve:GTK_CURVE(gtk_curve_new())];
}

- set_vector
 : (int) veclen
 : (gfloat*) vector
{
  gtk_curve_set_vector(gtkcurve, veclen, vector);
  return self;
}

- set_range
 : (gfloat) min_x
 : (gfloat) max_x
 : (gfloat) min_y
 : (gfloat) max_y
{
  gtk_curve_set_range(gtkcurve, min_x, max_x, min_y, max_y);
  return self;
}

@end
