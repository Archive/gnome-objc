#ifndef OBGTK_BUTTONBOX_H
#define OBGTK_BUTTONBOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBox.h>
#include <gtk/gtkbbox.h>

@interface Gtk_ButtonBox : Gtk_Box
{
@public
  GtkButtonBox *gtkbuttonbox;
}
- castGtkButtonBox : (GtkButtonBox *) castitem;
+ (id)makeGtkButtonBox : (GtkButtonBox *) castitem;
- set_spacing
 : (gint) spacing;
- set_child_size
 : (gint) min_width
 : (gint) min_height;
- (gint) get_spacing;
- set_child_ipadding
 : (gint) ipad_x
 : (gint) ipad_y;
- (GtkButtonBoxStyle) get_layout;
- get_child_size
 : (gint *) min_width
 : (gint *) min_height;
- get_child_ipadding
 : (gint *) ipad_x
 : (gint *) ipad_y;
- set_layout
 : (GtkButtonBoxStyle) layout_style;
@end

#endif /* OBGTK_BUTTONBOX_H */
