#ifndef OBGTK_VRULER_H
#define OBGTK_VRULER_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkRuler.h>
#include <gtk/gtkvruler.h>

@interface Gtk_VRuler : Gtk_Ruler
{
@public
  GtkVRuler *gtkvruler;
}
- castGtkVRuler : (GtkVRuler *) castitem;
+ (id)makeGtkVRuler : (GtkVRuler *) castitem;
- init;
@end

#endif /* OBGTK_VRULER_H */
