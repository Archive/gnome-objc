#ifndef OBGTK_MENUSHELL_H
#define OBGTK_MENUSHELL_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtkmenushell.h>

@interface Gtk_MenuShell : Gtk_Container
{
@public
  GtkMenuShell *gtkmenushell;
}
- castGtkMenuShell : (GtkMenuShell *) castitem;
+ (id)makeGtkMenuShell : (GtkMenuShell *) castitem;
- deselect;
- deactivate;
- insert
 : (id) child
 : (gint) position;
- append
 : (id) child;
- activate_item
 : (id) menu_item
 : (gboolean) force_deactivate;
- select_item
 : (id) menu_item;
- prepend
 : (id) child;
@end

#endif /* OBGTK_MENUSHELL_H */
