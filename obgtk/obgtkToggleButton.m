#include "obgtkToggleButton.h"

@implementation Gtk_ToggleButton

- castGtkToggleButton : (GtkToggleButton *) castitem
{
  gtktogglebutton = castitem;
  return [super castGtkButton:GTK_BUTTON(castitem)];
}

+ (id) makeGtkToggleButton : (GtkToggleButton *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkToggleButton:castitem];

  return retval;
}

- toggled
{
  gtk_toggle_button_toggled(gtktogglebutton);
  return self;
}

- (gboolean) get_active
{
  return gtk_toggle_button_get_active(gtktogglebutton);
}

- set_active
 : (gboolean) is_active
{
  gtk_toggle_button_set_active(gtktogglebutton, is_active);
  return self;
}

- initWithLabel
 : (const gchar *) label
{
  return [self castGtkToggleButton:GTK_TOGGLE_BUTTON(gtk_toggle_button_new_with_label(label))];
}

- init
{
  return [self castGtkToggleButton:GTK_TOGGLE_BUTTON(gtk_toggle_button_new())];
}

- set_mode
 : (gboolean) draw_indicator
{
  gtk_toggle_button_set_mode(gtktogglebutton, draw_indicator);
  return self;
}

@end
