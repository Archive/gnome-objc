#include "obgtkOptionMenu.h"

@implementation Gtk_OptionMenu

- castGtkOptionMenu : (GtkOptionMenu *) castitem
{
  gtkoptionmenu = castitem;
  return [super castGtkButton:GTK_BUTTON(castitem)];
}

+ (id) makeGtkOptionMenu : (GtkOptionMenu *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkOptionMenu:castitem];

  return retval;
}

- (id) get_menu
{
  return [[Gtk_Widget class] makeGtkWidget:gtk_option_menu_get_menu(gtkoptionmenu)];
}

- set_history
 : (guint) index
{
  gtk_option_menu_set_history(gtkoptionmenu, index);
  return self;
}

- remove_menu
{
  gtk_option_menu_remove_menu(gtkoptionmenu);
  return self;
}

- init
{
  return [self castGtkOptionMenu:GTK_OPTION_MENU(gtk_option_menu_new())];
}

- set_menu
 : (id) menu
{
  g_return_val_if_fail([menu isKindOf: [Gtk_Widget class]], 0);
  gtk_option_menu_set_menu(gtkoptionmenu, ((Gtk_Widget *)menu)->gtkwidget);
  return self;
}

@end
