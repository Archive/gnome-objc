#include "obgtkCList.h"

@implementation Gtk_CList

- castGtkCList : (GtkCList *) castitem
{
  gtkclist = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkCList : (GtkCList *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkCList:castitem];

  return retval;
}

- initWithCListInfo
 : (gint) columns
{
  return [self castGtkCList:GTK_CLIST(gtk_clist_new(columns))];
}

- column_title_passive
 : (gint) column
{
  gtk_clist_column_title_passive(gtkclist, column);
  return self;
}

- column_title_active
 : (gint) column
{
  gtk_clist_column_title_active(gtkclist, column);
  return self;
}

- set_sort_type
 : (GtkSortType) sort_type
{
  gtk_clist_set_sort_type(gtkclist, sort_type);
  return self;
}

- set_cell_style
 : (gint) row
 : (gint) column
 : (GtkStyle *) style
{
  gtk_clist_set_cell_style(gtkclist, row, column, style);
  return self;
}

- set_selectable
 : (gint) row
 : (gboolean) selectable
{
  gtk_clist_set_selectable(gtkclist, row, selectable);
  return self;
}

- set_column_width
 : (gint) column
 : (gint) width
{
  gtk_clist_set_column_width(gtkclist, column, width);
  return self;
}

- set_column_min_width
 : (gint) column
 : (gint) min_width
{
  gtk_clist_set_column_min_width(gtkclist, column, min_width);
  return self;
}

- set_column_justification
 : (gint) column
 : (GtkJustification) justification
{
  gtk_clist_set_column_justification(gtkclist, column, justification);
  return self;
}

- set_sort_column
 : (gint) column
{
  gtk_clist_set_sort_column(gtkclist, column);
  return self;
}

- set_column_max_width
 : (gint) column
 : (gint) max_width
{
  gtk_clist_set_column_max_width(gtkclist, column, max_width);
  return self;
}

- set_shadow_type
 : (GtkShadowType) type
{
  gtk_clist_set_shadow_type(gtkclist, type);
  return self;
}

- (gint) get_text
 : (gint) row
 : (gint) column
 : (gchar * *) text
{
  return gtk_clist_get_text(gtkclist, row, column, text);
}

- set_row_data_full
 : (gint) row
 : (gpointer) data
 : (GtkDestroyNotify) destroy
{
  gtk_clist_set_row_data_full(gtkclist, row, data, destroy);
  return self;
}

- (id) get_vadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_clist_get_vadjustment(gtkclist)];
}

- (GtkVisibility) row_is_visible
 : (gint) row
{
  return gtk_clist_row_is_visible(gtkclist, row);
}

- set_selection_mode
 : (GtkSelectionMode) mode
{
  gtk_clist_set_selection_mode(gtkclist, mode);
  return self;
}

- (gchar *) get_column_title
 : (gint) column
{
  return gtk_clist_get_column_title(gtkclist, column);
}

- moveto
 : (gint) row
 : (gint) column
 : (gfloat) row_align
 : (gfloat) col_align
{
  gtk_clist_moveto(gtkclist, row, column, row_align, col_align);
  return self;
}

- set_pixtext
 : (gint) row
 : (gint) column
 : (const gchar *) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask
{
  gtk_clist_set_pixtext(gtkclist, row, column, text, spacing, pixmap, mask);
  return self;
}

- undo_selection
{
  gtk_clist_undo_selection(gtkclist);
  return self;
}

- set_pixmap
 : (gint) row
 : (gint) column
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask
{
  gtk_clist_set_pixmap(gtkclist, row, column, pixmap, mask);
  return self;
}

- unselect_row
 : (gint) row
 : (gint) column
{
  gtk_clist_unselect_row(gtkclist, row, column);
  return self;
}

- (gpointer) get_row_data
 : (gint) row
{
  return gtk_clist_get_row_data(gtkclist, row);
}

- (gboolean) get_selectable
 : (gint) row
{
  return gtk_clist_get_selectable(gtkclist, row);
}

- (id) get_column_widget
 : (gint) column
{
  return [[Gtk_Widget class] makeGtkWidget:gtk_clist_get_column_widget(gtkclist, column)];
}

- set_row_data
 : (gint) row
 : (gpointer) data
{
  gtk_clist_set_row_data(gtkclist, row, data);
  return self;
}

- (GtkStyle *) get_cell_style
 : (gint) row
 : (gint) column
{
  return gtk_clist_get_cell_style(gtkclist, row, column);
}

- clear
{
  gtk_clist_clear(gtkclist);
  return self;
}

- set_column_auto_resize
 : (gint) column
 : (gboolean) auto_resize
{
  gtk_clist_set_column_auto_resize(gtkclist, column, auto_resize);
  return self;
}

- set_column_widget
 : (gint) column
 : (id) widget
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_clist_set_column_widget(gtkclist, column, ((Gtk_Widget *)widget)->gtkwidget);
  return self;
}

- (gint) get_pixtext
 : (gint) row
 : (gint) column
 : (gchar * *) text
 : (guint8 *) spacing
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask
{
  return gtk_clist_get_pixtext(gtkclist, row, column, text, spacing, pixmap, mask);
}

- (gint) append
 : (gchar **) text
{
  return gtk_clist_append(gtkclist, text);
}

- (gint) columns_autosize
{
  return gtk_clist_columns_autosize(gtkclist);
}

- select_all
{
  gtk_clist_select_all(gtkclist);
  return self;
}

- set_text
 : (gint) row
 : (gint) column
 : (const gchar *) text
{
  gtk_clist_set_text(gtkclist, row, column, text);
  return self;
}

- column_titles_hide
{
  gtk_clist_column_titles_hide(gtkclist);
  return self;
}

- column_titles_passive
{
  gtk_clist_column_titles_passive(gtkclist);
  return self;
}

- column_titles_active
{
  gtk_clist_column_titles_active(gtkclist);
  return self;
}

- set_auto_sort
 : (gboolean) auto_sort
{
  gtk_clist_set_auto_sort(gtkclist, auto_sort);
  return self;
}

- freeze
{
  gtk_clist_freeze(gtkclist);
  return self;
}

- column_titles_show
{
  gtk_clist_column_titles_show(gtkclist);
  return self;
}

- (id) get_hadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_clist_get_hadjustment(gtkclist)];
}

- set_column_resizeable
 : (gint) column
 : (gboolean) resizeable
{
  gtk_clist_set_column_resizeable(gtkclist, column, resizeable);
  return self;
}

- set_foreground
 : (gint) row
 : (GdkColor *) color
{
  gtk_clist_set_foreground(gtkclist, row, color);
  return self;
}

- set_row_height
 : (guint) height
{
  gtk_clist_set_row_height(gtkclist, height);
  return self;
}

- set_background
 : (gint) row
 : (GdkColor *) color
{
  gtk_clist_set_background(gtkclist, row, color);
  return self;
}

- set_shift
 : (gint) row
 : (gint) column
 : (gint) vertical
 : (gint) horizontal
{
  gtk_clist_set_shift(gtkclist, row, column, vertical, horizontal);
  return self;
}

- construct
 : (gint) columns
 : (gchar **) titles
{
  gtk_clist_construct(gtkclist, columns, titles);
  return self;
}

- set_hadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_clist_set_hadjustment(gtkclist, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- (GtkStyle *) get_row_style
 : (gint) row
{
  return gtk_clist_get_row_style(gtkclist, row);
}

- set_column_visibility
 : (gint) column
 : (gboolean) visible
{
  gtk_clist_set_column_visibility(gtkclist, column, visible);
  return self;
}

- (gint) optimal_column_width
 : (gint) column
{
  return gtk_clist_optimal_column_width(gtkclist, column);
}

- (gint) find_row_from_data
 : (gpointer) data
{
  return gtk_clist_find_row_from_data(gtkclist, data);
}

- (gint) get_selection_info
 : (gint) x
 : (gint) y
 : (gint *) row
 : (gint *) column
{
  return gtk_clist_get_selection_info(gtkclist, x, y, row, column);
}

- swap_rows
 : (gint) row1
 : (gint) row2
{
  gtk_clist_swap_rows(gtkclist, row1, row2);
  return self;
}

- thaw
{
  gtk_clist_thaw(gtkclist);
  return self;
}

- (GtkCellType) get_cell_type
 : (gint) row
 : (gint) column
{
  return gtk_clist_get_cell_type(gtkclist, row, column);
}

- row_move
 : (gint) source_row
 : (gint) dest_row
{
  gtk_clist_row_move(gtkclist, source_row, dest_row);
  return self;
}

- select_row
 : (gint) row
 : (gint) column
{
  gtk_clist_select_row(gtkclist, row, column);
  return self;
}

- set_vadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_clist_set_vadjustment(gtkclist, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- unselect_all
{
  gtk_clist_unselect_all(gtkclist);
  return self;
}

- set_button_actions
 : (guint) button
 : (guint8) button_actions
{
  gtk_clist_set_button_actions(gtkclist, button, button_actions);
  return self;
}

- (gint) get_pixmap
 : (gint) row
 : (gint) column
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask
{
  return gtk_clist_get_pixmap(gtkclist, row, column, pixmap, mask);
}

- (gint) insert
 : (gint) row
 : (gchar **) text
{
  return gtk_clist_insert(gtkclist, row, text);
}

- remove
 : (gint) row
{
  gtk_clist_remove(gtkclist, row);
  return self;
}

- (gint) prepend
 : (gchar **) text
{
  return gtk_clist_prepend(gtkclist, text);
}

- set_row_style
 : (gint) row
 : (GtkStyle *) style
{
  gtk_clist_set_row_style(gtkclist, row, style);
  return self;
}

- sort
{
  gtk_clist_sort(gtkclist);
  return self;
}

- initWithTitles
 : (gint) columns
 : (gchar **) titles
{
  return [self castGtkCList:GTK_CLIST(gtk_clist_new_with_titles(columns, titles))];
}

- set_column_title
 : (gint) column
 : (const gchar *) title
{
  gtk_clist_set_column_title(gtkclist, column, title);
  return self;
}

- set_use_drag_icons
 : (gboolean) use_icons
{
  gtk_clist_set_use_drag_icons(gtkclist, use_icons);
  return self;
}

- set_compare_func
 : (GtkCListCompareFunc) cmp_func
{
  gtk_clist_set_compare_func(gtkclist, cmp_func);
  return self;
}

- set_reorderable
 : (gboolean) reorderable
{
  gtk_clist_set_reorderable(gtkclist, reorderable);
  return self;
}

@end
