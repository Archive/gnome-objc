#include "obgtkLabel.h"

@implementation Gtk_Label

- castGtkLabel : (GtkLabel *) castitem
{
  gtklabel = castitem;
  return [super castGtkMisc:GTK_MISC(castitem)];
}

+ (id) makeGtkLabel : (GtkLabel *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkLabel:castitem];

  return retval;
}

- set_justify
 : (GtkJustification) jtype
{
  gtk_label_set_justify(gtklabel, jtype);
  return self;
}

- set_text
 : (const char *) str
{
  gtk_label_set_text(gtklabel, str);
  return self;
}

- initWithLabelInfo
 : (const char *) str
{
  return [self castGtkLabel:GTK_LABEL(gtk_label_new(str))];
}

- (guint) parse_uline
 : (const gchar *) string
{
  return gtk_label_parse_uline(gtklabel, string);
}

- get
 : (char * *) str
{
  gtk_label_get(gtklabel, str);
  return self;
}

- set_pattern
 : (const gchar *) pattern
{
  gtk_label_set_pattern(gtklabel, pattern);
  return self;
}

- set_line_wrap
 : (gboolean) wrap
{
  gtk_label_set_line_wrap(gtklabel, wrap);
  return self;
}

@end
