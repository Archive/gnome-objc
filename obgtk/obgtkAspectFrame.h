#ifndef OBGTK_ASPECTFRAME_H
#define OBGTK_ASPECTFRAME_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkaspectframe.h>

@interface Gtk_AspectFrame : Gtk_Bin
{
@public
  GtkAspectFrame *gtkaspectframe;
}
- castGtkAspectFrame : (GtkAspectFrame *) castitem;
+ (id)makeGtkAspectFrame : (GtkAspectFrame *) castitem;
- set
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) ratio
 : (gint) obey_child;
- initWithAspectFrameInfo
 : (const gchar *) label
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) ratio
 : (gint) obey_child;
@end

#endif /* OBGTK_ASPECTFRAME_H */
