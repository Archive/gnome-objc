#ifndef OBGTK_TEXT_H
#define OBGTK_TEXT_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkEditable.h>
#include <gtk/gtktext.h>

@interface Gtk_Text : Gtk_Editable
{
@public
  GtkText *gtktext;
}
- castGtkText : (GtkText *) castitem;
+ (id)makeGtkText : (GtkText *) castitem;
- set_adjustments
 : (id) hadj
 : (id) vadj;
- set_point
 : (guint) index;
- freeze;
- set_editable
 : (gboolean) editable;
- (gint) backward_delete
 : (guint) nchars;
- set_word_wrap
 : (gint) word_wrap;
- thaw;
- (guint) get_length;
- initWithTextInfo
 : (id) hadj
 : (id) vadj;
- (gint) forward_delete
 : (guint) nchars;
- set_line_wrap
 : (gint) line_wrap;
- insert
 : (GdkFont *) font
 : (GdkColor *) fore
 : (GdkColor *) back
 : (const char *) chars
 : (gint) length;
- (guint) get_point;
@end

#endif /* OBGTK_TEXT_H */
