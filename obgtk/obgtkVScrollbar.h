#ifndef OBGTK_VSCROLLBAR_H
#define OBGTK_VSCROLLBAR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkScrollbar.h>
#include <gtk/gtkvscrollbar.h>

@interface Gtk_VScrollbar : Gtk_Scrollbar
{
@public
  GtkVScrollbar *gtkvscrollbar;
}
- castGtkVScrollbar : (GtkVScrollbar *) castitem;
+ (id)makeGtkVScrollbar : (GtkVScrollbar *) castitem;
- initWithVScrollbarInfo
 : (id) adjustment;
@end

#endif /* OBGTK_VSCROLLBAR_H */
