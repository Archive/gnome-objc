#ifndef OBGTK_EDITABLE_H
#define OBGTK_EDITABLE_H 1
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkeditable.h>

@interface Gtk_Editable : Gtk_Widget
{
@public
  GtkEditable *gtkeditable;
}
- castGtkEditable : (GtkEditable *) castitem;
+ (id)makeGtkEditable : (GtkEditable *) castitem;
- delete_selection;
- (gchar*) get_chars
 : (gint) start_pos
 : (gint) end_pos;
- changed;
- copy_clipboard;
- delete_text
 : (gint) start_pos
 : (gint) end_pos;
- set_position
 : (gint) position;
- claim_selection
 : (gboolean) claim
 : (guint32) time;
- cut_clipboard;
- insert_text
 : (const gchar *) new_text
 : (gint) new_text_length
 : (gint *) position;
- (gint) get_position;
- paste_clipboard;
- select_region
 : (gint) start
 : (gint) end;
- set_editable
 : (gboolean) is_editable;
@end

#endif /* OBGTK_EDITABLE_H */
