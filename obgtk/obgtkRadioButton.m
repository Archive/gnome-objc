#include "obgtkRadioButton.h"

@implementation Gtk_RadioButton

- castGtkRadioButton : (GtkRadioButton *) castitem
{
  gtkradiobutton = castitem;
  return [super castGtkCheckButton:GTK_CHECK_BUTTON(castitem)];
}

+ (id) makeGtkRadioButton : (GtkRadioButton *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkRadioButton:castitem];

  return retval;
}

- initWithLabel
 : (GSList *) group
 : (const gchar *) label
{
  return [self castGtkRadioButton:GTK_RADIO_BUTTON(gtk_radio_button_new_with_label(group, label))];
}

- set_group
 : (GSList *) group
{
  gtk_radio_button_set_group(gtkradiobutton, group);
  return self;
}

- initWithLabelFromWidget
 : (GtkRadioButton *) group
 : (const gchar *) label
{
  return [self castGtkRadioButton:GTK_RADIO_BUTTON(gtk_radio_button_new_with_label_from_widget(group, label))];
}

- (GSList*) group
{
  return gtk_radio_button_group(gtkradiobutton);
}

- initWithRadioButtonInfo
 : (GSList *) group
{
  return [self castGtkRadioButton:GTK_RADIO_BUTTON(gtk_radio_button_new(group))];
}

- initFromWidget
 : (GtkRadioButton *) group
{
  return [self castGtkRadioButton:GTK_RADIO_BUTTON(gtk_radio_button_new_from_widget(group))];
}

@end
