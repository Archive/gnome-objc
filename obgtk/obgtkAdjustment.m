#include "obgtkAdjustment.h"

@implementation Gtk_Adjustment

- castGtkAdjustment : (GtkAdjustment *) castitem
{
  gtkadjustment = castitem;
  return [super castGtkData:GTK_DATA(castitem)];
}

+ (id) makeGtkAdjustment : (GtkAdjustment *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkAdjustment:castitem];

  return retval;
}

- changed
{
  gtk_adjustment_changed(gtkadjustment);
  return self;
}

- initWithAdjustmentInfo
 : (gfloat) value
 : (gfloat) lower
 : (gfloat) upper
 : (gfloat) step_increment
 : (gfloat) page_increment
 : (gfloat) page_size
{
  return [self castGtkAdjustment:GTK_ADJUSTMENT(gtk_adjustment_new(value, lower, upper, step_increment, page_increment, page_size))];
}

- set_value
 : (gfloat) value
{
  gtk_adjustment_set_value(gtkadjustment, value);
  return self;
}

- value_changed
{
  gtk_adjustment_value_changed(gtkadjustment);
  return self;
}

- clamp_page
 : (gfloat) lower
 : (gfloat) upper
{
  gtk_adjustment_clamp_page(gtkadjustment, lower, upper);
  return self;
}

@end
