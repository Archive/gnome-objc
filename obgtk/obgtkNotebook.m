#include "obgtkNotebook.h"

@implementation Gtk_Notebook

- castGtkNotebook : (GtkNotebook *) castitem
{
  gtknotebook = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkNotebook : (GtkNotebook *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkNotebook:castitem];

  return retval;
}

- prepend_page_menu
 : (id) child
 : (id) tab_label
 : (id) menu_label
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([tab_label isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([menu_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_prepend_page_menu(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)tab_label)->gtkwidget, ((Gtk_Widget *)menu_label)->gtkwidget);
  return self;
}

- reorder_child
 : (id) child
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_reorder_child(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, position);
  return self;
}

- set_tab_border
 : (guint) border_width
{
  gtk_notebook_set_tab_border(gtknotebook, border_width);
  return self;
}

- (gint) page_num
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  return gtk_notebook_page_num(gtknotebook, ((Gtk_Widget *)child)->gtkwidget);
}

- (id) get_nth_page
 : (gint) page_num
{
  return [[Gtk_Widget class] makeGtkWidget:gtk_notebook_get_nth_page(gtknotebook, page_num)];
}

- set_show_tabs
 : (gboolean) show_tabs
{
  gtk_notebook_set_show_tabs(gtknotebook, show_tabs);
  return self;
}

- set_page
 : (gint) page_num
{
  gtk_notebook_set_page(gtknotebook, page_num);
  return self;
}

- set_tab_hborder
 : (guint) tab_hborder
{
  gtk_notebook_set_tab_hborder(gtknotebook, tab_hborder);
  return self;
}

- append_page_menu
 : (id) child
 : (id) tab_label
 : (id) menu_label
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([tab_label isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([menu_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_append_page_menu(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)tab_label)->gtkwidget, ((Gtk_Widget *)menu_label)->gtkwidget);
  return self;
}

- set_menu_label
 : (id) child
 : (id) menu_label
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([menu_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_set_menu_label(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)menu_label)->gtkwidget);
  return self;
}

- (id) get_menu_label
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_notebook_get_menu_label(gtknotebook, ((Gtk_Widget *)child)->gtkwidget)];
}

- prev_page
{
  gtk_notebook_prev_page(gtknotebook);
  return self;
}

- set_tab_label
 : (id) child
 : (id) tab_label
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([tab_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_set_tab_label(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)tab_label)->gtkwidget);
  return self;
}

- (id) get_tab_label
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_notebook_get_tab_label(gtknotebook, ((Gtk_Widget *)child)->gtkwidget)];
}

- insert_page
 : (id) child
 : (id) tab_label
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([tab_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_insert_page(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)tab_label)->gtkwidget, position);
  return self;
}

- (gint) get_current_page
{
  return gtk_notebook_get_current_page(gtknotebook);
}

- set_scrollable
 : (gboolean) scrollable
{
  gtk_notebook_set_scrollable(gtknotebook, scrollable);
  return self;
}

- prepend_page
 : (id) child
 : (id) tab_label
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([tab_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_prepend_page(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)tab_label)->gtkwidget);
  return self;
}

- init
{
  return [self castGtkNotebook:GTK_NOTEBOOK(gtk_notebook_new())];
}

- append_page
 : (id) child
 : (id) tab_label
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([tab_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_append_page(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)tab_label)->gtkwidget);
  return self;
}

- set_tab_pos
 : (GtkPositionType) pos
{
  gtk_notebook_set_tab_pos(gtknotebook, pos);
  return self;
}

- set_tab_vborder
 : (guint) tab_vborder
{
  gtk_notebook_set_tab_vborder(gtknotebook, tab_vborder);
  return self;
}

- remove_page
 : (gint) page_num
{
  gtk_notebook_remove_page(gtknotebook, page_num);
  return self;
}

- set_homogeneous_tabs
 : (gboolean) homogeneous
{
  gtk_notebook_set_homogeneous_tabs(gtknotebook, homogeneous);
  return self;
}

- set_show_border
 : (gboolean) show_border
{
  gtk_notebook_set_show_border(gtknotebook, show_border);
  return self;
}

- popup_enable
{
  gtk_notebook_popup_enable(gtknotebook);
  return self;
}

- next_page
{
  gtk_notebook_next_page(gtknotebook);
  return self;
}

- set_tab_label_packing
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (GtkPackType) pack_type
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_set_tab_label_packing(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, expand, fill, pack_type);
  return self;
}

- set_menu_label_text
 : (id) child
 : (const gchar *) menu_text
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_set_menu_label_text(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, menu_text);
  return self;
}

- insert_page_menu
 : (id) child
 : (id) tab_label
 : (id) menu_label
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([tab_label isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([menu_label isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_insert_page_menu(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, ((Gtk_Widget *)tab_label)->gtkwidget, ((Gtk_Widget *)menu_label)->gtkwidget, position);
  return self;
}

- set_tab_label_text
 : (id) child
 : (const gchar *) tab_text
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_set_tab_label_text(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, tab_text);
  return self;
}

- popup_disable
{
  gtk_notebook_popup_disable(gtknotebook);
  return self;
}

- query_tab_label_packing
 : (id) child
 : (gboolean *) expand
 : (gboolean *) fill
 : (GtkPackType *) pack_type
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_notebook_query_tab_label_packing(gtknotebook, ((Gtk_Widget *)child)->gtkwidget, expand, fill, pack_type);
  return self;
}

@end
