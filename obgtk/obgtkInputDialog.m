#include "obgtkInputDialog.h"

@implementation Gtk_InputDialog

- castGtkInputDialog : (GtkInputDialog *) castitem
{
  gtkinputdialog = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkInputDialog : (GtkInputDialog *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkInputDialog:castitem];

  return retval;
}

- init
{
  return [self castGtkInputDialog:GTK_INPUT_DIALOG(gtk_input_dialog_new())];
}

@end
