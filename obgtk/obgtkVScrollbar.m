#include "obgtkVScrollbar.h"

@implementation Gtk_VScrollbar

- castGtkVScrollbar : (GtkVScrollbar *) castitem
{
  gtkvscrollbar = castitem;
  return [super castGtkScrollbar:GTK_SCROLLBAR(castitem)];
}

+ (id) makeGtkVScrollbar : (GtkVScrollbar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkVScrollbar:castitem];

  return retval;
}

- initWithVScrollbarInfo
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkVScrollbar:GTK_VSCROLLBAR(gtk_vscrollbar_new(((Gtk_Adjustment *)adjustment)->gtkadjustment))];
}

@end
