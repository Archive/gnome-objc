#ifndef OBGTK_COLORSELECTION_H
#define OBGTK_COLORSELECTION_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkcolorsel.h>

@interface Gtk_ColorSelection : Gtk_Window
{
@public
  GtkColorSelection *gtkcolorselection;
}
- castGtkColorSelection : (GtkColorSelection *) castitem;
+ (id)makeGtkColorSelection : (GtkColorSelection *) castitem;
- set_update_policy
 : (GtkUpdateType) policy;
- get_color
 : (gdouble *) color;
- init;
- set_opacity
 : (gint) use_opacity;
- initWithColorSelectionInfo
 : (const gchar *) title;
- set_color
 : (gdouble *) color;
@end

#endif /* OBGTK_COLORSELECTION_H */
