#include "obgtkContainer.h"

@implementation Gtk_Container

- castGtkContainer : (GtkContainer *) castitem
{
  gtkcontainer = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkContainer : (GtkContainer *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkContainer:castitem];

  return retval;
}

- forall
 : (GtkCallback) callback
 : (gpointer) callback_data
{
  gtk_container_forall(gtkcontainer, callback, callback_data);
  return self;
}

- arg_set
 : (id) child
 : (GtkArg *) arg
 : (GtkArgInfo *) info
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_container_arg_set(gtkcontainer, ((Gtk_Widget *)child)->gtkwidget, arg, info);
  return self;
}

- set_focus_hadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_container_set_focus_hadjustment(gtkcontainer, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- register_toplevel
{
  gtk_container_register_toplevel(gtkcontainer);
  return self;
}

- unregister_toplevel
{
  gtk_container_unregister_toplevel(gtkcontainer);
  return self;
}

- remove
 : (id) widget
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_container_remove(gtkcontainer, ((Gtk_Widget *)widget)->gtkwidget);
  return self;
}

- set_focus_vadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_container_set_focus_vadjustment(gtkcontainer, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- (GtkType) child_type
{
  return gtk_container_child_type(gtkcontainer);
}

- foreach
 : (GtkCallback) callback
 : (gpointer) callback_data
{
  gtk_container_foreach(gtkcontainer, callback, callback_data);
  return self;
}

- (GList*) children
{
  return gtk_container_children(gtkcontainer);
}

- foreach_full
 : (GtkCallback) callback
 : (GtkCallbackMarshal) marshal
 : (gpointer) callback_data
 : (GtkDestroyNotify) notify
{
  gtk_container_foreach_full(gtkcontainer, callback, marshal, callback_data, notify);
  return self;
}

- add
 : (id) widget
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_container_add(gtkcontainer, ((Gtk_Widget *)widget)->gtkwidget);
  return self;
}

- child_getv
 : (id) child
 : (guint) n_args
 : (GtkArg *) args
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_container_child_getv(gtkcontainer, ((Gtk_Widget *)child)->gtkwidget, n_args, args);
  return self;
}

- clear_resize_widgets
{
  gtk_container_clear_resize_widgets(gtkcontainer);
  return self;
}

- check_resize
{
  gtk_container_check_resize(gtkcontainer);
  return self;
}

- set_focus_child
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_container_set_focus_child(gtkcontainer, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- addv
 : (id) widget
 : (guint) n_args
 : (GtkArg *) args
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_container_addv(gtkcontainer, ((Gtk_Widget *)widget)->gtkwidget, n_args, args);
  return self;
}

- (gint) focus
 : (GtkDirectionType) direction
{
  return gtk_container_focus(gtkcontainer, direction);
}

- queue_resize
{
  gtk_container_queue_resize(gtkcontainer);
  return self;
}

- (gchar*) child_composite_name
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  return gtk_container_child_composite_name(gtkcontainer, ((Gtk_Widget *)child)->gtkwidget);
}

- set_border_width
 : (guint) border_width
{
  gtk_container_set_border_width(gtkcontainer, border_width);
  return self;
}

- child_setv
 : (id) child
 : (guint) n_args
 : (GtkArg *) args
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_container_child_setv(gtkcontainer, ((Gtk_Widget *)child)->gtkwidget, n_args, args);
  return self;
}

- resize_children
{
  gtk_container_resize_children(gtkcontainer);
  return self;
}

- arg_get
 : (id) child
 : (GtkArg *) arg
 : (GtkArgInfo *) info
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_container_arg_get(gtkcontainer, ((Gtk_Widget *)child)->gtkwidget, arg, info);
  return self;
}

- set_resize_mode
 : (GtkResizeMode) resize_mode
{
  gtk_container_set_resize_mode(gtkcontainer, resize_mode);
  return self;
}

@end
