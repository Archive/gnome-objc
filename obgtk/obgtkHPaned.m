#include "obgtkHPaned.h"

@implementation Gtk_HPaned

- castGtkHPaned : (GtkHPaned *) castitem
{
  gtkhpaned = castitem;
  return [super castGtkPaned:GTK_PANED(castitem)];
}

+ (id) makeGtkHPaned : (GtkHPaned *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHPaned:castitem];

  return retval;
}

- init
{
  return [self castGtkHPaned:GTK_HPANED(gtk_hpaned_new())];
}

@end
