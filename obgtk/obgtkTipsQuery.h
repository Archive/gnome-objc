#ifndef OBGTK_TIPSQUERY_H
#define OBGTK_TIPSQUERY_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkLabel.h>
#include <gtk/gtktipsquery.h>

@interface Gtk_TipsQuery : Gtk_Label
{
@public
  GtkTipsQuery *gtktipsquery;
}
- castGtkTipsQuery : (GtkTipsQuery *) castitem;
+ (id)makeGtkTipsQuery : (GtkTipsQuery *) castitem;
- set_caller
 : (id) caller;
- stop_query;
- start_query;
- init;
- set_labels
 : (const gchar *) label_inactive
 : (const gchar *) label_no_tip;
@end

#endif /* OBGTK_TIPSQUERY_H */
