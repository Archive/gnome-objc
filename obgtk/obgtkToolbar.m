#include "obgtkToolbar.h"

@implementation Gtk_Toolbar

- castGtkToolbar : (GtkToolbar *) castitem
{
  gtktoolbar = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkToolbar : (GtkToolbar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkToolbar:castitem];

  return retval;
}

- set_space_size
 : (gint) space_size
{
  gtk_toolbar_set_space_size(gtktoolbar, space_size);
  return self;
}

- (id) prepend_element
 : (GtkToolbarChildType) type
 : (id) widget
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([icon isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_toolbar_prepend_element(gtktoolbar, type, ((Gtk_Widget *)widget)->gtkwidget, text, tooltip_text, tooltip_private_text, ((Gtk_Widget *)icon)->gtkwidget, callback, user_data)];
}

- prepend_space
{
  gtk_toolbar_prepend_space(gtktoolbar);
  return self;
}

- initWithToolbarInfo
 : (GtkOrientation) orientation
 : (GtkToolbarStyle) style
{
  return [self castGtkToolbar:GTK_TOOLBAR(gtk_toolbar_new(orientation, style))];
}

- insert_widget
 : (id) widget
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (gint) position
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_toolbar_insert_widget(gtktoolbar, ((Gtk_Widget *)widget)->gtkwidget, tooltip_text, tooltip_private_text, position);
  return self;
}

- (id) append_element
 : (GtkToolbarChildType) type
 : (id) widget
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([icon isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_toolbar_append_element(gtktoolbar, type, ((Gtk_Widget *)widget)->gtkwidget, text, tooltip_text, tooltip_private_text, ((Gtk_Widget *)icon)->gtkwidget, callback, user_data)];
}

- set_orientation
 : (GtkOrientation) orientation
{
  gtk_toolbar_set_orientation(gtktoolbar, orientation);
  return self;
}

- set_style
 : (GtkToolbarStyle) style
{
  gtk_toolbar_set_style(gtktoolbar, style);
  return self;
}

- prepend_widget
 : (id) widget
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_toolbar_prepend_widget(gtktoolbar, ((Gtk_Widget *)widget)->gtkwidget, tooltip_text, tooltip_private_text);
  return self;
}

- append_space
{
  gtk_toolbar_append_space(gtktoolbar);
  return self;
}

- (id) insert_item
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
 : (gint) position
{
  g_return_val_if_fail([icon isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_toolbar_insert_item(gtktoolbar, text, tooltip_text, tooltip_private_text, ((Gtk_Widget *)icon)->gtkwidget, callback, user_data, position)];
}

- set_space_style
 : (GtkToolbarSpaceStyle) space_style
{
  gtk_toolbar_set_space_style(gtktoolbar, space_style);
  return self;
}

- (id) prepend_item
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
{
  g_return_val_if_fail([icon isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_toolbar_prepend_item(gtktoolbar, text, tooltip_text, tooltip_private_text, ((Gtk_Widget *)icon)->gtkwidget, callback, user_data)];
}

- (GtkReliefStyle) get_button_relief
{
  return gtk_toolbar_get_button_relief(gtktoolbar);
}

- append_widget
 : (id) widget
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_toolbar_append_widget(gtktoolbar, ((Gtk_Widget *)widget)->gtkwidget, tooltip_text, tooltip_private_text);
  return self;
}

- set_button_relief
 : (GtkReliefStyle) relief
{
  gtk_toolbar_set_button_relief(gtktoolbar, relief);
  return self;
}

- set_tooltips
 : (gint) enable
{
  gtk_toolbar_set_tooltips(gtktoolbar, enable);
  return self;
}

- (id) insert_element
 : (GtkToolbarChildType) type
 : (id) widget
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
 : (gint) position
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([icon isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_toolbar_insert_element(gtktoolbar, type, ((Gtk_Widget *)widget)->gtkwidget, text, tooltip_text, tooltip_private_text, ((Gtk_Widget *)icon)->gtkwidget, callback, user_data, position)];
}

- insert_space
 : (gint) position
{
  gtk_toolbar_insert_space(gtktoolbar, position);
  return self;
}

- (id) append_item
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
{
  g_return_val_if_fail([icon isKindOf: [Gtk_Widget class]], 0);
  return [[Gtk_Widget class] makeGtkWidget:gtk_toolbar_append_item(gtktoolbar, text, tooltip_text, tooltip_private_text, ((Gtk_Widget *)icon)->gtkwidget, callback, user_data)];
}

@end
