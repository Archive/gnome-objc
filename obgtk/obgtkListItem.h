#ifndef OBGTK_LISTITEM_H
#define OBGTK_LISTITEM_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkItem.h>
#include <gtk/gtklistitem.h>

@interface Gtk_ListItem : Gtk_Item
{
@public
  GtkListItem *gtklistitem;
}
- castGtkListItem : (GtkListItem *) castitem;
+ (id)makeGtkListItem : (GtkListItem *) castitem;
- initWithLabel
 : (const gchar *) label;
- select;
- deselect;
- init;
@end

#endif /* OBGTK_LISTITEM_H */
