#include "obgtkImage.h"

@implementation Gtk_Image

- castGtkImage : (GtkImage *) castitem
{
  gtkimage = castitem;
  return [super castGtkMisc:GTK_MISC(castitem)];
}

+ (id) makeGtkImage : (GtkImage *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkImage:castitem];

  return retval;
}

- initWithImageInfo
 : (GdkImage *) val
 : (GdkBitmap *) mask
{
  return [self castGtkImage:GTK_IMAGE(gtk_image_new(val, mask))];
}

- get
 : (GdkImage * *) val
 : (GdkBitmap * *) mask
{
  gtk_image_get(gtkimage, val, mask);
  return self;
}

- set
 : (GdkImage *) val
 : (GdkBitmap *) mask
{
  gtk_image_set(gtkimage, val, mask);
  return self;
}

@end
