#ifndef OBGTK_CURVE_H
#define OBGTK_CURVE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkDrawingArea.h>
#include <gtk/gtkcurve.h>

@interface Gtk_Curve : Gtk_DrawingArea
{
@public
  GtkCurve *gtkcurve;
}
- castGtkCurve : (GtkCurve *) castitem;
+ (id)makeGtkCurve : (GtkCurve *) castitem;
- get_vector
 : (int) veclen
 : (gfloat*) vector;
- set_gamma
 : (gfloat) gamma;
- reset;
- set_curve_type
 : (GtkCurveType) type;
- init;
- set_vector
 : (int) veclen
 : (gfloat*) vector;
- set_range
 : (gfloat) min_x
 : (gfloat) max_x
 : (gfloat) min_y
 : (gfloat) max_y;
@end

#endif /* OBGTK_CURVE_H */
