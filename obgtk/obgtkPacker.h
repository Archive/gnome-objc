#ifndef OBGTK_PACKER_H
#define OBGTK_PACKER_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtkpacker.h>

@interface Gtk_Packer : Gtk_Container
{
@public
  GtkPacker *gtkpacker;
}
- castGtkPacker : (GtkPacker *) castitem;
+ (id)makeGtkPacker : (GtkPacker *) castitem;
- set_default_ipad
 : (guint) i_pad_x
 : (guint) i_pad_y;
- add_defaults
 : (id) child
 : (GtkSideType) side
 : (GtkAnchorType) anchor
 : (GtkPackerOptions) options;
- set_default_border_width
 : (guint) border;
- reorder_child
 : (id) child
 : (gint) position;
- set_child_packing
 : (id) child
 : (GtkSideType) side
 : (GtkAnchorType) anchor
 : (GtkPackerOptions) options
 : (guint) border_width
 : (guint) pad_x
 : (guint) pad_y
 : (guint) i_pad_x
 : (guint) i_pad_y;
- set_spacing
 : (guint) spacing;
- init;
- set_default_pad
 : (guint) pad_x
 : (guint) pad_y;
- add
 : (id) child
 : (GtkSideType) side
 : (GtkAnchorType) anchor
 : (GtkPackerOptions) options
 : (guint) border_width
 : (guint) pad_x
 : (guint) pad_y
 : (guint) i_pad_x
 : (guint) i_pad_y;
@end

#endif /* OBGTK_PACKER_H */
