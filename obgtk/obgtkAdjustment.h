#ifndef OBGTK_ADJUSTMENT_H
#define OBGTK_ADJUSTMENT_H 1
#include <obgtk/obgtkObject.h>
#include <obgtk/obgtkData.h>
#include <gtk/gtkadjustment.h>

@interface Gtk_Adjustment : Gtk_Data
{
@public
  GtkAdjustment *gtkadjustment;
}
- castGtkAdjustment : (GtkAdjustment *) castitem;
+ (id)makeGtkAdjustment : (GtkAdjustment *) castitem;
- changed;
- initWithAdjustmentInfo
 : (gfloat) value
 : (gfloat) lower
 : (gfloat) upper
 : (gfloat) step_increment
 : (gfloat) page_increment
 : (gfloat) page_size;
- set_value
 : (gfloat) value;
- value_changed;
- clamp_page
 : (gfloat) lower
 : (gfloat) upper;
@end

#endif /* OBGTK_ADJUSTMENT_H */
