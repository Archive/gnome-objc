#ifndef OBGTK_VBOX_H
#define OBGTK_VBOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBox.h>
#include <gtk/gtkvbox.h>

@interface Gtk_VBox : Gtk_Box
{
@public
  GtkVBox *gtkvbox;
}
- castGtkVBox : (GtkVBox *) castitem;
+ (id)makeGtkVBox : (GtkVBox *) castitem;
- initWithVBoxInfo
 : (gboolean) homogeneous
 : (gint) spacing;
@end

#endif /* OBGTK_VBOX_H */
