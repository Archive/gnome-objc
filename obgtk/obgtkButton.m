#include "obgtkButton.h"

@implementation Gtk_Button

- castGtkButton : (GtkButton *) castitem
{
  gtkbutton = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkButton : (GtkButton *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkButton:castitem];

  return retval;
}

- leave
{
  gtk_button_leave(gtkbutton);
  return self;
}

- enter
{
  gtk_button_enter(gtkbutton);
  return self;
}

- clicked
{
  gtk_button_clicked(gtkbutton);
  return self;
}

- (GtkReliefStyle) get_relief
{
  return gtk_button_get_relief(gtkbutton);
}

- initWithLabel
 : (const gchar *) label
{
  return [self castGtkButton:GTK_BUTTON(gtk_button_new_with_label(label))];
}

- released
{
  gtk_button_released(gtkbutton);
  return self;
}

- init
{
  return [self castGtkButton:GTK_BUTTON(gtk_button_new())];
}

- pressed
{
  gtk_button_pressed(gtkbutton);
  return self;
}

- set_relief
 : (GtkReliefStyle) newstyle
{
  gtk_button_set_relief(gtkbutton, newstyle);
  return self;
}

@end
