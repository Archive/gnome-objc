#ifndef OBGTK_COMBO_H
#define OBGTK_COMBO_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkItem.h>
#include <obgtk/obgtkHBox.h>
#include <gtk/gtkcombo.h>

@interface Gtk_Combo : Gtk_HBox
{
@public
  GtkCombo *gtkcombo;
}
- castGtkCombo : (GtkCombo *) castitem;
+ (id)makeGtkCombo : (GtkCombo *) castitem;
- init;
@end

#endif /* OBGTK_COMBO_H */
