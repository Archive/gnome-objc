#ifndef OBGTK_INPUTDIALOG_H
#define OBGTK_INPUTDIALOG_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkinputdialog.h>

@interface Gtk_InputDialog : Gtk_Window
{
@public
  GtkInputDialog *gtkinputdialog;
}
- castGtkInputDialog : (GtkInputDialog *) castitem;
+ (id)makeGtkInputDialog : (GtkInputDialog *) castitem;
- init;
@end

#endif /* OBGTK_INPUTDIALOG_H */
