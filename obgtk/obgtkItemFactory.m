/* obgtkItemfactory: Copyright (C) 1999 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
  * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/****  WARNING : IMMATURE BINDING *****/

#include "obgtkItemFactory.h"

@implementation Gtk_ItemFactory

+ parseRc:(const gchar *) filename
{
  gtk_item_factory_parse_rc (filename);
}

+ parseRcString:(const gchar *) string
{
  gtk_item_factory_parse_rc_string (string);
}

+ parseRcScanner:(GScanner *) scanner
{
  gtk_item_factory_parse_rc_scanner (scanner);
}

+ fromWidget:(Gtk_Widget *)widget
{
  GtkItemFactory *gif;
  Gtk_ItemFactory *objcGif;

  gif = gtk_item_factory_from_widget( widget->gtkwidget );
  objcGif =  gtk_object_get_data_by_id( GTK_OBJECT(gif), OBJC_ID() );
  if (!objcGif) 
    {
      objcGif = [[ Gtk_ItemFactory alloc] castGtkItemFactory:gif];
    }

  return objcGif;  
}

+ (gchar *) pathFromWidget:(Gtk_Widget *) widget
{
  return gtk_item_factory_path_from_widget (widget->gtkwidget);
}

+ dumpItemsWithPathSpec:(GtkPatternSpec *) path_pspec
	   modifiedOnly:(gboolean) modified_only
	      printFunc:(GtkPrintFunc) print_func
	       funcData:(gpointer) func_data
{
  gtk_item_factory_dump_items (path_pspec, modified_only, print_func, func_data);
}

+ dumpRc        :(const gchar *) filename
 pathPatternSpec:(GtkPatternSpec *) path_pspec
    modifiedOnly:(gboolean) modified_only
{
  gtk_item_factory_dump_rc (filename, path_pspec, modified_only);
}

+ printFunc:(gpointer) file_pointer
     String:(gchar *)string
{
  gtk_item_factory_print_func (file_pointer, string);
} 

+ (gpointer) getDataFromWidget: (Gtk_Widget *)widget
{
  return gtk_item_factory_popup_data_from_widget (widget->gtkwidget);
}


- castGtkItemFactory:(GtkItemFactory *) castitem
{
  gtkitemfactory = castitem;
  return [super castGtkObject:(GtkObject *) castitem];
}

- initWithContainerType:(GtkType) type
		   path:(const gchar *) path
	     accelGroup:(Gtk_AccelGroup *) accelerator
{
  GtkItemFactory *gi;

  gi = gtk_item_factory_new(type, path, accelerator->gtkaccelgroup);
  return [self castGtkItemFactory: gi ];
}

- constructWithContainerType: (GtkType) type
			path:(const gchar *) path
		  accelGroup:(Gtk_AccelGroup *) accelerator
{
  gtk_item_factory_construct(gtkitemfactory, type, path, accelerator->gtkaccelgroup);
  return self;
}

- (Gtk_Widget *) getWidget:(const gchar *) path
{
  GtkWidget *widget;
  Gtk_Widget *objcWidget;

  widget = gtk_item_factory_get_widget( gtkitemfactory, path);
  objcWidget =  gtk_object_get_data_by_id( GTK_OBJECT(widget), OBJC_ID() );
  if (!objcWidget) 
    {
      objcWidget = [[ Gtk_Widget alloc] castGtkWidget:widget];
    }
  return objcWidget;
}


- (Gtk_Widget *) getWidgetByAction:(guint) action
{
  GtkWidget *widget;
  Gtk_Widget *objcWidget;

  widget = gtk_item_factory_get_widget_by_action( gtkitemfactory, action);
  objcWidget =  gtk_object_get_data_by_id( GTK_OBJECT(widget), OBJC_ID() );
  if (!objcWidget) 
    {
      objcWidget = [[ Gtk_Widget alloc] castGtkWidget:widget];
    }
  return objcWidget;
}

- createItemWithEntry: (GtkItemFactoryEntry *) entry
	 callbackData:(gpointer) callback_data
	 callbackType:(guint) callback_type
{
  gtk_item_factory_create_item (gtkitemfactory, entry, callback_data, callback_type);
  return self;
}

- createItemsWithNEntries: (guint) n_entries
		  entries:(GtkItemFactoryEntry *) entries
	     callbackData:(gpointer) callback_data
{
  gtk_item_factory_create_items (gtkitemfactory, n_entries, entries, callback_data);
  return self;
}

- deleteItemWithPath: (const gchar *) path
{
  gtk_item_factory_delete_item (gtkitemfactory, path);
  return self;
}

- deleteEntry: (GtkItemFactoryEntry *) entry
{
  gtk_item_factory_delete_entry (gtkitemfactory, entry);
  return self;
}

- deleteNEntries: (guint) n_entries
	 entries:(GtkItemFactoryEntry *) entries
{
  gtk_item_factory_delete_entries (gtkitemfactory, n_entries, entries);
  return self;
}

- popupWithX:(guint) x
	   y:(guint) y
 mouseButton:(guint) mouse_button
	time:(guint32) time
{
  gtk_item_factory_popup (gtkitemfactory, x, y, mouse_button, time);
  return self;
}

- popupWithData:(gpointer) data
  destroyNotify:(GtkDestroyNotify) destroy
	      x:(guint) x
	      y:(guint) y
 mouseButton:(guint) mouse_button
	time:(guint32) time
{
  gtk_item_factory_popup_with_data (gtkitemfactory, data, destroy, x, y, mouse_button, time);
  return self;
}

- (gpointer) getPopupData
{
  return gtk_item_factory_popup_data (gtkitemfactory);
}

- setTranslateFunc:(GtkTranslateFunc) func
	     data:(gpointer) data
    destroyNotify:(GtkDestroyNotify) destroy
{
  gtk_item_factory_set_translate_func (gtkitemfactory, func, data, destroy);
  return self;
}

@end
