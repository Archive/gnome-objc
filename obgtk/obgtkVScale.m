#include "obgtkVScale.h"

@implementation Gtk_VScale

- castGtkVScale : (GtkVScale *) castitem
{
  gtkvscale = castitem;
  return [super castGtkScale:GTK_SCALE(castitem)];
}

+ (id) makeGtkVScale : (GtkVScale *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkVScale:castitem];

  return retval;
}

- initWithVScaleInfo
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkVScale:GTK_VSCALE(gtk_vscale_new(((Gtk_Adjustment *)adjustment)->gtkadjustment))];
}

@end
