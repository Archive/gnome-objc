#include "obgtkEditable.h"

@implementation Gtk_Editable

- castGtkEditable : (GtkEditable *) castitem
{
  gtkeditable = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkEditable : (GtkEditable *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkEditable:castitem];

  return retval;
}

- delete_selection
{
  gtk_editable_delete_selection(gtkeditable);
  return self;
}

- (gchar*) get_chars
 : (gint) start_pos
 : (gint) end_pos
{
  return gtk_editable_get_chars(gtkeditable, start_pos, end_pos);
}

- changed
{
  gtk_editable_changed(gtkeditable);
  return self;
}

- copy_clipboard
{
  gtk_editable_copy_clipboard(gtkeditable);
  return self;
}

- delete_text
 : (gint) start_pos
 : (gint) end_pos
{
  gtk_editable_delete_text(gtkeditable, start_pos, end_pos);
  return self;
}

- set_position
 : (gint) position
{
  gtk_editable_set_position(gtkeditable, position);
  return self;
}

- claim_selection
 : (gboolean) claim
 : (guint32) time
{
  gtk_editable_claim_selection(gtkeditable, claim, time);
  return self;
}

- cut_clipboard
{
  gtk_editable_cut_clipboard(gtkeditable);
  return self;
}

- insert_text
 : (const gchar *) new_text
 : (gint) new_text_length
 : (gint *) position
{
  gtk_editable_insert_text(gtkeditable, new_text, new_text_length, position);
  return self;
}

- (gint) get_position
{
  return gtk_editable_get_position(gtkeditable);
}

- paste_clipboard
{
  gtk_editable_paste_clipboard(gtkeditable);
  return self;
}

- select_region
 : (gint) start
 : (gint) end
{
  gtk_editable_select_region(gtkeditable, start, end);
  return self;
}

- set_editable
 : (gboolean) is_editable
{
  gtk_editable_set_editable(gtkeditable, is_editable);
  return self;
}

@end
