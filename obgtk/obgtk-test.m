#include "obgtk.h"

@interface testApp : Gtk_App
{
  Gtk_Dialog *winMain;
  Gtk_Button *btnQuit;
}
@end

@implementation testApp
- button_press_event:(id) anobj
		:(GdkEventButton *) event
{
  g_print("Button %d was pressed\n", event->button);
  gtk_main_quit();
  return self;
}

- clicked:(id) anobj
{
  printf("A thing was clicked, object is %#x, btnQuit is %#x\n",
	 (unsigned int)anobj, (unsigned int)btnQuit);
  [winMain free];
  return self;
}

- initApp:(int *)argcp
      :(char ***)argvp
{
  [super initApp:argcp :argvp];

  btnQuit = [[Gtk_Button alloc] initWithLabel:"Quit"];
  [[btnQuit connectObj:"clicked" :self] show];
  [btnQuit connectObj:"button_press_event" :self];

  winMain = [[Gtk_Window alloc] initWithWindowInfo:GTK_WINDOW_TOPLEVEL];
	  
  [[winMain add:btnQuit] show];
  [winMain signal_connect:"delete_event"
	   signalFunc:gtk_main_quit funcData:NULL];
  return self;
}

- free
{
  g_print("Freeing me\n");
  gtk_main_quit();
  return [super free];
}
@end

int main(int argc, char *argv[])
{
	id myApp;

	myApp = [[testApp alloc] initApp:&argc :&argv];
	[myApp run];
	return 0;
}
