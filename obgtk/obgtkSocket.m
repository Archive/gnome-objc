#include "obgtkSocket.h"

@implementation Gtk_Socket

- castGtkSocket : (GtkSocket *) castitem
{
  gtksocket = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkSocket : (GtkSocket *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkSocket:castitem];

  return retval;
}

- init
{
  return [self castGtkSocket:GTK_SOCKET(gtk_socket_new())];
}

- steal
 : (guint32) wid
{
  gtk_socket_steal(gtksocket, wid);
  return self;
}

@end
