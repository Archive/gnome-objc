#include "obgtkGammaCurve.h"

@implementation Gtk_GammaCurve

- castGtkGammaCurve : (GtkGammaCurve *) castitem
{
  gtkgammacurve = castitem;
  return [super castGtkVBox:GTK_VBOX(castitem)];
}

+ (id) makeGtkGammaCurve : (GtkGammaCurve *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkGammaCurve:castitem];

  return retval;
}

- init
{
  return [self castGtkGammaCurve:GTK_GAMMA_CURVE(gtk_gamma_curve_new())];
}

@end
