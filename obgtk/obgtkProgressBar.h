#ifndef OBGTK_PROGRESSBAR_H
#define OBGTK_PROGRESSBAR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkProgress.h>
#include <gtk/gtkprogressbar.h>

@interface Gtk_ProgressBar : Gtk_Progress
{
@public
  GtkProgressBar *gtkprogressbar;
}
- castGtkProgressBar : (GtkProgressBar *) castitem;
+ (id)makeGtkProgressBar : (GtkProgressBar *) castitem;
- set_activity_blocks
 : (guint) blocks;
- update
 : (gfloat) percentage;
- set_orientation
 : (GtkProgressBarOrientation) orientation;
- set_discrete_blocks
 : (guint) blocks;
- set_bar_style
 : (GtkProgressBarStyle) style;
- init;
- set_activity_step
 : (guint) step;
- initWithAdjustment
 : (id) adjustment;
@end

#endif /* OBGTK_PROGRESSBAR_H */
