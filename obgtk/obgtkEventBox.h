#ifndef OBGTK_EVENTBOX_H
#define OBGTK_EVENTBOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkeventbox.h>

@interface Gtk_EventBox : Gtk_Bin
{
@public
  GtkEventBox *gtkeventbox;
}
- castGtkEventBox : (GtkEventBox *) castitem;
+ (id)makeGtkEventBox : (GtkEventBox *) castitem;
- init;
@end

#endif /* OBGTK_EVENTBOX_H */
