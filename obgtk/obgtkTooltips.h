#ifndef OBGTK_TOOLTIPS_H
#define OBGTK_TOOLTIPS_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkData.h>
#include <gtk/gtktooltips.h>

@interface Gtk_Tooltips : Gtk_Data
{
@public
  GtkTooltips *gtktooltips;
}
- castGtkTooltips : (GtkTooltips *) castitem;
+ (id)makeGtkTooltips : (GtkTooltips *) castitem;
- disable;
- force_window;
- enable;
- set_tip
 : (id) widget
 : (const gchar *) tip_text
 : (const gchar *) tip_private;
- init;
- set_delay
 : (guint) delay;
- set_colors
 : (GdkColor *) background
 : (GdkColor *) foreground;
@end

#endif /* OBGTK_TOOLTIPS_H */
