#ifndef OBGTK_COLORSELECTIONDIALOG_H
#define OBGTK_COLORSELECTIONDIALOG_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkcolorsel.h>

@interface Gtk_ColorSelectionDialog : Gtk_Window
{
@public
  GtkColorSelectionDialog *gtkcolorselectiondialog;
}
- castGtkColorSelectionDialog : (GtkColorSelectionDialog *) castitem;
+ (id)makeGtkColorSelectionDialog : (GtkColorSelectionDialog *) castitem;
- initWithColorSelectionDialogInfo
 : (const gchar *) title;
@end

#endif /* OBGTK_COLORSELECTIONDIALOG_H */
