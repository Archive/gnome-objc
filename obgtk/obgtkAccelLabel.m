#include "obgtkAccelLabel.h"

@implementation Gtk_AccelLabel

- castGtkAccelLabel : (GtkAccelLabel *) castitem
{
  gtkaccellabel = castitem;
  return [super castGtkLabel:GTK_LABEL(castitem)];
}

+ (id) makeGtkAccelLabel : (GtkAccelLabel *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkAccelLabel:castitem];

  return retval;
}

- set_accel_widget
 : (id) accel_widget
{
  g_return_val_if_fail([accel_widget isKindOf: [Gtk_Widget class]], 0);
  gtk_accel_label_set_accel_widget(gtkaccellabel, ((Gtk_Widget *)accel_widget)->gtkwidget);
  return self;
}

- initWithAccelLabelInfo
 : (const gchar *) string
{
  return [self castGtkAccelLabel:GTK_ACCEL_LABEL(gtk_accel_label_new(string))];
}

- (guint) get_accel_width
{
  return gtk_accel_label_get_accel_width(gtkaccellabel);
}

- (gboolean) refetch
{
  return gtk_accel_label_refetch(gtkaccellabel);
}

@end
