#include "obgtkTreeItem.h"

@implementation Gtk_TreeItem

- castGtkTreeItem : (GtkTreeItem *) castitem
{
  gtktreeitem = castitem;
  return [super castGtkItem:GTK_ITEM(castitem)];
}

+ (id) makeGtkTreeItem : (GtkTreeItem *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkTreeItem:castitem];

  return retval;
}

- collapse
{
  gtk_tree_item_collapse(gtktreeitem);
  return self;
}

- remove_subtree
{
  gtk_tree_item_remove_subtree(gtktreeitem);
  return self;
}

- initWithLabel
 : (gchar *) label
{
  return [self castGtkTreeItem:GTK_TREE_ITEM(gtk_tree_item_new_with_label(label))];
}

- deselect
{
  gtk_tree_item_deselect(gtktreeitem);
  return self;
}

- set_subtree
 : (id) subtree
{
  g_return_val_if_fail([subtree isKindOf: [Gtk_Widget class]], 0);
  gtk_tree_item_set_subtree(gtktreeitem, ((Gtk_Widget *)subtree)->gtkwidget);
  return self;
}

- select
{
  gtk_tree_item_select(gtktreeitem);
  return self;
}

- expand
{
  gtk_tree_item_expand(gtktreeitem);
  return self;
}

- init
{
  return [self castGtkTreeItem:GTK_TREE_ITEM(gtk_tree_item_new())];
}

@end
