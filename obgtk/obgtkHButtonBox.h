#ifndef OBGTK_HBUTTONBOX_H
#define OBGTK_HBUTTONBOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkButtonBox.h>
#include <gtk/gtkhbbox.h>

@interface Gtk_HButtonBox : Gtk_ButtonBox
{
@public
  GtkHButtonBox *gtkhbuttonbox;
}
- castGtkHButtonBox : (GtkHButtonBox *) castitem;
+ (id)makeGtkHButtonBox : (GtkHButtonBox *) castitem;
- init;
@end

#endif /* OBGTK_HBUTTONBOX_H */
