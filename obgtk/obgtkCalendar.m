#include "obgtkCalendar.h"

@implementation Gtk_Calendar

- castGtkCalendar : (GtkCalendar *) castitem
{
  gtkcalendar = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkCalendar : (GtkCalendar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkCalendar:castitem];

  return retval;
}

- freeze
{
  gtk_calendar_freeze(gtkcalendar);
  return self;
}

- get_date
 : (guint *) year
 : (guint *) month
 : (guint *) day
{
  gtk_calendar_get_date(gtkcalendar, year, month, day);
  return self;
}

- (gint) mark_day
 : (guint) day
{
  return gtk_calendar_mark_day(gtkcalendar, day);
}

- (gint) select_month
 : (guint) month
 : (guint) year
{
  return gtk_calendar_select_month(gtkcalendar, month, year);
}

- init
{
  return [self castGtkCalendar:GTK_CALENDAR(gtk_calendar_new())];
}

- thaw
{
  gtk_calendar_thaw(gtkcalendar);
  return self;
}

- display_options
 : (GtkCalendarDisplayOptions) flags
{
  gtk_calendar_display_options(gtkcalendar, flags);
  return self;
}

- clear_marks
{
  gtk_calendar_clear_marks(gtkcalendar);
  return self;
}

- (gint) unmark_day
 : (guint) day
{
  return gtk_calendar_unmark_day(gtkcalendar, day);
}

- select_day
 : (guint) day
{
  gtk_calendar_select_day(gtkcalendar, day);
  return self;
}

@end
