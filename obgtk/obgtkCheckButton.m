#include "obgtkCheckButton.h"

@implementation Gtk_CheckButton

- castGtkCheckButton : (GtkCheckButton *) castitem
{
  gtkcheckbutton = castitem;
  return [super castGtkToggleButton:GTK_TOGGLE_BUTTON(castitem)];
}

+ (id) makeGtkCheckButton : (GtkCheckButton *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkCheckButton:castitem];

  return retval;
}

- init
{
  return [self castGtkCheckButton:GTK_CHECK_BUTTON(gtk_check_button_new())];
}

- initWithLabel
 : (const gchar *) label
{
  return [self castGtkCheckButton:GTK_CHECK_BUTTON(gtk_check_button_new_with_label(label))];
}

@end
