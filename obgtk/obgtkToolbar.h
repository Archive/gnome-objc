#ifndef OBGTK_TOOLBAR_H
#define OBGTK_TOOLBAR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtktoolbar.h>

@interface Gtk_Toolbar : Gtk_Container
{
@public
  GtkToolbar *gtktoolbar;
}
- castGtkToolbar : (GtkToolbar *) castitem;
+ (id)makeGtkToolbar : (GtkToolbar *) castitem;
- set_space_size
 : (gint) space_size;
- (id) prepend_element
 : (GtkToolbarChildType) type
 : (id) widget
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data;
- prepend_space;
- initWithToolbarInfo
 : (GtkOrientation) orientation
 : (GtkToolbarStyle) style;
- insert_widget
 : (id) widget
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (gint) position;
- (id) append_element
 : (GtkToolbarChildType) type
 : (id) widget
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data;
- set_orientation
 : (GtkOrientation) orientation;
- set_style
 : (GtkToolbarStyle) style;
- prepend_widget
 : (id) widget
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text;
- append_space;
- (id) insert_item
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
 : (gint) position;
- set_space_style
 : (GtkToolbarSpaceStyle) space_style;
- (id) prepend_item
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data;
- (GtkReliefStyle) get_button_relief;
- append_widget
 : (id) widget
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text;
- set_button_relief
 : (GtkReliefStyle) relief;
- set_tooltips
 : (gint) enable;
- (id) insert_element
 : (GtkToolbarChildType) type
 : (id) widget
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data
 : (gint) position;
- insert_space
 : (gint) position;
- (id) append_item
 : (const char *) text
 : (const char *) tooltip_text
 : (const char *) tooltip_private_text
 : (id) icon
 : (GtkSignalFunc) callback
 : (gpointer) user_data;
@end

#endif /* OBGTK_TOOLBAR_H */
