#ifndef OBGTK_WIDGET_H
#define OBGTK_WIDGET_H 1
#include <obgtk/obgtkAccelGroup.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkObject.h>
#include <gtk/gtkwidget.h>

@interface Gtk_Widget : Gtk_Object
{
@public
  GtkWidget *gtkwidget;
}
- castGtkWidget : (GtkWidget *) castitem;
+ (id)makeGtkWidget : (GtkWidget *) castitem;
- ensure_style;
- unlock_accelerators;
- queue_clear_area
 : (gint) x
 : (gint) y
 : (gint) width
 : (gint) height;
- reset_shapes;
- draw
 : (GdkRectangle *) area;
- (GtkWidget*) get_toplevel;
- (guint) accelerator_signal
 : (id) accel_group
 : (guint) accel_key
 : (guint) accel_mods;
- realize;
- draw_focus;
- unrealize;
- class_path
 : (guint *) path_length
 : (gchar * *) path
 : (gchar * *) path_reversed;
- add_events
 : (gint) events;
- set_usize
 : (gint) width
 : (gint) height;
- set_parent_window
 : (GdkWindow *) parent_window;
- set_visual
 : (GdkVisual *) visual;
- (gint) get_events;
- reparent
 : (GtkWidget *) new_parent;
- (GdkVisual*) get_visual;
- show_all;
- popup
 : (gint) x
 : (gint) y;
- show;
- destroyed
 : (GtkWidget * *) widget_pointer;
- set_state
 : (GtkStateType) state;
- (gint) intersect
 : (GdkRectangle *) area
 : (GdkRectangle *) intersection;
- set_extension_events
 : (GdkExtensionMode) mode;
- getv
 : (guint) nargs
 : (GtkArg *) args;
- remove_accelerator
 : (id) accel_group
 : (guint) accel_key
 : (guint) accel_mods;
- queue_clear;
- set_colormap
 : (GdkColormap *) colormap;
- set_name
 : (const gchar *) name;
- get
 : (GtkArg *) arg;
- (gint) event
 : (GdkEvent *) event;
- queue_draw;
- set_events
 : (gint) events;
- setv
 : (guint) nargs
 : (GtkArg *) args;
- show_now;
- add_accelerator
 : (const gchar *) accel_signal
 : (id) accel_group
 : (guint) accel_key
 : (guint) accel_mods
 : (GtkAccelFlags) accel_flags;
- (gint) is_ancestor
 : (GtkWidget *) ancestor;
- reset_rc_styles;
- set_composite_name
 : (gchar *) name;
- set_app_paintable
 : (gboolean) app_paintable;
- (GtkStyle*) get_style;
- grab_default;
- lock_accelerators;
- (gboolean) activate;
- (GdkWindow *) get_parent_window;
- size_allocate
 : (GtkAllocation *) allocation;
- (GdkColormap*) get_colormap;
- (gint) hide_on_delete;
- destroy;
- queue_draw_area
 : (gint) x
 : (gint) y
 : (gint) width
 : (gint) height;
- set_rc_style;
- hide_all;
- (gboolean) set_scroll_adjustments
 : (id) hadjustment
 : (id) vadjustment;
- set_sensitive
 : (gboolean) sensitive;
- shape_combine_mask
 : (GdkBitmap *) shape_mask
 : (gint) offset_x
 : (gint) offset_y;
- queue_resize;
- (GdkExtensionMode) get_extension_events;
- size_request
 : (GtkRequisition *) requisition;
- (GtkWidget*) get_ancestor
 : (GtkType) widget_type;
- hide;
- grab_focus;
- (gchar*) get_name;
- map;
- get_pointer
 : (gint *) x
 : (gint *) y;
- unmap;
- set_uposition
 : (gint) x
 : (gint) y;
- initWithWidgetInfo
 : (GtkType) type
 : (guint) nargs
 : (GtkArg *) args;
- modify_style
 : (GtkRcStyle *) style;
- unparent;
- (gchar*) get_composite_name;
- get_child_requisition
 : (GtkRequisition *) requisition;
- draw_default;
- restore_default_style;
- set_style
 : (GtkStyle *) style;
- path
 : (guint *) path_length
 : (gchar * *) path
 : (gchar * *) path_reversed;
- ref;
- remove_accelerators
 : (const gchar *) accel_signal
 : (gboolean) visible_only;
- (gboolean) accelerators_locked;
- set_parent
 : (GtkWidget *) parent;
- unref;
@end

#endif /* OBGTK_WIDGET_H */
