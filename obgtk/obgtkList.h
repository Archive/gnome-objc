#ifndef OBGTK_LIST_H
#define OBGTK_LIST_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtklist.h>

@interface Gtk_List : Gtk_Container
{
@public
  GtkList *gtklist;
}
- castGtkList : (GtkList *) castitem;
+ (id)makeGtkList : (GtkList *) castitem;
- undo_selection;
- unselect_all;
- select_item
 : (gint) item;
- end_selection;
- extend_selection
 : (GtkScrollType) scroll_type
 : (gfloat) position
 : (gboolean) auto_start_selection;
- (gint) child_position
 : (id) child;
- unselect_item
 : (gint) item;
- insert_items
 : (GList *) items
 : (gint) position;
- remove_items_no_unref
 : (GList *) items;
- select_child
 : (id) child;
- toggle_row
 : (id) item;
- scroll_horizontal
 : (GtkScrollType) scroll_type
 : (gfloat) position;
- clear_items
 : (gint) start
 : (gint) end;
- prepend_items
 : (GList *) items;
- remove_items
 : (GList *) items;
- unselect_child
 : (id) child;
- toggle_add_mode;
- start_selection;
- append_items
 : (GList *) items;
- init;
- set_selection_mode
 : (GtkSelectionMode) mode;
- toggle_focus_row;
- select_all;
- end_drag_selection;
- scroll_vertical
 : (GtkScrollType) scroll_type
 : (gfloat) position;
@end

#endif /* OBGTK_LIST_H */
