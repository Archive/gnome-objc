#include "obgtkEventBox.h"

@implementation Gtk_EventBox

- castGtkEventBox : (GtkEventBox *) castitem
{
  gtkeventbox = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkEventBox : (GtkEventBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkEventBox:castitem];

  return retval;
}

- init
{
  return [self castGtkEventBox:GTK_EVENT_BOX(gtk_event_box_new())];
}

@end
