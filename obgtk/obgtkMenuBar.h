#ifndef OBGTK_MENUBAR_H
#define OBGTK_MENUBAR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkMenuShell.h>
#include <gtk/gtkmenubar.h>

@interface Gtk_MenuBar : Gtk_MenuShell
{
@public
  GtkMenuBar *gtkmenubar;
}
- castGtkMenuBar : (GtkMenuBar *) castitem;
+ (id)makeGtkMenuBar : (GtkMenuBar *) castitem;
- prepend
 : (id) child;
- init;
- insert
 : (id) child
 : (gint) position;
- append
 : (id) child;
- set_shadow_type
 : (GtkShadowType) type;
@end

#endif /* OBGTK_MENUBAR_H */
