#include "obgtkTree.h"

@implementation Gtk_Tree

- castGtkTree : (GtkTree *) castitem
{
  gtktree = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkTree : (GtkTree *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkTree:castitem];

  return retval;
}

- unselect_item
 : (gint) item
{
  gtk_tree_unselect_item(gtktree, item);
  return self;
}

- remove_item
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_tree_remove_item(gtktree, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- select_child
 : (id) tree_item
{
  g_return_val_if_fail([tree_item isKindOf: [Gtk_Widget class]], 0);
  gtk_tree_select_child(gtktree, ((Gtk_Widget *)tree_item)->gtkwidget);
  return self;
}

- unselect_child
 : (id) tree_item
{
  g_return_val_if_fail([tree_item isKindOf: [Gtk_Widget class]], 0);
  gtk_tree_unselect_child(gtktree, ((Gtk_Widget *)tree_item)->gtkwidget);
  return self;
}

- insert
 : (id) tree_item
 : (gint) position
{
  g_return_val_if_fail([tree_item isKindOf: [Gtk_Widget class]], 0);
  gtk_tree_insert(gtktree, ((Gtk_Widget *)tree_item)->gtkwidget, position);
  return self;
}

- clear_items
 : (gint) start
 : (gint) end
{
  gtk_tree_clear_items(gtktree, start, end);
  return self;
}

- remove_items
 : (GList *) items
{
  gtk_tree_remove_items(gtktree, items);
  return self;
}

- prepend
 : (id) tree_item
{
  g_return_val_if_fail([tree_item isKindOf: [Gtk_Widget class]], 0);
  gtk_tree_prepend(gtktree, ((Gtk_Widget *)tree_item)->gtkwidget);
  return self;
}

- set_view_lines
 : (guint) flag
{
  gtk_tree_set_view_lines(gtktree, flag);
  return self;
}

- init
{
  return [self castGtkTree:GTK_TREE(gtk_tree_new())];
}

- set_view_mode
 : (GtkTreeViewMode) mode
{
  gtk_tree_set_view_mode(gtktree, mode);
  return self;
}

- append
 : (id) tree_item
{
  g_return_val_if_fail([tree_item isKindOf: [Gtk_Widget class]], 0);
  gtk_tree_append(gtktree, ((Gtk_Widget *)tree_item)->gtkwidget);
  return self;
}

- set_selection_mode
 : (GtkSelectionMode) mode
{
  gtk_tree_set_selection_mode(gtktree, mode);
  return self;
}

- select_item
 : (gint) item
{
  gtk_tree_select_item(gtktree, item);
  return self;
}

- (gint) child_position
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  return gtk_tree_child_position(gtktree, ((Gtk_Widget *)child)->gtkwidget);
}

@end
