#ifndef OBGTK_VBUTTONBOX_H
#define OBGTK_VBUTTONBOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkButtonBox.h>
#include <gtk/gtkvbbox.h>

@interface Gtk_VButtonBox : Gtk_ButtonBox
{
@public
  GtkVButtonBox *gtkvbuttonbox;
}
- castGtkVButtonBox : (GtkVButtonBox *) castitem;
+ (id)makeGtkVButtonBox : (GtkVButtonBox *) castitem;
- init;
@end

#endif /* OBGTK_VBUTTONBOX_H */
