#ifndef OBGTK_SCALE_H
#define OBGTK_SCALE_H 1
#include <obgtk/obgtkRange.h>
#include <gtk/gtkscale.h>

@interface Gtk_Scale : Gtk_Range
{
@public
  GtkScale *gtkscale;
}
- castGtkScale : (GtkScale *) castitem;
+ (id)makeGtkScale : (GtkScale *) castitem;
- set_digits
 : (gint) digits;
- draw_value;
- set_draw_value
 : (gboolean) draw_value;
- (gint) get_value_width;
- set_value_pos
 : (GtkPositionType) pos;
@end

#endif /* OBGTK_SCALE_H */
