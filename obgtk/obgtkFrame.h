#ifndef OBGTK_FRAME_H
#define OBGTK_FRAME_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkframe.h>

@interface Gtk_Frame : Gtk_Bin
{
@public
  GtkFrame *gtkframe;
}
- castGtkFrame : (GtkFrame *) castitem;
+ (id)makeGtkFrame : (GtkFrame *) castitem;
- set_label
 : (const gchar *) label;
- set_shadow_type
 : (GtkShadowType) type;
- set_label_align
 : (gfloat) xalign
 : (gfloat) yalign;
- initWithFrameInfo
 : (const gchar *) label;
@end

#endif /* OBGTK_FRAME_H */
