#ifndef OBGTK_RADIOMENUITEM_H
#define OBGTK_RADIOMENUITEM_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkCheckMenuItem.h>
#include <gtk/gtkradiomenuitem.h>

@interface Gtk_RadioMenuItem : Gtk_CheckMenuItem
{
@public
  GtkRadioMenuItem *gtkradiomenuitem;
}
- castGtkRadioMenuItem : (GtkRadioMenuItem *) castitem;
+ (id)makeGtkRadioMenuItem : (GtkRadioMenuItem *) castitem;
- set_group
 : (GSList *) group;
- initWithLabel
 : (GSList *) group
 : (const gchar *) label;
- (GSList*) group;
- initWithRadioMenuItemInfo
 : (GSList *) group;
@end

#endif /* OBGTK_RADIOMENUITEM_H */
