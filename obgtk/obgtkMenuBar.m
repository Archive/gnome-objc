#include "obgtkMenuBar.h"

@implementation Gtk_MenuBar

- castGtkMenuBar : (GtkMenuBar *) castitem
{
  gtkmenubar = castitem;
  return [super castGtkMenuShell:GTK_MENU_SHELL(castitem)];
}

+ (id) makeGtkMenuBar : (GtkMenuBar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkMenuBar:castitem];

  return retval;
}

- prepend
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_bar_prepend(gtkmenubar, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- init
{
  return [self castGtkMenuBar:GTK_MENU_BAR(gtk_menu_bar_new())];
}

- insert
 : (id) child
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_bar_insert(gtkmenubar, ((Gtk_Widget *)child)->gtkwidget, position);
  return self;
}

- append
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_bar_append(gtkmenubar, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- set_shadow_type
 : (GtkShadowType) type
{
  gtk_menu_bar_set_shadow_type(gtkmenubar, type);
  return self;
}

@end
