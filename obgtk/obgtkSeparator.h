#ifndef OBGTK_SEPARATOR_H
#define OBGTK_SEPARATOR_H 1
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkseparator.h>

@interface Gtk_Separator : Gtk_Widget
{
@public
  GtkSeparator *gtkseparator;
}
- castGtkSeparator : (GtkSeparator *) castitem;
+ (id)makeGtkSeparator : (GtkSeparator *) castitem;
@end

#endif /* OBGTK_SEPARATOR_H */
