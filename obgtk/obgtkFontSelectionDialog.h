#ifndef OBGTK_FONTSELECTIONDIALOG_H
#define OBGTK_FONTSELECTIONDIALOG_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkfontsel.h>

@interface Gtk_FontSelectionDialog : Gtk_Window
{
@public
  GtkFontSelectionDialog *gtkfontselectiondialog;
}
- castGtkFontSelectionDialog : (GtkFontSelectionDialog *) castitem;
+ (id)makeGtkFontSelectionDialog : (GtkFontSelectionDialog *) castitem;
- initWithFontSelectionDialogInfo
 : (const gchar *) title;
- (GdkFont*) get_font;
- set_filter
 : (GtkFontFilterType) filter_type
 : (GtkFontType) font_type
 : (gchar * *) foundries
 : (gchar * *) weights
 : (gchar * *) slants
 : (gchar * *) setwidths
 : (gchar * *) spacings
 : (gchar * *) charsets;
- (gboolean) set_font_name
 : (const gchar *) fontname;
- (gchar*) get_preview_text;
- (gchar*) get_font_name;
- set_preview_text
 : (const gchar *) text;
@end

#endif /* OBGTK_FONTSELECTIONDIALOG_H */
