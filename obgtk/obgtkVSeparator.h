#ifndef OBGTK_VSEPARATOR_H
#define OBGTK_VSEPARATOR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkSeparator.h>
#include <gtk/gtkvseparator.h>

@interface Gtk_VSeparator : Gtk_Separator
{
@public
  GtkVSeparator *gtkvseparator;
}
- castGtkVSeparator : (GtkVSeparator *) castitem;
+ (id)makeGtkVSeparator : (GtkVSeparator *) castitem;
- init;
@end

#endif /* OBGTK_VSEPARATOR_H */
