#ifndef OBGTK_HBOX_H
#define OBGTK_HBOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBox.h>
#include <gtk/gtkhbox.h>

@interface Gtk_HBox : Gtk_Box
{
@public
  GtkHBox *gtkhbox;
}
- castGtkHBox : (GtkHBox *) castitem;
+ (id)makeGtkHBox : (GtkHBox *) castitem;
- initWithHBoxInfo
 : (gboolean) homogeneous
 : (gint) spacing;
@end

#endif /* OBGTK_HBOX_H */
