#ifndef OBGTK_RULER_H
#define OBGTK_RULER_H 1
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkruler.h>

@interface Gtk_Ruler : Gtk_Widget
{
@public
  GtkRuler *gtkruler;
}
- castGtkRuler : (GtkRuler *) castitem;
+ (id)makeGtkRuler : (GtkRuler *) castitem;
- draw_ticks;
- set_metric
 : (GtkMetricType) metric;
- set_range
 : (gfloat) lower
 : (gfloat) upper
 : (gfloat) position
 : (gfloat) max_size;
- draw_pos;
@end

#endif /* OBGTK_RULER_H */
