#ifndef OBGTK_WINDOW_H
#define OBGTK_WINDOW_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAccelGroup.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkwindow.h>

@interface Gtk_Window : Gtk_Bin
{
@public
  GtkWindow *gtkwindow;
}
- castGtkWindow : (GtkWindow *) castitem;
+ (id)makeGtkWindow : (GtkWindow *) castitem;
- set_policy
 : (gint) allow_shrink
 : (gint) allow_grow
 : (gint) auto_shrink;
- set_transient_for
 : (GtkWindow *) parent;
- set_focus
 : (id) focus;
- set_default
 : (id) defaultw;
- add_embedded_xid
 : (guint) xid;
- initWithWindowInfo
 : (GtkWindowType) type;
- (gint) activate_focus;
- set_title
 : (const gchar *) title;
- add_accel_group
 : (id) accel_group;
- set_wmclass
 : (const gchar *) wmclass_name
 : (const gchar *) wmclass_class;
- remove_embedded_xid
 : (guint) xid;
- set_position
 : (GtkWindowPosition) position;
- (gint) activate_default;
- remove_accel_group
 : (id) accel_group;
- set_modal
 : (gboolean) modal;
- set_default_size
 : (gint) width
 : (gint) height;
- set_geometry_hints
 : (id) geometry_widget
 : (GdkGeometry *) geometry
 : (GdkWindowHints) geom_mask;
@end

#endif /* OBGTK_WINDOW_H */
