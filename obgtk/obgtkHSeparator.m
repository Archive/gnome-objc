#include "obgtkHSeparator.h"

@implementation Gtk_HSeparator

- castGtkHSeparator : (GtkHSeparator *) castitem
{
  gtkhseparator = castitem;
  return [super castGtkSeparator:GTK_SEPARATOR(castitem)];
}

+ (id) makeGtkHSeparator : (GtkHSeparator *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHSeparator:castitem];

  return retval;
}

- init
{
  return [self castGtkHSeparator:GTK_HSEPARATOR(gtk_hseparator_new())];
}

@end
