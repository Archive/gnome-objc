#include "obgtkStatusbar.h"

@implementation Gtk_Statusbar

- castGtkStatusbar : (GtkStatusbar *) castitem
{
  gtkstatusbar = castitem;
  return [super castGtkHBox:GTK_HBOX(castitem)];
}

+ (id) makeGtkStatusbar : (GtkStatusbar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkStatusbar:castitem];

  return retval;
}

- pop
 : (guint) context_id
{
  gtk_statusbar_pop(gtkstatusbar, context_id);
  return self;
}

- init
{
  return [self castGtkStatusbar:GTK_STATUSBAR(gtk_statusbar_new())];
}

- remove
 : (guint) context_id
 : (guint) message_id
{
  gtk_statusbar_remove(gtkstatusbar, context_id, message_id);
  return self;
}

- (guint) get_context_id
 : (const gchar *) context_description
{
  return gtk_statusbar_get_context_id(gtkstatusbar, context_description);
}

- (guint) push
 : (guint) context_id
 : (const gchar *) text
{
  return gtk_statusbar_push(gtkstatusbar, context_id, text);
}

@end
