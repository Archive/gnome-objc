#include "obgtkBox.h"

@implementation Gtk_Box

- castGtkBox : (GtkBox *) castitem
{
  gtkbox = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkBox : (GtkBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkBox:castitem];

  return retval;
}

- set_spacing
 : (gint) spacing
{
  gtk_box_set_spacing(gtkbox, spacing);
  return self;
}

- pack_start_defaults
 : (id) widget
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_box_pack_start_defaults(gtkbox, ((Gtk_Widget *)widget)->gtkwidget);
  return self;
}

- pack_start
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (guint) padding
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_box_pack_start(gtkbox, ((Gtk_Widget *)child)->gtkwidget, expand, fill, padding);
  return self;
}

- set_child_packing
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (guint) padding
 : (GtkPackType) pack_type
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_box_set_child_packing(gtkbox, ((Gtk_Widget *)child)->gtkwidget, expand, fill, padding, pack_type);
  return self;
}

- query_child_packing
 : (id) child
 : (gboolean *) expand
 : (gboolean *) fill
 : (guint *) padding
 : (GtkPackType *) pack_type
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_box_query_child_packing(gtkbox, ((Gtk_Widget *)child)->gtkwidget, expand, fill, padding, pack_type);
  return self;
}

- reorder_child
 : (id) child
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_box_reorder_child(gtkbox, ((Gtk_Widget *)child)->gtkwidget, position);
  return self;
}

- pack_end
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (guint) padding
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_box_pack_end(gtkbox, ((Gtk_Widget *)child)->gtkwidget, expand, fill, padding);
  return self;
}

- pack_end_defaults
 : (id) widget
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_box_pack_end_defaults(gtkbox, ((Gtk_Widget *)widget)->gtkwidget);
  return self;
}

- set_homogeneous
 : (gboolean) homogeneous
{
  gtk_box_set_homogeneous(gtkbox, homogeneous);
  return self;
}

@end
