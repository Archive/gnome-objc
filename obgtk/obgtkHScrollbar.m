#include "obgtkHScrollbar.h"

@implementation Gtk_HScrollbar

- castGtkHScrollbar : (GtkHScrollbar *) castitem
{
  gtkhscrollbar = castitem;
  return [super castGtkScrollbar:GTK_SCROLLBAR(castitem)];
}

+ (id) makeGtkHScrollbar : (GtkHScrollbar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHScrollbar:castitem];

  return retval;
}

- initWithHScrollbarInfo
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkHScrollbar:GTK_HSCROLLBAR(gtk_hscrollbar_new(((Gtk_Adjustment *)adjustment)->gtkadjustment))];
}

@end
