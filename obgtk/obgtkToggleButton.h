#ifndef OBGTK_TOGGLEBUTTON_H
#define OBGTK_TOGGLEBUTTON_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkButton.h>
#include <gtk/gtktogglebutton.h>

@interface Gtk_ToggleButton : Gtk_Button
{
@public
  GtkToggleButton *gtktogglebutton;
}
- castGtkToggleButton : (GtkToggleButton *) castitem;
+ (id)makeGtkToggleButton : (GtkToggleButton *) castitem;
- toggled;
- (gboolean) get_active;
- set_active
 : (gboolean) is_active;
- initWithLabel
 : (const gchar *) label;
- init;
- set_mode
 : (gboolean) draw_indicator;
@end

#endif /* OBGTK_TOGGLEBUTTON_H */
