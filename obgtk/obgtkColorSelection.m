#include "obgtkColorSelection.h"

@implementation Gtk_ColorSelection

- castGtkColorSelection : (GtkColorSelection *) castitem
{
  gtkcolorselection = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkColorSelection : (GtkColorSelection *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkColorSelection:castitem];

  return retval;
}

- set_update_policy
 : (GtkUpdateType) policy
{
  gtk_color_selection_set_update_policy(gtkcolorselection, policy);
  return self;
}

- get_color
 : (gdouble *) color
{
  gtk_color_selection_get_color(gtkcolorselection, color);
  return self;
}

- init
{
  return [self castGtkColorSelection:GTK_COLOR_SELECTION(gtk_color_selection_new())];
}

- set_opacity
 : (gint) use_opacity
{
  gtk_color_selection_set_opacity(gtkcolorselection, use_opacity);
  return self;
}

- initWithColorSelectionInfo
 : (const gchar *) title
{
  return [self castGtkColorSelection:GTK_COLOR_SELECTION(gtk_color_selection_dialog_new(title))];
}

- set_color
 : (gdouble *) color
{
  gtk_color_selection_set_color(gtkcolorselection, color);
  return self;
}

@end
