#include "obgtkMisc.h"

@implementation Gtk_Misc

- castGtkMisc : (GtkMisc *) castitem
{
  gtkmisc = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkMisc : (GtkMisc *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkMisc:castitem];

  return retval;
}

- set_alignment
 : (gfloat) xalign
 : (gfloat) yalign
{
  gtk_misc_set_alignment(gtkmisc, xalign, yalign);
  return self;
}

- set_padding
 : (gint) xpad
 : (gint) ypad
{
  gtk_misc_set_padding(gtkmisc, xpad, ypad);
  return self;
}

@end
