#include "obgtkEntry.h"

@implementation Gtk_Entry

- castGtkEntry : (GtkEntry *) castitem
{
  gtkentry = castitem;
  return [super castGtkEditable:GTK_EDITABLE(castitem)];
}

+ (id) makeGtkEntry : (GtkEntry *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkEntry:castitem];

  return retval;
}

- prepend_text
 : (const gchar *) text
{
  gtk_entry_prepend_text(gtkentry, text);
  return self;
}

- init
{
  return [self castGtkEntry:GTK_ENTRY(gtk_entry_new())];
}

- initWithMaxLength
 : (guint16) max
{
  return [self castGtkEntry:GTK_ENTRY(gtk_entry_new_with_max_length(max))];
}

- (gchar*) get_text
{
  return gtk_entry_get_text(gtkentry);
}

- set_max_length
 : (guint16) max
{
  gtk_entry_set_max_length(gtkentry, max);
  return self;
}

- set_editable
 : (gboolean) editable
{
  gtk_entry_set_editable(gtkentry, editable);
  return self;
}

- append_text
 : (const gchar *) text
{
  gtk_entry_append_text(gtkentry, text);
  return self;
}

- set_position
 : (gint) position
{
  gtk_entry_set_position(gtkentry, position);
  return self;
}

- set_visibility
 : (gboolean) visible
{
  gtk_entry_set_visibility(gtkentry, visible);
  return self;
}

- select_region
 : (gint) start
 : (gint) end
{
  gtk_entry_select_region(gtkentry, start, end);
  return self;
}

- set_text
 : (const gchar *) text
{
  gtk_entry_set_text(gtkentry, text);
  return self;
}

@end
