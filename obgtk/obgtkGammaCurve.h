#ifndef OBGTK_GAMMACURVE_H
#define OBGTK_GAMMACURVE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkVBox.h>
#include <gtk/gtkgamma.h>

@interface Gtk_GammaCurve : Gtk_VBox
{
@public
  GtkGammaCurve *gtkgammacurve;
}
- castGtkGammaCurve : (GtkGammaCurve *) castitem;
+ (id)makeGtkGammaCurve : (GtkGammaCurve *) castitem;
- init;
@end

#endif /* OBGTK_GAMMACURVE_H */
