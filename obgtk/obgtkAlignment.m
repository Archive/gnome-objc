#include "obgtkAlignment.h"

@implementation Gtk_Alignment

- castGtkAlignment : (GtkAlignment *) castitem
{
  gtkalignment = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkAlignment : (GtkAlignment *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkAlignment:castitem];

  return retval;
}

- initWithAlignmentInfo
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) xscale
 : (gfloat) yscale
{
  return [self castGtkAlignment:GTK_ALIGNMENT(gtk_alignment_new(xalign, yalign, xscale, yscale))];
}

- set
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) xscale
 : (gfloat) yscale
{
  gtk_alignment_set(gtkalignment, xalign, yalign, xscale, yscale);
  return self;
}

@end
