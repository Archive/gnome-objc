#include "obgtkDrawingArea.h"

@implementation Gtk_DrawingArea

- castGtkDrawingArea : (GtkDrawingArea *) castitem
{
  gtkdrawingarea = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkDrawingArea : (GtkDrawingArea *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkDrawingArea:castitem];

  return retval;
}

- size
 : (gint) width
 : (gint) height
{
  gtk_drawing_area_size(gtkdrawingarea, width, height);
  return self;
}

- init
{
  return [self castGtkDrawingArea:GTK_DRAWING_AREA(gtk_drawing_area_new())];
}

@end
