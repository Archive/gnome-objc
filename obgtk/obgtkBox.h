#ifndef OBGTK_BOX_H
#define OBGTK_BOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtkbox.h>

@interface Gtk_Box : Gtk_Container
{
@public
  GtkBox *gtkbox;
}
- castGtkBox : (GtkBox *) castitem;
+ (id)makeGtkBox : (GtkBox *) castitem;
- set_spacing
 : (gint) spacing;
- pack_start_defaults
 : (id) widget;
- pack_start
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (guint) padding;
- set_child_packing
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (guint) padding
 : (GtkPackType) pack_type;
- query_child_packing
 : (id) child
 : (gboolean *) expand
 : (gboolean *) fill
 : (guint *) padding
 : (GtkPackType *) pack_type;
- reorder_child
 : (id) child
 : (gint) position;
- pack_end
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (guint) padding;
- pack_end_defaults
 : (id) widget;
- set_homogeneous
 : (gboolean) homogeneous;
@end

#endif /* OBGTK_BOX_H */
