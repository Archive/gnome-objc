#ifndef OBGTK_VPANED_H
#define OBGTK_VPANED_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkPaned.h>
#include <gtk/gtkvpaned.h>

@interface Gtk_VPaned : Gtk_Paned
{
@public
  GtkVPaned *gtkvpaned;
}
- castGtkVPaned : (GtkVPaned *) castitem;
+ (id)makeGtkVPaned : (GtkVPaned *) castitem;
- init;
@end

#endif /* OBGTK_VPANED_H */
