#include "obgtkTable.h"

@implementation Gtk_Table

- castGtkTable : (GtkTable *) castitem
{
  gtktable = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkTable : (GtkTable *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkTable:castitem];

  return retval;
}

- set_col_spacing
 : (guint) column
 : (guint) spacing
{
  gtk_table_set_col_spacing(gtktable, column, spacing);
  return self;
}

- set_col_spacings
 : (guint) spacing
{
  gtk_table_set_col_spacings(gtktable, spacing);
  return self;
}

- set_row_spacing
 : (guint) row
 : (guint) spacing
{
  gtk_table_set_row_spacing(gtktable, row, spacing);
  return self;
}

- attach
 : (id) child
 : (guint) left_attach
 : (guint) right_attach
 : (guint) top_attach
 : (guint) bottom_attach
 : (GtkAttachOptions) xoptions
 : (GtkAttachOptions) yoptions
 : (guint) xpadding
 : (guint) ypadding
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_table_attach(gtktable, ((Gtk_Widget *)child)->gtkwidget, left_attach, right_attach, top_attach, bottom_attach, xoptions, yoptions, xpadding, ypadding);
  return self;
}

- attach_defaults
 : (id) widget
 : (guint) left_attach
 : (guint) right_attach
 : (guint) top_attach
 : (guint) bottom_attach
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_table_attach_defaults(gtktable, ((Gtk_Widget *)widget)->gtkwidget, left_attach, right_attach, top_attach, bottom_attach);
  return self;
}

- initWithTableInfo
 : (guint) rows
 : (guint) columns
 : (gboolean) homogeneous
{
  return [self castGtkTable:GTK_TABLE(gtk_table_new(rows, columns, homogeneous))];
}

- set_homogeneous
 : (gboolean) homogeneous
{
  gtk_table_set_homogeneous(gtktable, homogeneous);
  return self;
}

- set_row_spacings
 : (guint) spacing
{
  gtk_table_set_row_spacings(gtktable, spacing);
  return self;
}

- resize
 : (guint) rows
 : (guint) columns
{
  gtk_table_resize(gtktable, rows, columns);
  return self;
}

@end
