#include "obgtkProgress.h"

@implementation Gtk_Progress

- castGtkProgress : (GtkProgress *) castitem
{
  gtkprogress = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkProgress : (GtkProgress *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkProgress:castitem];

  return retval;
}

- set_activity_mode
 : (guint) activity_mode
{
  gtk_progress_set_activity_mode(gtkprogress, activity_mode);
  return self;
}

- set_percentage
 : (gfloat) percentage
{
  gtk_progress_set_percentage(gtkprogress, percentage);
  return self;
}

- (gchar*) get_current_text
{
  return gtk_progress_get_current_text(gtkprogress);
}

- set_show_text
 : (gint) show_text
{
  gtk_progress_set_show_text(gtkprogress, show_text);
  return self;
}

- configure
 : (gfloat) value
 : (gfloat) min
 : (gfloat) max
{
  gtk_progress_configure(gtkprogress, value, min, max);
  return self;
}

- set_adjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_progress_set_adjustment(gtkprogress, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- set_text_alignment
 : (gfloat) x_align
 : (gfloat) y_align
{
  gtk_progress_set_text_alignment(gtkprogress, x_align, y_align);
  return self;
}

- (gchar*) get_text_from_value
 : (gfloat) value
{
  return gtk_progress_get_text_from_value(gtkprogress, value);
}

- (gfloat) get_value
{
  return gtk_progress_get_value(gtkprogress);
}

- (gfloat) get_current_percentage
{
  return gtk_progress_get_current_percentage(gtkprogress);
}

- (gfloat) get_percentage_from_value
 : (gfloat) value
{
  return gtk_progress_get_percentage_from_value(gtkprogress, value);
}

- set_format_string
 : (gchar *) format
{
  gtk_progress_set_format_string(gtkprogress, format);
  return self;
}

- set_value
 : (gfloat) value
{
  gtk_progress_set_value(gtkprogress, value);
  return self;
}

@end
