#include "obgtkListItem.h"

@implementation Gtk_ListItem

- castGtkListItem : (GtkListItem *) castitem
{
  gtklistitem = castitem;
  return [super castGtkItem:GTK_ITEM(castitem)];
}

+ (id) makeGtkListItem : (GtkListItem *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkListItem:castitem];

  return retval;
}

- initWithLabel
 : (const gchar *) label
{
  return [self castGtkListItem:GTK_LIST_ITEM(gtk_list_item_new_with_label(label))];
}

- select
{
  gtk_list_item_select(gtklistitem);
  return self;
}

- deselect
{
  gtk_list_item_deselect(gtklistitem);
  return self;
}

- init
{
  return [self castGtkListItem:GTK_LIST_ITEM(gtk_list_item_new())];
}

@end
