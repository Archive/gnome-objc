#include "obgtkProgressBar.h"

@implementation Gtk_ProgressBar

- castGtkProgressBar : (GtkProgressBar *) castitem
{
  gtkprogressbar = castitem;
  return [super castGtkProgress:GTK_PROGRESS(castitem)];
}

+ (id) makeGtkProgressBar : (GtkProgressBar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkProgressBar:castitem];

  return retval;
}

- set_activity_blocks
 : (guint) blocks
{
  gtk_progress_bar_set_activity_blocks(gtkprogressbar, blocks);
  return self;
}

- update
 : (gfloat) percentage
{
  gtk_progress_bar_update(gtkprogressbar, percentage);
  return self;
}

- set_orientation
 : (GtkProgressBarOrientation) orientation
{
  gtk_progress_bar_set_orientation(gtkprogressbar, orientation);
  return self;
}

- set_discrete_blocks
 : (guint) blocks
{
  gtk_progress_bar_set_discrete_blocks(gtkprogressbar, blocks);
  return self;
}

- set_bar_style
 : (GtkProgressBarStyle) style
{
  gtk_progress_bar_set_bar_style(gtkprogressbar, style);
  return self;
}

- init
{
  return [self castGtkProgressBar:GTK_PROGRESS_BAR(gtk_progress_bar_new())];
}

- set_activity_step
 : (guint) step
{
  gtk_progress_bar_set_activity_step(gtkprogressbar, step);
  return self;
}

- initWithAdjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkProgressBar:GTK_PROGRESS_BAR(gtk_progress_bar_new_with_adjustment(((Gtk_Adjustment *)adjustment)->gtkadjustment))];
}

@end
