#include "obgtkPixmap.h"

@implementation Gtk_Pixmap

- castGtkPixmap : (GtkPixmap *) castitem
{
  gtkpixmap = castitem;
  return [super castGtkMisc:GTK_MISC(castitem)];
}

+ (id) makeGtkPixmap : (GtkPixmap *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkPixmap:castitem];

  return retval;
}

- initWithPixmapInfo
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask
{
  return [self castGtkPixmap:GTK_PIXMAP(gtk_pixmap_new(pixmap, mask))];
}

- set_build_insensitive
 : (guint) build
{
  gtk_pixmap_set_build_insensitive(gtkpixmap, build);
  return self;
}

- get
 : (GdkPixmap * *) val
 : (GdkBitmap * *) mask
{
  gtk_pixmap_get(gtkpixmap, val, mask);
  return self;
}

- set
 : (GdkPixmap *) val
 : (GdkBitmap *) mask
{
  gtk_pixmap_set(gtkpixmap, val, mask);
  return self;
}

@end
