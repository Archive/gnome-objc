#ifndef OBGTK_INVISIBLE_H
#define OBGTK_INVISIBLE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkinvisible.h>

@interface Gtk_Invisible : Gtk_Bin
{
@public
  GtkInvisible *gtkinvisible;
}
- castGtkInvisible : (GtkInvisible *) castitem;
+ (id)makeGtkInvisible : (GtkInvisible *) castitem;
- init;
@end

#endif /* OBGTK_INVISIBLE_H */
