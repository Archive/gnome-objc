#ifndef OBGTK_MENU_H
#define OBGTK_MENU_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAccelGroup.h>
#include <obgtk/obgtkMenuShell.h>
#include <gtk/gtkmenu.h>

@interface Gtk_Menu : Gtk_MenuShell
{
@public
  GtkMenu *gtkmenu;
}
- castGtkMenu : (GtkMenu *) castitem;
+ (id)makeGtkMenu : (GtkMenu *) castitem;
- set_title
 : (const gchar *) title;
- set_active
 : (guint) index;
- prepend
 : (id) child;
- (id) ensure_uline_accel_group;
- set_accel_group
 : (id) accel_group;
- (id) get_attach_widget;
- append
 : (id) child;
- popup
 : (id) parent_menu_shell
 : (id) parent_menu_item
 : (GtkMenuPositionFunc) func
 : (gpointer) data
 : (guint) button
 : (guint32) activate_time;
- (id) get_active;
- set_tearoff_state
 : (gboolean) torn_off;
- (id) get_uline_accel_group;
- init;
- (id) get_accel_group;
- popdown;
- detach;
- reposition;
- insert
 : (id) child
 : (gint) position;
- reorder_child
 : (id) child
 : (gint) position;
- attach_to_widget
 : (id) attach_widget
 : (GtkMenuDetachFunc) detacher;
@end

#endif /* OBGTK_MENU_H */
