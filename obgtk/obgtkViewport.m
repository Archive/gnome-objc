#include "obgtkViewport.h"

@implementation Gtk_Viewport

- castGtkViewport : (GtkViewport *) castitem
{
  gtkviewport = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkViewport : (GtkViewport *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkViewport:castitem];

  return retval;
}

- set_shadow_type
 : (GtkShadowType) type
{
  gtk_viewport_set_shadow_type(gtkviewport, type);
  return self;
}

- (id) get_vadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_viewport_get_vadjustment(gtkviewport)];
}

- set_vadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_viewport_set_vadjustment(gtkviewport, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- initWithViewportInfo
 : (id) hadjustment
 : (id) vadjustment
{
  g_return_val_if_fail([hadjustment isKindOf: [Gtk_Adjustment class]], 0);
  g_return_val_if_fail([vadjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkViewport:GTK_VIEWPORT(gtk_viewport_new(((Gtk_Adjustment *)hadjustment)->gtkadjustment, ((Gtk_Adjustment *)vadjustment)->gtkadjustment))];
}

- (id) get_hadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_viewport_get_hadjustment(gtkviewport)];
}

- set_hadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_viewport_set_hadjustment(gtkviewport, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

@end
