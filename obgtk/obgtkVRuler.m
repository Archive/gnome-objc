#include "obgtkVRuler.h"

@implementation Gtk_VRuler

- castGtkVRuler : (GtkVRuler *) castitem
{
  gtkvruler = castitem;
  return [super castGtkRuler:GTK_RULER(castitem)];
}

+ (id) makeGtkVRuler : (GtkVRuler *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkVRuler:castitem];

  return retval;
}

- init
{
  return [self castGtkVRuler:GTK_VRULER(gtk_vruler_new())];
}

@end
