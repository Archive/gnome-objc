#include "obgtkDialog.h"

@implementation Gtk_Dialog

- castGtkDialog : (GtkDialog *) castitem
{
  gtkdialog = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkDialog : (GtkDialog *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkDialog:castitem];

  return retval;
}

- init
{
  return [self castGtkDialog:GTK_DIALOG(gtk_dialog_new())];
}

@end
