#ifndef OBGTK_HSEPARATOR_H
#define OBGTK_HSEPARATOR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkSeparator.h>
#include <gtk/gtkhseparator.h>

@interface Gtk_HSeparator : Gtk_Separator
{
@public
  GtkHSeparator *gtkhseparator;
}
- castGtkHSeparator : (GtkHSeparator *) castitem;
+ (id)makeGtkHSeparator : (GtkHSeparator *) castitem;
- init;
@end

#endif /* OBGTK_HSEPARATOR_H */
