#include "obgtkAccelGroup.h"
#include "obgtkObject.h"

@implementation Gtk_AccelGroup
{
@public
  GtkAccelGroup *gtkaccelgroup;
}
- init
{
  self = [super init];
  gtkaccelgroup = gtk_accel_group_new();
  [self ref];
  return self;
}

- ref
{
  gtk_accel_group_ref(gtkaccelgroup);
  return self;
}

- unref
{
  gtk_accel_group_unref(gtkaccelgroup);
  return self;
}

- free
{
  [self unref];
  return [super free];
}

- add:(guchar) accel_key
     :(guint8) accel_mods
     :(guint8) accel_flags
     :(id) gtk_object
     :(const gchar *) signal_name
{
  gtk_accel_group_add(gtkaccelgroup,
		      accel_key,
		      accel_mods,
		      accel_flags,
		      [gtk_object getGtkObject],
		      signal_name);
  return self;
}
@end
