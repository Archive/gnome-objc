#ifndef OBGTK_FILESELECTION_H
#define OBGTK_FILESELECTION_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkfilesel.h>

@interface Gtk_FileSelection : Gtk_Window
{
@public
  GtkFileSelection *gtkfileselection;
}
- castGtkFileSelection : (GtkFileSelection *) castitem;
+ (id)makeGtkFileSelection : (GtkFileSelection *) castitem;
- set_filename
 : (const gchar *) filename;
- complete
 : (const gchar *) pattern;
- initWithFileSelectionInfo
 : (const gchar *) title;
- show_fileop_buttons;
- (gchar*) get_filename;
- hide_fileop_buttons;
@end

#endif /* OBGTK_FILESELECTION_H */
