#ifndef OBGTK_FIXED_H
#define OBGTK_FIXED_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtkfixed.h>

@interface Gtk_Fixed : Gtk_Container
{
@public
  GtkFixed *gtkfixed;
}
- castGtkFixed : (GtkFixed *) castitem;
+ (id)makeGtkFixed : (GtkFixed *) castitem;
- move
 : (id) widget
 : (gint16) x
 : (gint16) y;
- init;
- put
 : (id) widget
 : (gint16) x
 : (gint16) y;
@end

#endif /* OBGTK_FIXED_H */
