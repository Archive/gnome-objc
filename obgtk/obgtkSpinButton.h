#ifndef OBGTK_SPINBUTTON_H
#define OBGTK_SPINBUTTON_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkEntry.h>
#include <gtk/gtkspinbutton.h>

@interface Gtk_SpinButton : Gtk_Entry
{
@public
  GtkSpinButton *gtkspinbutton;
}
- castGtkSpinButton : (GtkSpinButton *) castitem;
+ (id)makeGtkSpinButton : (GtkSpinButton *) castitem;
- set_value
 : (gfloat) value;
- configure
 : (id) adjustment
 : (gfloat) climb_rate
 : (guint) digits;
- (gfloat) get_value_as_float;
- set_snap_to_ticks
 : (gboolean) snap_to_ticks;
- initWithSpinButtonInfo
 : (id) adjustment
 : (gfloat) climb_rate
 : (guint) digits;
- set_wrap
 : (gboolean) wrap;
- update;
- set_shadow_type
 : (GtkShadowType) shadow_type;
- set_numeric
 : (gboolean) numeric;
- set_adjustment
 : (id) adjustment;
- set_digits
 : (guint) digits;
- spin
 : (GtkSpinType) direction
 : (gfloat) increment;
- set_update_policy
 : (GtkSpinButtonUpdatePolicy) policy;
- (gint) get_value_as_int;
- (id) get_adjustment;
@end

#endif /* OBGTK_SPINBUTTON_H */
