#ifndef OBGTK_FONTSELECTION_H
#define OBGTK_FONTSELECTION_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkFontSelectionDialog.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkfontsel.h>

@interface Gtk_FontSelection : Gtk_Window
{
@public
  GtkFontSelection *gtkfontselection;
}
- castGtkFontSelection : (GtkFontSelection *) castitem;
+ (id)makeGtkFontSelection : (GtkFontSelection *) castitem;
- initWithFontSelectionInfo
 : (const gchar *) title;
- init;
@end

#endif /* OBGTK_FONTSELECTION_H */
