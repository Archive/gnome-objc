#ifndef OBGTK_TEAROFFMENUITEM_H
#define OBGTK_TEAROFFMENUITEM_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkMenuItem.h>
#include <gtk/gtktearoffmenuitem.h>

@interface Gtk_TearoffMenuItem : Gtk_MenuItem
{
@public
  GtkTearoffMenuItem *gtktearoffmenuitem;
}
- castGtkTearoffMenuItem : (GtkTearoffMenuItem *) castitem;
+ (id)makeGtkTearoffMenuItem : (GtkTearoffMenuItem *) castitem;
- init;
@end

#endif /* OBGTK_TEAROFFMENUITEM_H */
