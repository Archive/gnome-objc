#include "obgtkAspectFrame.h"

@implementation Gtk_AspectFrame

- castGtkAspectFrame : (GtkAspectFrame *) castitem
{
  gtkaspectframe = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkAspectFrame : (GtkAspectFrame *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkAspectFrame:castitem];

  return retval;
}

- set
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) ratio
 : (gint) obey_child
{
  gtk_aspect_frame_set(gtkaspectframe, xalign, yalign, ratio, obey_child);
  return self;
}

- initWithAspectFrameInfo
 : (const gchar *) label
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) ratio
 : (gint) obey_child
{
  return [self castGtkAspectFrame:GTK_ASPECT_FRAME(gtk_aspect_frame_new(label, xalign, yalign, ratio, obey_child))];
}

@end
