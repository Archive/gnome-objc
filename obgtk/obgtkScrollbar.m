#include "obgtkScrollbar.h"

@implementation Gtk_Scrollbar

- castGtkScrollbar : (GtkScrollbar *) castitem
{
  gtkscrollbar = castitem;
  return [super castGtkRange:GTK_RANGE(castitem)];
}

+ (id) makeGtkScrollbar : (GtkScrollbar *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkScrollbar:castitem];

  return retval;
}

@end
