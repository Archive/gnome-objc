#ifndef OBGTK_ACCEL_GROUP_H
#define OBGTK_ACCEL_GROUP_H 1

#include <objc/Object.h>
#include <gtk/gtkaccelgroup.h>

@interface Gtk_AccelGroup : Object
{
@public
  GtkAccelGroup *gtkaccelgroup;
}
- ref;
- unref;
- add:(guchar) accel_key
     :(guint8) accel_mods
     :(guint8) accel_flags
     :(id) gtk_object
     :(const gchar *) signal_name;
@end

#endif /* OBGTK_ACCELERATOR_TABLE_H */

