#ifndef OBGTK_PLUG_H
#define OBGTK_PLUG_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkplug.h>

@interface Gtk_Plug : Gtk_Window
{
@public
  GtkPlug *gtkplug;
}
- castGtkPlug : (GtkPlug *) castitem;
+ (id)makeGtkPlug : (GtkPlug *) castitem;
- construct
 : (guint32) socket_id;
- initWithPlugInfo
 : (guint32) socket_id;
@end

#endif /* OBGTK_PLUG_H */
