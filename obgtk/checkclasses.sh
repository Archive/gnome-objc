#!/bin/sh
grep Class ../../gtk+/gtk/*.h | \
	grep typedef | grep -v gpointer | awk '{print $4}'| \
	sed -e 's,Class;,,' > /tmp/.$$
for I in `cat /tmp/.$$ | sed -e 's,^Gtk,,'`; do
	[ ! -f obgtk$I.h ] && echo "Gtk$I object needs an obgtk wrapper"
done
rm -f /tmp/.$$
