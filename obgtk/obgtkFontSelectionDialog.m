#include "obgtkFontSelectionDialog.h"

@implementation Gtk_FontSelectionDialog

- castGtkFontSelectionDialog : (GtkFontSelectionDialog *) castitem
{
  gtkfontselectiondialog = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkFontSelectionDialog : (GtkFontSelectionDialog *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkFontSelectionDialog:castitem];

  return retval;
}

- initWithFontSelectionDialogInfo
 : (const gchar *) title
{
  return [self castGtkFontSelectionDialog:GTK_FONT_SELECTION_DIALOG(gtk_font_selection_dialog_new(title))];
}

- (GdkFont*) get_font
{
  return gtk_font_selection_dialog_get_font(gtkfontselectiondialog);
}

- set_filter
 : (GtkFontFilterType) filter_type
 : (GtkFontType) font_type
 : (gchar * *) foundries
 : (gchar * *) weights
 : (gchar * *) slants
 : (gchar * *) setwidths
 : (gchar * *) spacings
 : (gchar * *) charsets
{
  gtk_font_selection_dialog_set_filter(gtkfontselectiondialog, filter_type, font_type, foundries, weights, slants, setwidths, spacings, charsets);
  return self;
}

- (gboolean) set_font_name
 : (const gchar *) fontname
{
  return gtk_font_selection_dialog_set_font_name(gtkfontselectiondialog, fontname);
}

- (gchar*) get_preview_text
{
  return gtk_font_selection_dialog_get_preview_text(gtkfontselectiondialog);
}

- (gchar*) get_font_name
{
  return gtk_font_selection_dialog_get_font_name(gtkfontselectiondialog);
}

- set_preview_text
 : (const gchar *) text
{
  gtk_font_selection_dialog_set_preview_text(gtkfontselectiondialog, text);
  return self;
}

@end
