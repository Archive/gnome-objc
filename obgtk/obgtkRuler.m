#include "obgtkRuler.h"

@implementation Gtk_Ruler

- castGtkRuler : (GtkRuler *) castitem
{
  gtkruler = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkRuler : (GtkRuler *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkRuler:castitem];

  return retval;
}

- draw_ticks
{
  gtk_ruler_draw_ticks(gtkruler);
  return self;
}

- set_metric
 : (GtkMetricType) metric
{
  gtk_ruler_set_metric(gtkruler, metric);
  return self;
}

- set_range
 : (gfloat) lower
 : (gfloat) upper
 : (gfloat) position
 : (gfloat) max_size
{
  gtk_ruler_set_range(gtkruler, lower, upper, position, max_size);
  return self;
}

- draw_pos
{
  gtk_ruler_draw_pos(gtkruler);
  return self;
}

@end
