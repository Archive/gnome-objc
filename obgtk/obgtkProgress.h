#ifndef OBGTK_PROGRESS_H
#define OBGTK_PROGRESS_H 1
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkprogress.h>

@interface Gtk_Progress : Gtk_Widget
{
@public
  GtkProgress *gtkprogress;
}
- castGtkProgress : (GtkProgress *) castitem;
+ (id)makeGtkProgress : (GtkProgress *) castitem;
- set_activity_mode
 : (guint) activity_mode;
- set_percentage
 : (gfloat) percentage;
- (gchar*) get_current_text;
- set_show_text
 : (gint) show_text;
- configure
 : (gfloat) value
 : (gfloat) min
 : (gfloat) max;
- set_adjustment
 : (id) adjustment;
- set_text_alignment
 : (gfloat) x_align
 : (gfloat) y_align;
- (gchar*) get_text_from_value
 : (gfloat) value;
- (gfloat) get_value;
- (gfloat) get_current_percentage;
- (gfloat) get_percentage_from_value
 : (gfloat) value;
- set_format_string
 : (gchar *) format;
- set_value
 : (gfloat) value;
@end

#endif /* OBGTK_PROGRESS_H */
