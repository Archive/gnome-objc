#ifndef OBGTK_DIALOG_H
#define OBGTK_DIALOG_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWindow.h>
#include <gtk/gtkdialog.h>

@interface Gtk_Dialog : Gtk_Window
{
@public
  GtkDialog *gtkdialog;
}
- castGtkDialog : (GtkDialog *) castitem;
+ (id)makeGtkDialog : (GtkDialog *) castitem;
- init;
@end

#endif /* OBGTK_DIALOG_H */
