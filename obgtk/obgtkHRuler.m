#include "obgtkHRuler.h"

@implementation Gtk_HRuler

- castGtkHRuler : (GtkHRuler *) castitem
{
  gtkhruler = castitem;
  return [super castGtkRuler:GTK_RULER(castitem)];
}

+ (id) makeGtkHRuler : (GtkHRuler *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHRuler:castitem];

  return retval;
}

- init
{
  return [self castGtkHRuler:GTK_HRULER(gtk_hruler_new())];
}

@end
