#include "obgtkData.h"

@implementation Gtk_Data

- castGtkData : (GtkData *) castitem
{
  gtkdata = castitem;
  return [super castGtkObject:GTK_OBJECT(castitem)];
}

+ (id) makeGtkData : (GtkData *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkData:castitem];

  return retval;
}

@end
