#ifndef OBGTK_SCROLLBAR_H
#define OBGTK_SCROLLBAR_H 1
#include <obgtk/obgtkRange.h>
#include <gtk/gtkscrollbar.h>

@interface Gtk_Scrollbar : Gtk_Range
{
@public
  GtkScrollbar *gtkscrollbar;
}
- castGtkScrollbar : (GtkScrollbar *) castitem;
+ (id)makeGtkScrollbar : (GtkScrollbar *) castitem;
@end

#endif /* OBGTK_SCROLLBAR_H */
