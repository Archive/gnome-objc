#ifndef OBGTK_VIEWPORT_H
#define OBGTK_VIEWPORT_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkviewport.h>

@interface Gtk_Viewport : Gtk_Bin
{
@public
  GtkViewport *gtkviewport;
}
- castGtkViewport : (GtkViewport *) castitem;
+ (id)makeGtkViewport : (GtkViewport *) castitem;
- set_shadow_type
 : (GtkShadowType) type;
- (id) get_vadjustment;
- set_vadjustment
 : (id) adjustment;
- initWithViewportInfo
 : (id) hadjustment
 : (id) vadjustment;
- (id) get_hadjustment;
- set_hadjustment
 : (id) adjustment;
@end

#endif /* OBGTK_VIEWPORT_H */
