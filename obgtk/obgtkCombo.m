#include "obgtkCombo.h"

@implementation Gtk_Combo

- castGtkCombo : (GtkCombo *) castitem
{
  gtkcombo = castitem;
  return [super castGtkHBox:GTK_HBOX(castitem)];
}

+ (id) makeGtkCombo : (GtkCombo *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkCombo:castitem];

  return retval;
}

- init
{
  return [self castGtkCombo:GTK_COMBO(gtk_combo_new())];
}

@end
