#include "obgtkScale.h"

@implementation Gtk_Scale

- castGtkScale : (GtkScale *) castitem
{
  gtkscale = castitem;
  return [super castGtkRange:GTK_RANGE(castitem)];
}

+ (id) makeGtkScale : (GtkScale *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkScale:castitem];

  return retval;
}

- set_digits
 : (gint) digits
{
  gtk_scale_set_digits(gtkscale, digits);
  return self;
}

- draw_value
{
  gtk_scale_draw_value(gtkscale);
  return self;
}

- set_draw_value
 : (gboolean) draw_value
{
  gtk_scale_set_draw_value(gtkscale, draw_value);
  return self;
}

- (gint) get_value_width
{
  return gtk_scale_get_value_width(gtkscale);
}

- set_value_pos
 : (GtkPositionType) pos
{
  gtk_scale_set_value_pos(gtkscale, pos);
  return self;
}

@end
