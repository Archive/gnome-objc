#ifndef OBGTK_ARROW_H
#define OBGTK_ARROW_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkMisc.h>
#include <gtk/gtkarrow.h>

@interface Gtk_Arrow : Gtk_Misc
{
@public
  GtkArrow *gtkarrow;
}
- castGtkArrow : (GtkArrow *) castitem;
+ (id)makeGtkArrow : (GtkArrow *) castitem;
- initWithArrowInfo
 : (GtkArrowType) arrow_type
 : (GtkShadowType) shadow_type;
- set
 : (GtkArrowType) arrow_type
 : (GtkShadowType) shadow_type;
@end

#endif /* OBGTK_ARROW_H */
