#include "obgtkHScale.h"

@implementation Gtk_HScale

- castGtkHScale : (GtkHScale *) castitem
{
  gtkhscale = castitem;
  return [super castGtkScale:GTK_SCALE(castitem)];
}

+ (id) makeGtkHScale : (GtkHScale *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHScale:castitem];

  return retval;
}

- initWithHScaleInfo
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkHScale:GTK_HSCALE(gtk_hscale_new(((Gtk_Adjustment *)adjustment)->gtkadjustment))];
}

@end
