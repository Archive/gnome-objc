#include "obgtkList.h"

@implementation Gtk_List

- castGtkList : (GtkList *) castitem
{
  gtklist = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkList : (GtkList *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkList:castitem];

  return retval;
}

- undo_selection
{
  gtk_list_undo_selection(gtklist);
  return self;
}

- unselect_all
{
  gtk_list_unselect_all(gtklist);
  return self;
}

- select_item
 : (gint) item
{
  gtk_list_select_item(gtklist, item);
  return self;
}

- end_selection
{
  gtk_list_end_selection(gtklist);
  return self;
}

- extend_selection
 : (GtkScrollType) scroll_type
 : (gfloat) position
 : (gboolean) auto_start_selection
{
  gtk_list_extend_selection(gtklist, scroll_type, position, auto_start_selection);
  return self;
}

- (gint) child_position
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  return gtk_list_child_position(gtklist, ((Gtk_Widget *)child)->gtkwidget);
}

- unselect_item
 : (gint) item
{
  gtk_list_unselect_item(gtklist, item);
  return self;
}

- insert_items
 : (GList *) items
 : (gint) position
{
  gtk_list_insert_items(gtklist, items, position);
  return self;
}

- remove_items_no_unref
 : (GList *) items
{
  gtk_list_remove_items_no_unref(gtklist, items);
  return self;
}

- select_child
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_list_select_child(gtklist, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- toggle_row
 : (id) item
{
  g_return_val_if_fail([item isKindOf: [Gtk_Widget class]], 0);
  gtk_list_toggle_row(gtklist, ((Gtk_Widget *)item)->gtkwidget);
  return self;
}

- scroll_horizontal
 : (GtkScrollType) scroll_type
 : (gfloat) position
{
  gtk_list_scroll_horizontal(gtklist, scroll_type, position);
  return self;
}

- clear_items
 : (gint) start
 : (gint) end
{
  gtk_list_clear_items(gtklist, start, end);
  return self;
}

- prepend_items
 : (GList *) items
{
  gtk_list_prepend_items(gtklist, items);
  return self;
}

- remove_items
 : (GList *) items
{
  gtk_list_remove_items(gtklist, items);
  return self;
}

- unselect_child
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_list_unselect_child(gtklist, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- toggle_add_mode
{
  gtk_list_toggle_add_mode(gtklist);
  return self;
}

- start_selection
{
  gtk_list_start_selection(gtklist);
  return self;
}

- append_items
 : (GList *) items
{
  gtk_list_append_items(gtklist, items);
  return self;
}

- init
{
  return [self castGtkList:GTK_LIST(gtk_list_new())];
}

- set_selection_mode
 : (GtkSelectionMode) mode
{
  gtk_list_set_selection_mode(gtklist, mode);
  return self;
}

- toggle_focus_row
{
  gtk_list_toggle_focus_row(gtklist);
  return self;
}

- select_all
{
  gtk_list_select_all(gtklist);
  return self;
}

- end_drag_selection
{
  gtk_list_end_drag_selection(gtklist);
  return self;
}

- scroll_vertical
 : (GtkScrollType) scroll_type
 : (gfloat) position
{
  gtk_list_scroll_vertical(gtklist, scroll_type, position);
  return self;
}

@end
