#include "obgtkScrolledWindow.h"

@implementation Gtk_ScrolledWindow

- castGtkScrolledWindow : (GtkScrolledWindow *) castitem
{
  gtkscrolledwindow = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkScrolledWindow : (GtkScrolledWindow *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkScrolledWindow:castitem];

  return retval;
}

- set_placement
 : (GtkCornerType) window_placement
{
  gtk_scrolled_window_set_placement(gtkscrolledwindow, window_placement);
  return self;
}

- (id) get_vadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_scrolled_window_get_vadjustment(gtkscrolledwindow)];
}

- set_vadjustment
 : (id) hadjustment
{
  g_return_val_if_fail([hadjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_scrolled_window_set_vadjustment(gtkscrolledwindow, ((Gtk_Adjustment *)hadjustment)->gtkadjustment);
  return self;
}

- (id) get_hadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_scrolled_window_get_hadjustment(gtkscrolledwindow)];
}

- initWithScrolledWindowInfo
 : (id) hadjustment
 : (id) vadjustment
{
  g_return_val_if_fail([hadjustment isKindOf: [Gtk_Adjustment class]], 0);
  g_return_val_if_fail([vadjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkScrolledWindow:GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(((Gtk_Adjustment *)hadjustment)->gtkadjustment, ((Gtk_Adjustment *)vadjustment)->gtkadjustment))];
}

- set_hadjustment
 : (id) hadjustment
{
  g_return_val_if_fail([hadjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_scrolled_window_set_hadjustment(gtkscrolledwindow, ((Gtk_Adjustment *)hadjustment)->gtkadjustment);
  return self;
}

- add_with_viewport
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_scrolled_window_add_with_viewport(gtkscrolledwindow, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- set_policy
 : (GtkPolicyType) hscrollbar_policy
 : (GtkPolicyType) vscrollbar_policy
{
  gtk_scrolled_window_set_policy(gtkscrolledwindow, hscrollbar_policy, vscrollbar_policy);
  return self;
}

@end
