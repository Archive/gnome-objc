#ifndef OBGTK_DRAWINGAREA_H
#define OBGTK_DRAWINGAREA_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkdrawingarea.h>

@interface Gtk_DrawingArea : Gtk_Widget
{
@public
  GtkDrawingArea *gtkdrawingarea;
}
- castGtkDrawingArea : (GtkDrawingArea *) castitem;
+ (id)makeGtkDrawingArea : (GtkDrawingArea *) castitem;
- size
 : (gint) width
 : (gint) height;
- init;
@end

#endif /* OBGTK_DRAWINGAREA_H */
