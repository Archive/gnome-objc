#include "obgtkFileSelection.h"

@implementation Gtk_FileSelection

- castGtkFileSelection : (GtkFileSelection *) castitem
{
  gtkfileselection = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkFileSelection : (GtkFileSelection *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkFileSelection:castitem];

  return retval;
}

- set_filename
 : (const gchar *) filename
{
  gtk_file_selection_set_filename(gtkfileselection, filename);
  return self;
}

- complete
 : (const gchar *) pattern
{
  gtk_file_selection_complete(gtkfileselection, pattern);
  return self;
}

- initWithFileSelectionInfo
 : (const gchar *) title
{
  return [self castGtkFileSelection:GTK_FILE_SELECTION(gtk_file_selection_new(title))];
}

- show_fileop_buttons
{
  gtk_file_selection_show_fileop_buttons(gtkfileselection);
  return self;
}

- (gchar*) get_filename
{
  return gtk_file_selection_get_filename(gtkfileselection);
}

- hide_fileop_buttons
{
  gtk_file_selection_hide_fileop_buttons(gtkfileselection);
  return self;
}

@end
