#ifndef OBGTK_HRULER_H
#define OBGTK_HRULER_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkRuler.h>
#include <gtk/gtkhruler.h>

@interface Gtk_HRuler : Gtk_Ruler
{
@public
  GtkHRuler *gtkhruler;
}
- castGtkHRuler : (GtkHRuler *) castitem;
+ (id)makeGtkHRuler : (GtkHRuler *) castitem;
- init;
@end

#endif /* OBGTK_HRULER_H */
