#ifndef OBGTK_ENTRY_H
#define OBGTK_ENTRY_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkEditable.h>
#include <gtk/gtkentry.h>

@interface Gtk_Entry : Gtk_Editable
{
@public
  GtkEntry *gtkentry;
}
- castGtkEntry : (GtkEntry *) castitem;
+ (id)makeGtkEntry : (GtkEntry *) castitem;
- prepend_text
 : (const gchar *) text;
- init;
- initWithMaxLength
 : (guint16) max;
- (gchar*) get_text;
- set_max_length
 : (guint16) max;
- set_editable
 : (gboolean) editable;
- append_text
 : (const gchar *) text;
- set_position
 : (gint) position;
- set_visibility
 : (gboolean) visible;
- select_region
 : (gint) start
 : (gint) end;
- set_text
 : (const gchar *) text;
@end

#endif /* OBGTK_ENTRY_H */
