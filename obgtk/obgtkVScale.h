#ifndef OBGTK_VSCALE_H
#define OBGTK_VSCALE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkScale.h>
#include <gtk/gtkvscale.h>

@interface Gtk_VScale : Gtk_Scale
{
@public
  GtkVScale *gtkvscale;
}
- castGtkVScale : (GtkVScale *) castitem;
+ (id)makeGtkVScale : (GtkVScale *) castitem;
- initWithVScaleInfo
 : (id) adjustment;
@end

#endif /* OBGTK_VSCALE_H */
