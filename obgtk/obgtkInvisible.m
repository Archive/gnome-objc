#include "obgtkInvisible.h"

@implementation Gtk_Invisible

- castGtkInvisible : (GtkInvisible *) castitem
{
  gtkinvisible = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkInvisible : (GtkInvisible *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkInvisible:castitem];

  return retval;
}

- init
{
  return [self castGtkInvisible:GTK_INVISIBLE(gtk_invisible_new())];
}

@end
