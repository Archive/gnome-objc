#include "obgtkMenuItem.h"

@implementation Gtk_MenuItem

- castGtkMenuItem : (GtkMenuItem *) castitem
{
  gtkmenuitem = castitem;
  return [super castGtkItem:GTK_ITEM(castitem)];
}

+ (id) makeGtkMenuItem : (GtkMenuItem *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkMenuItem:castitem];

  return retval;
}

- initWithLabel
 : (const gchar *) label
{
  return [self castGtkMenuItem:GTK_MENU_ITEM(gtk_menu_item_new_with_label(label))];
}

- configure
 : (gint) show_toggle_indicator
 : (gint) show_submenu_indicator
{
  gtk_menu_item_configure(gtkmenuitem, show_toggle_indicator, show_submenu_indicator);
  return self;
}

- set_placement
 : (GtkSubmenuPlacement) placement
{
  gtk_menu_item_set_placement(gtkmenuitem, placement);
  return self;
}

- init
{
  return [self castGtkMenuItem:GTK_MENU_ITEM(gtk_menu_item_new())];
}

- deselect
{
  gtk_menu_item_deselect(gtkmenuitem);
  return self;
}

- set_submenu
 : (id) submenu
{
  g_return_val_if_fail([submenu isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_item_set_submenu(gtkmenuitem, ((Gtk_Widget *)submenu)->gtkwidget);
  return self;
}

- right_justify
{
  gtk_menu_item_right_justify(gtkmenuitem);
  return self;
}

- remove_submenu
{
  gtk_menu_item_remove_submenu(gtkmenuitem);
  return self;
}

- select
{
  gtk_menu_item_select(gtkmenuitem);
  return self;
}

- activate
{
  gtk_menu_item_activate(gtkmenuitem);
  return self;
}

@end
