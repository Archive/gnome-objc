#ifndef OBGTK_HPANED_H
#define OBGTK_HPANED_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkPaned.h>
#include <gtk/gtkhpaned.h>

@interface Gtk_HPaned : Gtk_Paned
{
@public
  GtkHPaned *gtkhpaned;
}
- castGtkHPaned : (GtkHPaned *) castitem;
+ (id)makeGtkHPaned : (GtkHPaned *) castitem;
- init;
@end

#endif /* OBGTK_HPANED_H */
