#ifndef OBGTK_LAYOUT_H
#define OBGTK_LAYOUT_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtklayout.h>

@interface Gtk_Layout : Gtk_Container
{
@public
  GtkLayout *gtklayout;
}
- castGtkLayout : (GtkLayout *) castitem;
+ (id)makeGtkLayout : (GtkLayout *) castitem;
- thaw;
- set_size
 : (guint) width
 : (guint) height;
- initWithLayoutInfo
 : (id) hadjustment
 : (id) vadjustment;
- move
 : (id) widget
 : (gint) x
 : (gint) y;
- set_vadjustment
 : (id) adjustment;
- put
 : (id) widget
 : (gint) x
 : (gint) y;
- set_hadjustment
 : (id) adjustment;
- freeze;
- (id) get_vadjustment;
- (id) get_hadjustment;
@end

#endif /* OBGTK_LAYOUT_H */
