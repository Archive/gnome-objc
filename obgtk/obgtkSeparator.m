#include "obgtkSeparator.h"

@implementation Gtk_Separator

- castGtkSeparator : (GtkSeparator *) castitem
{
  gtkseparator = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkSeparator : (GtkSeparator *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkSeparator:castitem];

  return retval;
}

@end
