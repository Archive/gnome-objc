#include "obgtkCTree.h"

@implementation Gtk_CTree

- castGtkCTree : (GtkCTree *) castitem
{
  gtkctree = castitem;
  return [super castGtkCList:GTK_CLIST(castitem)];
}

+ (id) makeGtkCTree : (GtkCTree *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkCTree:castitem];

  return retval;
}

- set_line_style
 : (GtkCTreeLineStyle) line_style
{
  gtk_ctree_set_line_style(gtkctree, line_style);
  return self;
}

- sort_recursive
 : (GtkCTreeNode *) node
{
  gtk_ctree_sort_recursive(gtkctree, node);
  return self;
}

- (gboolean) is_viewable
 : (GtkCTreeNode *) node
{
  return gtk_ctree_is_viewable(gtkctree, node);
}

- select
 : (GtkCTreeNode *) node
{
  gtk_ctree_select(gtkctree, node);
  return self;
}

- (GtkCTreeNode *) insert_node
 : (GtkCTreeNode *) parent
 : (GtkCTreeNode *) sibling
 : (gchar **) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap_closed
 : (GdkBitmap *) mask_closed
 : (GdkPixmap *) pixmap_opened
 : (GdkBitmap *) mask_opened
 : (gboolean) is_leaf
 : (gboolean) expanded
{
  return gtk_ctree_insert_node(gtkctree, parent, sibling, text, spacing, pixmap_closed, mask_closed, pixmap_opened, mask_opened, is_leaf, expanded);
}

- (GtkCTreeNode *) insert_gnode
 : (GtkCTreeNode *) parent
 : (GtkCTreeNode *) sibling
 : (GNode *) gnode
 : (GtkCTreeGNodeFunc) func
 : (gpointer) data
{
  return gtk_ctree_insert_gnode(gtkctree, parent, sibling, gnode, func, data);
}

- node_set_pixtext
 : (GtkCTreeNode *) node
 : (gint) column
 : (const gchar *) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask
{
  gtk_ctree_node_set_pixtext(gtkctree, node, column, text, spacing, pixmap, mask);
  return self;
}

- node_set_text
 : (GtkCTreeNode *) node
 : (gint) column
 : (const gchar *) text
{
  gtk_ctree_node_set_text(gtkctree, node, column, text);
  return self;
}

- (gboolean) is_ancestor
 : (GtkCTreeNode *) node
 : (GtkCTreeNode *) child
{
  return gtk_ctree_is_ancestor(gtkctree, node, child);
}

- post_recursive
 : (GtkCTreeNode *) node
 : (GtkCTreeFunc) func
 : (gpointer) data
{
  gtk_ctree_post_recursive(gtkctree, node, func, data);
  return self;
}

- initWithCTreeInfo
 : (gint) columns
 : (gint) tree_column
{
  return [self castGtkCTree:GTK_CTREE(gtk_ctree_new(columns, tree_column))];
}

- node_set_selectable
 : (GtkCTreeNode *) node
 : (gboolean) selectable
{
  gtk_ctree_node_set_selectable(gtkctree, node, selectable);
  return self;
}

- (GList *) find_all_by_row_data
 : (GtkCTreeNode *) node
 : (gpointer) data
{
  return gtk_ctree_find_all_by_row_data(gtkctree, node, data);
}

- construct
 : (gint) columns
 : (gint) tree_column
 : (gchar **) titles
{
  gtk_ctree_construct(gtkctree, columns, tree_column, titles);
  return self;
}

- (GtkStyle *) node_get_cell_style
 : (GtkCTreeNode *) node
 : (gint) column
{
  return gtk_ctree_node_get_cell_style(gtkctree, node, column);
}

- set_indent
 : (gint) indent
{
  gtk_ctree_set_indent(gtkctree, indent);
  return self;
}

- (gboolean) is_hot_spot
 : (gint) x
 : (gint) y
{
  return gtk_ctree_is_hot_spot(gtkctree, x, y);
}

- node_set_foreground
 : (GtkCTreeNode *) node
 : (GdkColor *) color
{
  gtk_ctree_node_set_foreground(gtkctree, node, color);
  return self;
}

- real_select_recursive
 : (GtkCTreeNode *) node
 : (gint) state
{
  gtk_ctree_real_select_recursive(gtkctree, node, state);
  return self;
}

- (GtkCTreeNode *) find_by_row_data_custom
 : (GtkCTreeNode *) node
 : (gpointer) data
 : (GCompareFunc) func
{
  return gtk_ctree_find_by_row_data_custom(gtkctree, node, data, func);
}

- (gint) node_get_pixtext
 : (GtkCTreeNode *) node
 : (gint) column
 : (gchar * *) text
 : (guint8 *) spacing
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask
{
  return gtk_ctree_node_get_pixtext(gtkctree, node, column, text, spacing, pixmap, mask);
}

- (gint) node_get_text
 : (GtkCTreeNode *) node
 : (gint) column
 : (gchar * *) text
{
  return gtk_ctree_node_get_text(gtkctree, node, column, text);
}

- set_drag_compare_func
 : (GtkCTreeCompareDragFunc) cmp_func
{
  gtk_ctree_set_drag_compare_func(gtkctree, cmp_func);
  return self;
}

- initWithTitles
 : (gint) columns
 : (gint) tree_column
 : (gchar **) titles
{
  return [self castGtkCTree:GTK_CTREE(gtk_ctree_new_with_titles(columns, tree_column, titles))];
}

- expand
 : (GtkCTreeNode *) node
{
  gtk_ctree_expand(gtkctree, node);
  return self;
}

- unselect
 : (GtkCTreeNode *) node
{
  gtk_ctree_unselect(gtkctree, node);
  return self;
}

- (GNode *) export_to_gnode
 : (GNode *) parent
 : (GNode *) sibling
 : (GtkCTreeNode *) node
 : (GtkCTreeGNodeFunc) func
 : (gpointer) data
{
  return gtk_ctree_export_to_gnode(gtkctree, parent, sibling, node, func, data);
}

- expand_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth
{
  gtk_ctree_expand_to_depth(gtkctree, node, depth);
  return self;
}

- (GtkVisibility) node_is_visible
 : (GtkCTreeNode *) node
{
  return gtk_ctree_node_is_visible(gtkctree, node);
}

- set_expander_style
 : (GtkCTreeExpanderStyle) expander_style
{
  gtk_ctree_set_expander_style(gtkctree, expander_style);
  return self;
}

- (gpointer) node_get_row_data
 : (GtkCTreeNode *) node
{
  return gtk_ctree_node_get_row_data(gtkctree, node);
}

- (GtkCTreeNode *) last
 : (GtkCTreeNode *) node
{
  return gtk_ctree_last(gtkctree, node);
}

- sort_node
 : (GtkCTreeNode *) node
{
  gtk_ctree_sort_node(gtkctree, node);
  return self;
}

- toggle_expansion_recursive
 : (GtkCTreeNode *) node
{
  gtk_ctree_toggle_expansion_recursive(gtkctree, node);
  return self;
}

- node_set_row_data
 : (GtkCTreeNode *) node
 : (gpointer) data
{
  gtk_ctree_node_set_row_data(gtkctree, node, data);
  return self;
}

- (gboolean) find
 : (GtkCTreeNode *) node
 : (GtkCTreeNode *) child
{
  return gtk_ctree_find(gtkctree, node, child);
}

- (GtkCTreeNode *) find_by_row_data
 : (GtkCTreeNode *) node
 : (gpointer) data
{
  return gtk_ctree_find_by_row_data(gtkctree, node, data);
}

- (gint) node_get_pixmap
 : (GtkCTreeNode *) node
 : (gint) column
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask
{
  return gtk_ctree_node_get_pixmap(gtkctree, node, column, pixmap, mask);
}

- remove_node
 : (GtkCTreeNode *) node
{
  gtk_ctree_remove_node(gtkctree, node);
  return self;
}

- (gint) get_node_info
 : (GtkCTreeNode *) node
 : (gchar * *) text
 : (guint8 *) spacing
 : (GdkPixmap * *) pixmap_closed
 : (GdkBitmap * *) mask_closed
 : (GdkPixmap * *) pixmap_opened
 : (GdkBitmap * *) mask_opened
 : (gboolean *) is_leaf
 : (gboolean *) expanded
{
  return gtk_ctree_get_node_info(gtkctree, node, text, spacing, pixmap_closed, mask_closed, pixmap_opened, mask_opened, is_leaf, expanded);
}

- set_spacing
 : (gint) spacing
{
  gtk_ctree_set_spacing(gtkctree, spacing);
  return self;
}

- set_node_info
 : (GtkCTreeNode *) node
 : (const gchar *) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap_closed
 : (GdkBitmap *) mask_closed
 : (GdkPixmap *) pixmap_opened
 : (GdkBitmap *) mask_opened
 : (gboolean) is_leaf
 : (gboolean) expanded
{
  gtk_ctree_set_node_info(gtkctree, node, text, spacing, pixmap_closed, mask_closed, pixmap_opened, mask_opened, is_leaf, expanded);
  return self;
}

- unselect_recursive
 : (GtkCTreeNode *) node
{
  gtk_ctree_unselect_recursive(gtkctree, node);
  return self;
}

- node_set_shift
 : (GtkCTreeNode *) node
 : (gint) column
 : (gint) vertical
 : (gint) horizontal
{
  gtk_ctree_node_set_shift(gtkctree, node, column, vertical, horizontal);
  return self;
}

- collapse_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth
{
  gtk_ctree_collapse_to_depth(gtkctree, node, depth);
  return self;
}

- node_set_row_data_full
 : (GtkCTreeNode *) node
 : (gpointer) data
 : (GtkDestroyNotify) destroy
{
  gtk_ctree_node_set_row_data_full(gtkctree, node, data, destroy);
  return self;
}

- (GtkCellType) node_get_cell_type
 : (GtkCTreeNode *) node
 : (gint) column
{
  return gtk_ctree_node_get_cell_type(gtkctree, node, column);
}

- node_set_cell_style
 : (GtkCTreeNode *) node
 : (gint) column
 : (GtkStyle *) style
{
  gtk_ctree_node_set_cell_style(gtkctree, node, column, style);
  return self;
}

- (gboolean) node_get_selectable
 : (GtkCTreeNode *) node
{
  return gtk_ctree_node_get_selectable(gtkctree, node);
}

- collapse
 : (GtkCTreeNode *) node
{
  gtk_ctree_collapse(gtkctree, node);
  return self;
}

- (GtkCTreeNode *) node_nth
 : (guint) row
{
  return gtk_ctree_node_nth(gtkctree, row);
}

- toggle_expansion
 : (GtkCTreeNode *) node
{
  gtk_ctree_toggle_expansion(gtkctree, node);
  return self;
}

- node_set_row_style
 : (GtkCTreeNode *) node
 : (GtkStyle *) style
{
  gtk_ctree_node_set_row_style(gtkctree, node, style);
  return self;
}

- select_recursive
 : (GtkCTreeNode *) node
{
  gtk_ctree_select_recursive(gtkctree, node);
  return self;
}

- node_set_background
 : (GtkCTreeNode *) node
 : (GdkColor *) color
{
  gtk_ctree_node_set_background(gtkctree, node, color);
  return self;
}

- (GtkStyle *) node_get_row_style
 : (GtkCTreeNode *) node
{
  return gtk_ctree_node_get_row_style(gtkctree, node);
}

- set_show_stub
 : (gboolean) show_stub
{
  gtk_ctree_set_show_stub(gtkctree, show_stub);
  return self;
}

- move
 : (GtkCTreeNode *) node
 : (GtkCTreeNode *) new_parent
 : (GtkCTreeNode *) new_sibling
{
  gtk_ctree_move(gtkctree, node, new_parent, new_sibling);
  return self;
}

- node_moveto
 : (GtkCTreeNode *) node
 : (gint) column
 : (gfloat) row_align
 : (gfloat) col_align
{
  gtk_ctree_node_moveto(gtkctree, node, column, row_align, col_align);
  return self;
}

- (GtkCTreeNode *) find_node_ptr
 : (GtkCTreeRow *) ctree_row
{
  return gtk_ctree_find_node_ptr(gtkctree, ctree_row);
}

- pre_recursive
 : (GtkCTreeNode *) node
 : (GtkCTreeFunc) func
 : (gpointer) data
{
  gtk_ctree_pre_recursive(gtkctree, node, func, data);
  return self;
}

- collapse_recursive
 : (GtkCTreeNode *) node
{
  gtk_ctree_collapse_recursive(gtkctree, node);
  return self;
}

- node_set_pixmap
 : (GtkCTreeNode *) node
 : (gint) column
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask
{
  gtk_ctree_node_set_pixmap(gtkctree, node, column, pixmap, mask);
  return self;
}

- post_recursive_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth
 : (GtkCTreeFunc) func
 : (gpointer) data
{
  gtk_ctree_post_recursive_to_depth(gtkctree, node, depth, func, data);
  return self;
}

- pre_recursive_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth
 : (GtkCTreeFunc) func
 : (gpointer) data
{
  gtk_ctree_pre_recursive_to_depth(gtkctree, node, depth, func, data);
  return self;
}

- expand_recursive
 : (GtkCTreeNode *) node
{
  gtk_ctree_expand_recursive(gtkctree, node);
  return self;
}

- (GList *) find_all_by_row_data_custom
 : (GtkCTreeNode *) node
 : (gpointer) data
 : (GCompareFunc) func
{
  return gtk_ctree_find_all_by_row_data_custom(gtkctree, node, data, func);
}

@end
