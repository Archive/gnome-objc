#include "obgtkItem.h"

@implementation Gtk_Item

- castGtkItem : (GtkItem *) castitem
{
  gtkitem = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkItem : (GtkItem *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkItem:castitem];

  return retval;
}

- deselect
{
  gtk_item_deselect(gtkitem);
  return self;
}

- toggle
{
  gtk_item_toggle(gtkitem);
  return self;
}

- select
{
  gtk_item_select(gtkitem);
  return self;
}

@end
