#include "obgtkRadioMenuItem.h"

@implementation Gtk_RadioMenuItem

- castGtkRadioMenuItem : (GtkRadioMenuItem *) castitem
{
  gtkradiomenuitem = castitem;
  return [super castGtkCheckMenuItem:GTK_CHECK_MENU_ITEM(castitem)];
}

+ (id) makeGtkRadioMenuItem : (GtkRadioMenuItem *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkRadioMenuItem:castitem];

  return retval;
}

- set_group
 : (GSList *) group
{
  gtk_radio_menu_item_set_group(gtkradiomenuitem, group);
  return self;
}

- initWithLabel
 : (GSList *) group
 : (const gchar *) label
{
  return [self castGtkRadioMenuItem:GTK_RADIO_MENU_ITEM(gtk_radio_menu_item_new_with_label(group, label))];
}

- (GSList*) group
{
  return gtk_radio_menu_item_group(gtkradiomenuitem);
}

- initWithRadioMenuItemInfo
 : (GSList *) group
{
  return [self castGtkRadioMenuItem:GTK_RADIO_MENU_ITEM(gtk_radio_menu_item_new(group))];
}

@end
