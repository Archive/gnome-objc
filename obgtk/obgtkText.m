#include "obgtkText.h"

@implementation Gtk_Text

- castGtkText : (GtkText *) castitem
{
  gtktext = castitem;
  return [super castGtkEditable:GTK_EDITABLE(castitem)];
}

+ (id) makeGtkText : (GtkText *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkText:castitem];

  return retval;
}

- set_adjustments
 : (id) hadj
 : (id) vadj
{
  g_return_val_if_fail([hadj isKindOf: [Gtk_Adjustment class]], 0);
  g_return_val_if_fail([vadj isKindOf: [Gtk_Adjustment class]], 0);
  gtk_text_set_adjustments(gtktext, ((Gtk_Adjustment *)hadj)->gtkadjustment, ((Gtk_Adjustment *)vadj)->gtkadjustment);
  return self;
}

- set_point
 : (guint) index
{
  gtk_text_set_point(gtktext, index);
  return self;
}

- freeze
{
  gtk_text_freeze(gtktext);
  return self;
}

- set_editable
 : (gboolean) editable
{
  gtk_text_set_editable(gtktext, editable);
  return self;
}

- (gint) backward_delete
 : (guint) nchars
{
  return gtk_text_backward_delete(gtktext, nchars);
}

- set_word_wrap
 : (gint) word_wrap
{
  gtk_text_set_word_wrap(gtktext, word_wrap);
  return self;
}

- thaw
{
  gtk_text_thaw(gtktext);
  return self;
}

- (guint) get_length
{
  return gtk_text_get_length(gtktext);
}

- initWithTextInfo
 : (id) hadj
 : (id) vadj
{
  g_return_val_if_fail([hadj isKindOf: [Gtk_Adjustment class]], 0);
  g_return_val_if_fail([vadj isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkText:GTK_TEXT(gtk_text_new(((Gtk_Adjustment *)hadj)->gtkadjustment, ((Gtk_Adjustment *)vadj)->gtkadjustment))];
}

- (gint) forward_delete
 : (guint) nchars
{
  return gtk_text_forward_delete(gtktext, nchars);
}

- set_line_wrap
 : (gint) line_wrap
{
  gtk_text_set_line_wrap(gtktext, line_wrap);
  return self;
}

- insert
 : (GdkFont *) font
 : (GdkColor *) fore
 : (GdkColor *) back
 : (const char *) chars
 : (gint) length
{
  gtk_text_insert(gtktext, font, fore, back, chars, length);
  return self;
}

- (guint) get_point
{
  return gtk_text_get_point(gtktext);
}

@end
