#ifndef OBGTK_NOTEBOOK_H
#define OBGTK_NOTEBOOK_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtknotebook.h>

@interface Gtk_Notebook : Gtk_Container
{
@public
  GtkNotebook *gtknotebook;
}
- castGtkNotebook : (GtkNotebook *) castitem;
+ (id)makeGtkNotebook : (GtkNotebook *) castitem;
- prepend_page_menu
 : (id) child
 : (id) tab_label
 : (id) menu_label;
- reorder_child
 : (id) child
 : (gint) position;
- set_tab_border
 : (guint) border_width;
- (gint) page_num
 : (id) child;
- (id) get_nth_page
 : (gint) page_num;
- set_show_tabs
 : (gboolean) show_tabs;
- set_page
 : (gint) page_num;
- set_tab_hborder
 : (guint) tab_hborder;
- append_page_menu
 : (id) child
 : (id) tab_label
 : (id) menu_label;
- set_menu_label
 : (id) child
 : (id) menu_label;
- (id) get_menu_label
 : (id) child;
- prev_page;
- set_tab_label
 : (id) child
 : (id) tab_label;
- (id) get_tab_label
 : (id) child;
- insert_page
 : (id) child
 : (id) tab_label
 : (gint) position;
- (gint) get_current_page;
- set_scrollable
 : (gboolean) scrollable;
- prepend_page
 : (id) child
 : (id) tab_label;
- init;
- append_page
 : (id) child
 : (id) tab_label;
- set_tab_pos
 : (GtkPositionType) pos;
- set_tab_vborder
 : (guint) tab_vborder;
- remove_page
 : (gint) page_num;
- set_homogeneous_tabs
 : (gboolean) homogeneous;
- set_show_border
 : (gboolean) show_border;
- popup_enable;
- next_page;
- set_tab_label_packing
 : (id) child
 : (gboolean) expand
 : (gboolean) fill
 : (GtkPackType) pack_type;
- set_menu_label_text
 : (id) child
 : (const gchar *) menu_text;
- insert_page_menu
 : (id) child
 : (id) tab_label
 : (id) menu_label
 : (gint) position;
- set_tab_label_text
 : (id) child
 : (const gchar *) tab_text;
- popup_disable;
- query_tab_label_packing
 : (id) child
 : (gboolean *) expand
 : (gboolean *) fill
 : (GtkPackType *) pack_type;
@end

#endif /* OBGTK_NOTEBOOK_H */
