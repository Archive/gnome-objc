#ifndef OBGTK_MISC_H
#define OBGTK_MISC_H 1
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkmisc.h>

@interface Gtk_Misc : Gtk_Widget
{
@public
  GtkMisc *gtkmisc;
}
- castGtkMisc : (GtkMisc *) castitem;
+ (id)makeGtkMisc : (GtkMisc *) castitem;
- set_alignment
 : (gfloat) xalign
 : (gfloat) yalign;
- set_padding
 : (gint) xpad
 : (gint) ypad;
@end

#endif /* OBGTK_MISC_H */
