#ifndef OBGTK_PANED_H
#define OBGTK_PANED_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtkpaned.h>

@interface Gtk_Paned : Gtk_Container
{
@public
  GtkPaned *gtkpaned;
}
- castGtkPaned : (GtkPaned *) castitem;
+ (id)makeGtkPaned : (GtkPaned *) castitem;
- set_handle_size
 : (guint16) size;
- compute_position
 : (gint) allocation
 : (gint) child1_req
 : (gint) child2_req;
- pack1
 : (id) child
 : (gboolean) resize
 : (gboolean) shrink;
- pack2
 : (id) child
 : (gboolean) resize
 : (gboolean) shrink;
- add1
 : (id) child;
- add2
 : (id) child;
- set_position
 : (gint) position;
- set_gutter_size
 : (guint16) size;
@end

#endif /* OBGTK_PANED_H */
