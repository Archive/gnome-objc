#ifndef OBGTK_STATUSBAR_H
#define OBGTK_STATUSBAR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkHBox.h>
#include <gtk/gtkstatusbar.h>

@interface Gtk_Statusbar : Gtk_HBox
{
@public
  GtkStatusbar *gtkstatusbar;
}
- castGtkStatusbar : (GtkStatusbar *) castitem;
+ (id)makeGtkStatusbar : (GtkStatusbar *) castitem;
- pop
 : (guint) context_id;
- init;
- remove
 : (guint) context_id
 : (guint) message_id;
- (guint) get_context_id
 : (const gchar *) context_description;
- (guint) push
 : (guint) context_id
 : (const gchar *) text;
@end

#endif /* OBGTK_STATUSBAR_H */
