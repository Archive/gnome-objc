#ifndef OBGTK_TREE_H
#define OBGTK_TREE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkContainer.h>
#include <gtk/gtktree.h>

@interface Gtk_Tree : Gtk_Container
{
@public
  GtkTree *gtktree;
}
- castGtkTree : (GtkTree *) castitem;
+ (id)makeGtkTree : (GtkTree *) castitem;
- unselect_item
 : (gint) item;
- remove_item
 : (id) child;
- select_child
 : (id) tree_item;
- unselect_child
 : (id) tree_item;
- insert
 : (id) tree_item
 : (gint) position;
- clear_items
 : (gint) start
 : (gint) end;
- remove_items
 : (GList *) items;
- prepend
 : (id) tree_item;
- set_view_lines
 : (guint) flag;
- init;
- set_view_mode
 : (GtkTreeViewMode) mode;
- append
 : (id) tree_item;
- set_selection_mode
 : (GtkSelectionMode) mode;
- select_item
 : (gint) item;
- (gint) child_position
 : (id) child;
@end

#endif /* OBGTK_TREE_H */
