#include "obgtkVButtonBox.h"

@implementation Gtk_VButtonBox

- castGtkVButtonBox : (GtkVButtonBox *) castitem
{
  gtkvbuttonbox = castitem;
  return [super castGtkButtonBox:GTK_BUTTON_BOX(castitem)];
}

+ (id) makeGtkVButtonBox : (GtkVButtonBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkVButtonBox:castitem];

  return retval;
}

- init
{
  return [self castGtkVButtonBox:GTK_VBUTTON_BOX(gtk_vbutton_box_new())];
}

@end
