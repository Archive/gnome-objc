#include "obgtkPreview.h"

@implementation Gtk_Preview

- castGtkPreview : (GtkPreview *) castitem
{
  gtkpreview = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

+ (id) makeGtkPreview : (GtkPreview *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkPreview:castitem];

  return retval;
}

- set_dither
 : (GdkRgbDither) dither
{
  gtk_preview_set_dither(gtkpreview, dither);
  return self;
}

- set_expand
 : (gint) expand
{
  gtk_preview_set_expand(gtkpreview, expand);
  return self;
}

- size
 : (gint) width
 : (gint) height
{
  gtk_preview_size(gtkpreview, width, height);
  return self;
}

- draw_row
 : (guchar *) data
 : (gint) x
 : (gint) y
 : (gint) w
{
  gtk_preview_draw_row(gtkpreview, data, x, y, w);
  return self;
}

- initWithPreviewInfo
 : (GtkPreviewType) type
{
  return [self castGtkPreview:GTK_PREVIEW(gtk_preview_new(type))];
}

- put
 : (GdkWindow *) window
 : (GdkGC *) gc
 : (gint) srcx
 : (gint) srcy
 : (gint) destx
 : (gint) desty
 : (gint) width
 : (gint) height
{
  gtk_preview_put(gtkpreview, window, gc, srcx, srcy, destx, desty, width, height);
  return self;
}

@end
