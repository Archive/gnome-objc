#include "obgtkHButtonBox.h"

@implementation Gtk_HButtonBox

- castGtkHButtonBox : (GtkHButtonBox *) castitem
{
  gtkhbuttonbox = castitem;
  return [super castGtkButtonBox:GTK_BUTTON_BOX(castitem)];
}

+ (id) makeGtkHButtonBox : (GtkHButtonBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHButtonBox:castitem];

  return retval;
}

- init
{
  return [self castGtkHButtonBox:GTK_HBUTTON_BOX(gtk_hbutton_box_new())];
}

@end
