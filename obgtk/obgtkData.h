#ifndef OBGTK_DATA_H
#define OBGTK_DATA_H 1
#include <obgtk/obgtkObject.h>
#include <gtk/gtkdata.h>

@interface Gtk_Data : Gtk_Object
{
@public
  GtkData *gtkdata;
}
- castGtkData : (GtkData *) castitem;
+ (id)makeGtkData : (GtkData *) castitem;
@end

#endif /* OBGTK_DATA_H */
