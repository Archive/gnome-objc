#include "obgtkMenu.h"

@implementation Gtk_Menu

- castGtkMenu : (GtkMenu *) castitem
{
  gtkmenu = castitem;
  return [super castGtkMenuShell:GTK_MENU_SHELL(castitem)];
}

+ (id) makeGtkMenu : (GtkMenu *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkMenu:castitem];

  return retval;
}

- set_title
 : (const gchar *) title
{
  gtk_menu_set_title(gtkmenu, title);
  return self;
}

- set_active
 : (guint) index
{
  gtk_menu_set_active(gtkmenu, index);
  return self;
}

- prepend
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_prepend(gtkmenu, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- (id) ensure_uline_accel_group
{
  return [[Gtk_AccelGroup class] makeGtkAccelGroup:gtk_menu_ensure_uline_accel_group(gtkmenu)];
}

- set_accel_group
 : (id) accel_group
{
  g_return_val_if_fail([accel_group isKindOf: [Gtk_AccelGroup class]], 0);
  gtk_menu_set_accel_group(gtkmenu, ((Gtk_AccelGroup *)accel_group)->gtkaccelgroup);
  return self;
}

- (id) get_attach_widget
{
  return [[Gtk_Widget class] makeGtkWidget:gtk_menu_get_attach_widget(gtkmenu)];
}

- append
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_append(gtkmenu, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- popup
 : (id) parent_menu_shell
 : (id) parent_menu_item
 : (GtkMenuPositionFunc) func
 : (gpointer) data
 : (guint) button
 : (guint32) activate_time
{
  g_return_val_if_fail([parent_menu_shell isKindOf: [Gtk_Widget class]], 0);
  g_return_val_if_fail([parent_menu_item isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_popup(gtkmenu, ((Gtk_Widget *)parent_menu_shell)->gtkwidget, ((Gtk_Widget *)parent_menu_item)->gtkwidget, func, data, button, activate_time);
  return self;
}

- (id) get_active
{
  return [[Gtk_Widget class] makeGtkWidget:gtk_menu_get_active(gtkmenu)];
}

- set_tearoff_state
 : (gboolean) torn_off
{
  gtk_menu_set_tearoff_state(gtkmenu, torn_off);
  return self;
}

- (id) get_uline_accel_group
{
  return [[Gtk_AccelGroup class] makeGtkAccelGroup:gtk_menu_get_uline_accel_group(gtkmenu)];
}

- init
{
  return [self castGtkMenu:GTK_MENU(gtk_menu_new())];
}

- (id) get_accel_group
{
  return [[Gtk_AccelGroup class] makeGtkAccelGroup:gtk_menu_get_accel_group(gtkmenu)];
}

- popdown
{
  gtk_menu_popdown(gtkmenu);
  return self;
}

- detach
{
  gtk_menu_detach(gtkmenu);
  return self;
}

- reposition
{
  gtk_menu_reposition(gtkmenu);
  return self;
}

- insert
 : (id) child
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_insert(gtkmenu, ((Gtk_Widget *)child)->gtkwidget, position);
  return self;
}

- reorder_child
 : (id) child
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_reorder_child(gtkmenu, ((Gtk_Widget *)child)->gtkwidget, position);
  return self;
}

- attach_to_widget
 : (id) attach_widget
 : (GtkMenuDetachFunc) detacher
{
  g_return_val_if_fail([attach_widget isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_attach_to_widget(gtkmenu, ((Gtk_Widget *)attach_widget)->gtkwidget, detacher);
  return self;
}

@end
