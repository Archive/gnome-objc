#include "obgtkSpinButton.h"

@implementation Gtk_SpinButton

- castGtkSpinButton : (GtkSpinButton *) castitem
{
  gtkspinbutton = castitem;
  return [super castGtkEntry:GTK_ENTRY(castitem)];
}

+ (id) makeGtkSpinButton : (GtkSpinButton *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkSpinButton:castitem];

  return retval;
}

- set_value
 : (gfloat) value
{
  gtk_spin_button_set_value(gtkspinbutton, value);
  return self;
}

- configure
 : (id) adjustment
 : (gfloat) climb_rate
 : (guint) digits
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_spin_button_configure(gtkspinbutton, ((Gtk_Adjustment *)adjustment)->gtkadjustment, climb_rate, digits);
  return self;
}

- (gfloat) get_value_as_float
{
  return gtk_spin_button_get_value_as_float(gtkspinbutton);
}

- set_snap_to_ticks
 : (gboolean) snap_to_ticks
{
  gtk_spin_button_set_snap_to_ticks(gtkspinbutton, snap_to_ticks);
  return self;
}

- initWithSpinButtonInfo
 : (id) adjustment
 : (gfloat) climb_rate
 : (guint) digits
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkSpinButton:GTK_SPIN_BUTTON(gtk_spin_button_new(((Gtk_Adjustment *)adjustment)->gtkadjustment, climb_rate, digits))];
}

- set_wrap
 : (gboolean) wrap
{
  gtk_spin_button_set_wrap(gtkspinbutton, wrap);
  return self;
}

- update
{
  gtk_spin_button_update(gtkspinbutton);
  return self;
}

- set_shadow_type
 : (GtkShadowType) shadow_type
{
  gtk_spin_button_set_shadow_type(gtkspinbutton, shadow_type);
  return self;
}

- set_numeric
 : (gboolean) numeric
{
  gtk_spin_button_set_numeric(gtkspinbutton, numeric);
  return self;
}

- set_adjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_spin_button_set_adjustment(gtkspinbutton, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- set_digits
 : (guint) digits
{
  gtk_spin_button_set_digits(gtkspinbutton, digits);
  return self;
}

- spin
 : (GtkSpinType) direction
 : (gfloat) increment
{
  gtk_spin_button_spin(gtkspinbutton, direction, increment);
  return self;
}

- set_update_policy
 : (GtkSpinButtonUpdatePolicy) policy
{
  gtk_spin_button_set_update_policy(gtkspinbutton, policy);
  return self;
}

- (gint) get_value_as_int
{
  return gtk_spin_button_get_value_as_int(gtkspinbutton);
}

- (id) get_adjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_spin_button_get_adjustment(gtkspinbutton)];
}

@end
