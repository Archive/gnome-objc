#ifndef OBGTK_BIN_H
#define OBGTK_BIN_H 1
#include <obgtk/obgtkContainer.h>
#include <gtk/gtkbin.h>

@interface Gtk_Bin : Gtk_Container
{
@public
  GtkBin *gtkbin;
}
- castGtkBin : (GtkBin *) castitem;
+ (id)makeGtkBin : (GtkBin *) castitem;
@end

#endif /* OBGTK_BIN_H */
