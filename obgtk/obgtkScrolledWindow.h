#ifndef OBGTK_SCROLLEDWINDOW_H
#define OBGTK_SCROLLEDWINDOW_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkscrolledwindow.h>

@interface Gtk_ScrolledWindow : Gtk_Bin
{
@public
  GtkScrolledWindow *gtkscrolledwindow;
}
- castGtkScrolledWindow : (GtkScrolledWindow *) castitem;
+ (id)makeGtkScrolledWindow : (GtkScrolledWindow *) castitem;
- set_placement
 : (GtkCornerType) window_placement;
- (id) get_vadjustment;
- set_vadjustment
 : (id) hadjustment;
- (id) get_hadjustment;
- initWithScrolledWindowInfo
 : (id) hadjustment
 : (id) vadjustment;
- set_hadjustment
 : (id) hadjustment;
- add_with_viewport
 : (id) child;
- set_policy
 : (GtkPolicyType) hscrollbar_policy
 : (GtkPolicyType) vscrollbar_policy;
@end

#endif /* OBGTK_SCROLLEDWINDOW_H */
