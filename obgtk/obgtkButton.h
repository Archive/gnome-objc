#ifndef OBGTK_BUTTON_H
#define OBGTK_BUTTON_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkbutton.h>

@interface Gtk_Button : Gtk_Bin
{
@public
  GtkButton *gtkbutton;
}
- castGtkButton : (GtkButton *) castitem;
+ (id)makeGtkButton : (GtkButton *) castitem;
- leave;
- enter;
- clicked;
- (GtkReliefStyle) get_relief;
- initWithLabel
 : (const gchar *) label;
- released;
- init;
- pressed;
- set_relief
 : (GtkReliefStyle) newstyle;
@end

#endif /* OBGTK_BUTTON_H */
