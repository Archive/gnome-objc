#include "obgtkLayout.h"

@implementation Gtk_Layout

- castGtkLayout : (GtkLayout *) castitem
{
  gtklayout = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkLayout : (GtkLayout *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkLayout:castitem];

  return retval;
}

- thaw
{
  gtk_layout_thaw(gtklayout);
  return self;
}

- set_size
 : (guint) width
 : (guint) height
{
  gtk_layout_set_size(gtklayout, width, height);
  return self;
}

- initWithLayoutInfo
 : (id) hadjustment
 : (id) vadjustment
{
  g_return_val_if_fail([hadjustment isKindOf: [Gtk_Adjustment class]], 0);
  g_return_val_if_fail([vadjustment isKindOf: [Gtk_Adjustment class]], 0);
  return [self castGtkLayout:GTK_LAYOUT(gtk_layout_new(((Gtk_Adjustment *)hadjustment)->gtkadjustment, ((Gtk_Adjustment *)vadjustment)->gtkadjustment))];
}

- move
 : (id) widget
 : (gint) x
 : (gint) y
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_layout_move(gtklayout, ((Gtk_Widget *)widget)->gtkwidget, x, y);
  return self;
}

- set_vadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_layout_set_vadjustment(gtklayout, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- put
 : (id) widget
 : (gint) x
 : (gint) y
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_layout_put(gtklayout, ((Gtk_Widget *)widget)->gtkwidget, x, y);
  return self;
}

- set_hadjustment
 : (id) adjustment
{
  g_return_val_if_fail([adjustment isKindOf: [Gtk_Adjustment class]], 0);
  gtk_layout_set_hadjustment(gtklayout, ((Gtk_Adjustment *)adjustment)->gtkadjustment);
  return self;
}

- freeze
{
  gtk_layout_freeze(gtklayout);
  return self;
}

- (id) get_vadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_layout_get_vadjustment(gtklayout)];
}

- (id) get_hadjustment
{
  return [[Gtk_Adjustment class] makeGtkAdjustment:gtk_layout_get_hadjustment(gtklayout)];
}

@end
