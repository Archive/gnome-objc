#ifndef OBGTK_CHECKBUTTON_H
#define OBGTK_CHECKBUTTON_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkToggleButton.h>
#include <gtk/gtkcheckbutton.h>

@interface Gtk_CheckButton : Gtk_ToggleButton
{
@public
  GtkCheckButton *gtkcheckbutton;
}
- castGtkCheckButton : (GtkCheckButton *) castitem;
+ (id)makeGtkCheckButton : (GtkCheckButton *) castitem;
- init;
- initWithLabel
 : (const gchar *) label;
@end

#endif /* OBGTK_CHECKBUTTON_H */
