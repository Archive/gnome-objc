#include "obgtkPacker.h"

@implementation Gtk_Packer

- castGtkPacker : (GtkPacker *) castitem
{
  gtkpacker = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkPacker : (GtkPacker *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkPacker:castitem];

  return retval;
}

- set_default_ipad
 : (guint) i_pad_x
 : (guint) i_pad_y
{
  gtk_packer_set_default_ipad(gtkpacker, i_pad_x, i_pad_y);
  return self;
}

- add_defaults
 : (id) child
 : (GtkSideType) side
 : (GtkAnchorType) anchor
 : (GtkPackerOptions) options
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_packer_add_defaults(gtkpacker, ((Gtk_Widget *)child)->gtkwidget, side, anchor, options);
  return self;
}

- set_default_border_width
 : (guint) border
{
  gtk_packer_set_default_border_width(gtkpacker, border);
  return self;
}

- reorder_child
 : (id) child
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_packer_reorder_child(gtkpacker, ((Gtk_Widget *)child)->gtkwidget, position);
  return self;
}

- set_child_packing
 : (id) child
 : (GtkSideType) side
 : (GtkAnchorType) anchor
 : (GtkPackerOptions) options
 : (guint) border_width
 : (guint) pad_x
 : (guint) pad_y
 : (guint) i_pad_x
 : (guint) i_pad_y
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_packer_set_child_packing(gtkpacker, ((Gtk_Widget *)child)->gtkwidget, side, anchor, options, border_width, pad_x, pad_y, i_pad_x, i_pad_y);
  return self;
}

- set_spacing
 : (guint) spacing
{
  gtk_packer_set_spacing(gtkpacker, spacing);
  return self;
}

- init
{
  return [self castGtkPacker:GTK_PACKER(gtk_packer_new())];
}

- set_default_pad
 : (guint) pad_x
 : (guint) pad_y
{
  gtk_packer_set_default_pad(gtkpacker, pad_x, pad_y);
  return self;
}

- add
 : (id) child
 : (GtkSideType) side
 : (GtkAnchorType) anchor
 : (GtkPackerOptions) options
 : (guint) border_width
 : (guint) pad_x
 : (guint) pad_y
 : (guint) i_pad_x
 : (guint) i_pad_y
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_packer_add(gtkpacker, ((Gtk_Widget *)child)->gtkwidget, side, anchor, options, border_width, pad_x, pad_y, i_pad_x, i_pad_y);
  return self;
}

@end
