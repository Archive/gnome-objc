#include "obgtkMenuShell.h"

@implementation Gtk_MenuShell

- castGtkMenuShell : (GtkMenuShell *) castitem
{
  gtkmenushell = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkMenuShell : (GtkMenuShell *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkMenuShell:castitem];

  return retval;
}

- deselect
{
  gtk_menu_shell_deselect(gtkmenushell);
  return self;
}

- deactivate
{
  gtk_menu_shell_deactivate(gtkmenushell);
  return self;
}

- insert
 : (id) child
 : (gint) position
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_shell_insert(gtkmenushell, ((Gtk_Widget *)child)->gtkwidget, position);
  return self;
}

- append
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_shell_append(gtkmenushell, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- activate_item
 : (id) menu_item
 : (gboolean) force_deactivate
{
  g_return_val_if_fail([menu_item isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_shell_activate_item(gtkmenushell, ((Gtk_Widget *)menu_item)->gtkwidget, force_deactivate);
  return self;
}

- select_item
 : (id) menu_item
{
  g_return_val_if_fail([menu_item isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_shell_select_item(gtkmenushell, ((Gtk_Widget *)menu_item)->gtkwidget);
  return self;
}

- prepend
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_menu_shell_prepend(gtkmenushell, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

@end
