#include "obgtkHandleBox.h"

@implementation Gtk_HandleBox

- castGtkHandleBox : (GtkHandleBox *) castitem
{
  gtkhandlebox = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkHandleBox : (GtkHandleBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHandleBox:castitem];

  return retval;
}

- set_handle_position
 : (GtkPositionType) position
{
  gtk_handle_box_set_handle_position(gtkhandlebox, position);
  return self;
}

- init
{
  return [self castGtkHandleBox:GTK_HANDLE_BOX(gtk_handle_box_new())];
}

- set_shadow_type
 : (GtkShadowType) type
{
  gtk_handle_box_set_shadow_type(gtkhandlebox, type);
  return self;
}

- set_snap_edge
 : (GtkPositionType) edge
{
  gtk_handle_box_set_snap_edge(gtkhandlebox, edge);
  return self;
}

@end
