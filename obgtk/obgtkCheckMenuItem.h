#ifndef OBGTK_CHECKMENUITEM_H
#define OBGTK_CHECKMENUITEM_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkMenuItem.h>
#include <gtk/gtkcheckmenuitem.h>

@interface Gtk_CheckMenuItem : Gtk_MenuItem
{
@public
  GtkCheckMenuItem *gtkcheckmenuitem;
}
- castGtkCheckMenuItem : (GtkCheckMenuItem *) castitem;
+ (id)makeGtkCheckMenuItem : (GtkCheckMenuItem *) castitem;
- set_show_toggle
 : (gboolean) always;
- init;
- toggled;
- set_active
 : (gboolean) is_active;
- initWithLabel
 : (const gchar *) label;
@end

#endif /* OBGTK_CHECKMENUITEM_H */
