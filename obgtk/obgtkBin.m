#include "obgtkBin.h"

@implementation Gtk_Bin

- castGtkBin : (GtkBin *) castitem
{
  gtkbin = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkBin : (GtkBin *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkBin:castitem];

  return retval;
}

@end
