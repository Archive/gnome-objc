#ifndef OBGTK_OBJECT_H
#define OBGTK_OBJECT_H 1

#include <objc/Object.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkobject.h>

extern guint obgtk_objc_id_key;
extern inline guint OBJC_ID(void)
{
  if(!obgtk_objc_id_key)
    obgtk_objc_id_key = g_quark_from_static_string("objc_id");
  return obgtk_objc_id_key;
}

struct _ObgtkRelayInfo {
  id object;
  SEL method;
};
typedef struct _ObgtkRelayInfo *ObgtkRelayInfo;

/* Used for various message & callback relaying stuff */
gpointer
obgtk_signal_relay(GtkObject *object,
		   ObgtkRelayInfo data,
		   gint nargs,
		   GtkArg *args);

/* You can pass this in as a GtkSignalFunc and have the data be
   the selector of the method to call... However it's not perfect */
gpointer
obgtk_callback_relay(GtkObject *object,
		     gpointer data);

@interface Gtk_Object : Object
{
  guint destroy_handler_id;
@public
  GtkObject *gtkobject;
}
- (GtkObject *)getGtkObject;
+ (id)getObjectForGtkObject:(GtkObject *)obj;
- castGtkObject:(GtkObject *)castitem;
- set_user_data:(gpointer) thedata;
- (gpointer)get_user_data;
- set_data:(const gchar *) key
	  :(gpointer) data;
- (gpointer)
  get_data:(const gchar *) key;
- signal_connect:(gchar *) signal_name
      signalFunc:(GtkSignalFunc) signalfunc
	funcData:(gpointer) funcdata;

/* These two methods both connect signals to a method
   - signal_name:(id) signaledObject;

   for '- connect' that signaledObject should always be self.
   */

/* connect sends a message to 'self' when the gtk event in "signal_name"
   occurs. */
- connect:(gchar *) signal_name;

/* connectObj sends a message to signalObject when gtk event in "signal_name"
   occurs */
- connectObj:(gchar *) signal_name
	    :(id) signalObject;

- connectObjMethod:(gchar *) signal_name
		  :(id) signalObject
		  :(SEL) method;

/* A little more freedom here (don't want to break old interfaces :-) */
- connectObj2:(gchar *) signal_name
	     :(id) signalObject
	     :(guint *) ret_handler_id
	     :(SEL) method;

- ref;
- unref;
- sink;

- destroy;
@end

#endif /* OBGTK_OBJECT_H */

