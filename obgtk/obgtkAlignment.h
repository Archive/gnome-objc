#ifndef OBGTK_ALIGNMENT_H
#define OBGTK_ALIGNMENT_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkalignment.h>

@interface Gtk_Alignment : Gtk_Bin
{
@public
  GtkAlignment *gtkalignment;
}
- castGtkAlignment : (GtkAlignment *) castitem;
+ (id)makeGtkAlignment : (GtkAlignment *) castitem;
- initWithAlignmentInfo
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) xscale
 : (gfloat) yscale;
- set
 : (gfloat) xalign
 : (gfloat) yalign
 : (gfloat) xscale
 : (gfloat) yscale;
@end

#endif /* OBGTK_ALIGNMENT_H */
