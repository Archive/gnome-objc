#include "obgtkTearoffMenuItem.h"

@implementation Gtk_TearoffMenuItem

- castGtkTearoffMenuItem : (GtkTearoffMenuItem *) castitem
{
  gtktearoffmenuitem = castitem;
  return [super castGtkMenuItem:GTK_MENU_ITEM(castitem)];
}

+ (id) makeGtkTearoffMenuItem : (GtkTearoffMenuItem *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkTearoffMenuItem:castitem];

  return retval;
}

- init
{
  return [self castGtkTearoffMenuItem:GTK_TEAROFF_MENU_ITEM(gtk_tearoff_menu_item_new())];
}

@end
