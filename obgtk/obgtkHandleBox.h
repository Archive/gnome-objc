#ifndef OBGTK_HANDLEBOX_H
#define OBGTK_HANDLEBOX_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkBin.h>
#include <gtk/gtkhandlebox.h>

@interface Gtk_HandleBox : Gtk_Bin
{
@public
  GtkHandleBox *gtkhandlebox;
}
- castGtkHandleBox : (GtkHandleBox *) castitem;
+ (id)makeGtkHandleBox : (GtkHandleBox *) castitem;
- set_handle_position
 : (GtkPositionType) position;
- init;
- set_shadow_type
 : (GtkShadowType) type;
- set_snap_edge
 : (GtkPositionType) edge;
@end

#endif /* OBGTK_HANDLEBOX_H */
