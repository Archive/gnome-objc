#include "obgtkFrame.h"

@implementation Gtk_Frame

- castGtkFrame : (GtkFrame *) castitem
{
  gtkframe = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkFrame : (GtkFrame *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkFrame:castitem];

  return retval;
}

- set_label
 : (const gchar *) label
{
  gtk_frame_set_label(gtkframe, label);
  return self;
}

- set_shadow_type
 : (GtkShadowType) type
{
  gtk_frame_set_shadow_type(gtkframe, type);
  return self;
}

- set_label_align
 : (gfloat) xalign
 : (gfloat) yalign
{
  gtk_frame_set_label_align(gtkframe, xalign, yalign);
  return self;
}

- initWithFrameInfo
 : (const gchar *) label
{
  return [self castGtkFrame:GTK_FRAME(gtk_frame_new(label))];
}

@end
