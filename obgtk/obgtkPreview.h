#ifndef OBGTK_PREVIEW_H
#define OBGTK_PREVIEW_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkpreview.h>

@interface Gtk_Preview : Gtk_Widget
{
@public
  GtkPreview *gtkpreview;
}
- castGtkPreview : (GtkPreview *) castitem;
+ (id)makeGtkPreview : (GtkPreview *) castitem;
- set_dither
 : (GdkRgbDither) dither;
- set_expand
 : (gint) expand;
- size
 : (gint) width
 : (gint) height;
- draw_row
 : (guchar *) data
 : (gint) x
 : (gint) y
 : (gint) w;
- initWithPreviewInfo
 : (GtkPreviewType) type;
- put
 : (GdkWindow *) window
 : (GdkGC *) gc
 : (gint) srcx
 : (gint) srcy
 : (gint) destx
 : (gint) desty
 : (gint) width
 : (gint) height;
@end

#endif /* OBGTK_PREVIEW_H */
