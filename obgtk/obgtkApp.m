#include "obgtkApp.h"
#include "obgtkObject.h"

@implementation Gtk_App
- initApp:(int *)argcp
      :(char ***)argvp
{
  [super init];
  gtk_init(argcp, argvp);
  return self;
}

- run
{
  gtk_main();
  return self;
}

- quit
{
  gtk_main_quit();
  return self;
}

- (guint) level
{
  return gtk_main_level();
}

- (gchar *)set_locale
{
  return gtk_set_locale();
}

- (gint) events_pending
{
  return gtk_events_pending();
}

- free
{
  [self quit];
  return [super free];
}

- (gint) run_iteration
{
  return gtk_main_iteration();
}

- grab_add:(id)awidget
{
  gtk_grab_add(GTK_WIDGET([awidget getGtkObject]));
  return self;
}

- grab_remove:(id)awidget
{
  gtk_grab_remove(GTK_WIDGET([awidget getGtkObject]));
  return self;
}

- init_add:(GtkFunction) function :(gpointer) data
{
  gtk_init_add(function, data);
  return self;
}

- quit_add_destroy:(guint) main_level :(id) anobject
{
  gtk_quit_add_destroy(main_level, [anobject getGtkObject]);
  return self;
}

- (guint)quit_add:(guint) main_level
 callbackFunction:(GtkFunction) function
     callbackData:(gpointer) data
{
  return gtk_quit_add(main_level, function, data);
}

- quit_remove:(guint) quit_handler_id
{
  gtk_quit_remove(quit_handler_id);
  return self;
}

- quit_remove_by_data:(gpointer) data
{
  gtk_quit_remove_by_data(data);
  return self;
}

- (guint) timeout_add:(guint32) interval
     callbackFunction:(GtkFunction) function
	 callbackData:(gpointer) data
{
  return gtk_timeout_add(interval, function, data);
}

- timeout_remove:(guint) tag
{
  gtk_timeout_remove(tag);
  return self;
}

- (guint) idle_add:(GtkFunction) function
      callbackData:(gpointer) data
{
  return gtk_idle_add(function, data);
}

- (guint) idle_add_priority:(gint) priority
	  callbackFunction:(GtkFunction) function
		   callbackData:(gpointer) data
{
  return gtk_idle_add(function, data);
}

- idle_remove:(gint) tag
{
  gtk_idle_remove(tag);
  return self;
}

- (guint)connect_quit:(guint) main_level
       callbackObject:(id) anObject
       callbackMethod:(SEL) method
{
  ObgtkRelayInfo ri = g_new(struct _ObgtkRelayInfo, 1);
  ri->object = anObject;
  ri->method = method;
  return gtk_quit_add_full(main_level,
			   (GtkFunction)method, /* We have the SEL inside ri, but we have to pass
				      _something_ here */
			   (GtkCallbackMarshal)obgtk_signal_relay,
			   (gpointer)ri,
			   g_free);
}

- (guint) connect_timeout:(guint32) interval
	   callbackObject:(id) anObject
	   callbackMethod:(SEL) method
{
  ObgtkRelayInfo ri = g_new(struct _ObgtkRelayInfo, 1);
  ri->object = anObject;
  ri->method = method;
  return gtk_timeout_add_full(interval,
			      (GtkFunction)method, /* We have the SEL inside ri, but we have to pass
					 _something_ here */
			      (GtkCallbackMarshal)obgtk_signal_relay,
			      ri,
			      g_free);
}

- (guint) connect_idle:(gint) priority
	callbackObject:(id) anObject
	callbackMethod:(SEL) method
{
  ObgtkRelayInfo ri = g_new(struct _ObgtkRelayInfo, 1);
  ri->object = anObject;
  ri->method = method;
  return gtk_idle_add_full(priority,
			   (GtkFunction)method, /* We have the SEL inside ri, but we have to pass
				      _something_ here */
			   (GtkCallbackMarshal)obgtk_signal_relay,
			   ri,
			   g_free);
}

/* This expects a method of the form
   - amethod:(gpointer) data
     inputFD:(gint) source
   inputCondition:(GdkInputCondition) condition
*/
- (guint)connect_input:(gint) source
	inputCondition:(GdkInputCondition) condition
	callbackObject:(id) anObject
	callbackMethod:(SEL) method;
{
  ObgtkRelayInfo ri = g_new(struct _ObgtkRelayInfo, 1);
  ri->object = anObject;
  ri->method = method;
  return gtk_input_add_full(source,
			    condition,
			    (GdkInputFunction)method, /* We have the SEL inside ri, but we have to pass
				       _something_ here */
			    (GtkCallbackMarshal)obgtk_signal_relay,
			    ri,
			    (GtkDestroyNotify)g_free);
}

- input_remove:(guint) input_handler_id
{
  gtk_input_remove(input_handler_id);
  return self;
}

- (guint)key_snooper_install:(GtkKeySnoopFunc) snooper
		callbackData:(gpointer) func_data
{
  return gtk_key_snooper_install(snooper, func_data);
}

- key_snooper_remove:(guint) snooper_handler_id
{
  gtk_key_snooper_remove(snooper_handler_id);
  return self;
}

- (GdkEvent *) get_current_event
{
  return gtk_get_current_event();
}

- (id) get_event_widget:(GdkEvent *) event
{
  return gtk_object_get_data_by_id(GTK_OBJECT(gtk_get_event_widget(event)),
				   OBJC_ID());
}

- (id) grab_get_current
{
  return (id)gtk_object_get_data_by_id(GTK_OBJECT(gtk_grab_get_current()),
				       OBJC_ID());
}
@end
