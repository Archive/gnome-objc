#include "obgtkHBox.h"

@implementation Gtk_HBox

- castGtkHBox : (GtkHBox *) castitem
{
  gtkhbox = castitem;
  return [super castGtkBox:GTK_BOX(castitem)];
}

+ (id) makeGtkHBox : (GtkHBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkHBox:castitem];

  return retval;
}

- init
{
	return [self initWithHBoxInfo:FALSE :5];
}

- initWithHBoxInfo
 : (gboolean) homogeneous
 : (gint) spacing
{
  return [self castGtkHBox:GTK_HBOX(gtk_hbox_new(homogeneous, spacing))];
}

@end
