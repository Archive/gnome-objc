#include "obgtkPlug.h"

@implementation Gtk_Plug

- castGtkPlug : (GtkPlug *) castitem
{
  gtkplug = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkPlug : (GtkPlug *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkPlug:castitem];

  return retval;
}

- construct
 : (guint32) socket_id
{
  gtk_plug_construct(gtkplug, socket_id);
  return self;
}

- initWithPlugInfo
 : (guint32) socket_id
{
  return [self castGtkPlug:GTK_PLUG(gtk_plug_new(socket_id))];
}

@end
