#include "obgtkVSeparator.h"

@implementation Gtk_VSeparator

- castGtkVSeparator : (GtkVSeparator *) castitem
{
  gtkvseparator = castitem;
  return [super castGtkSeparator:GTK_SEPARATOR(castitem)];
}

+ (id) makeGtkVSeparator : (GtkVSeparator *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkVSeparator:castitem];

  return retval;
}

- init
{
  return [self castGtkVSeparator:GTK_VSEPARATOR(gtk_vseparator_new())];
}

@end
