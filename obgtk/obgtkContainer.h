#ifndef OBGTK_CONTAINER_H
#define OBGTK_CONTAINER_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkcontainer.h>

@interface Gtk_Container : Gtk_Widget
{
@public
  GtkContainer *gtkcontainer;
}
- castGtkContainer : (GtkContainer *) castitem;
+ (id)makeGtkContainer : (GtkContainer *) castitem;
- forall
 : (GtkCallback) callback
 : (gpointer) callback_data;
- arg_set
 : (id) child
 : (GtkArg *) arg
 : (GtkArgInfo *) info;
- set_focus_hadjustment
 : (id) adjustment;
- register_toplevel;
- unregister_toplevel;
- remove
 : (id) widget;
- set_focus_vadjustment
 : (id) adjustment;
- (GtkType) child_type;
- foreach
 : (GtkCallback) callback
 : (gpointer) callback_data;
- (GList*) children;
- foreach_full
 : (GtkCallback) callback
 : (GtkCallbackMarshal) marshal
 : (gpointer) callback_data
 : (GtkDestroyNotify) notify;
- add
 : (id) widget;
- child_getv
 : (id) child
 : (guint) n_args
 : (GtkArg *) args;
- clear_resize_widgets;
- check_resize;
- set_focus_child
 : (id) child;
- addv
 : (id) widget
 : (guint) n_args
 : (GtkArg *) args;
- (gint) focus
 : (GtkDirectionType) direction;
- queue_resize;
- (gchar*) child_composite_name
 : (id) child;
- set_border_width
 : (guint) border_width;
- child_setv
 : (id) child
 : (guint) n_args
 : (GtkArg *) args;
- resize_children;
- arg_get
 : (id) child
 : (GtkArg *) arg
 : (GtkArgInfo *) info;
- set_resize_mode
 : (GtkResizeMode) resize_mode;
@end

#endif /* OBGTK_CONTAINER_H */
