#ifndef OBGTK_HSCROLLBAR_H
#define OBGTK_HSCROLLBAR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkScrollbar.h>
#include <gtk/gtkhscrollbar.h>

@interface Gtk_HScrollbar : Gtk_Scrollbar
{
@public
  GtkHScrollbar *gtkhscrollbar;
}
- castGtkHScrollbar : (GtkHScrollbar *) castitem;
+ (id)makeGtkHScrollbar : (GtkHScrollbar *) castitem;
- initWithHScrollbarInfo
 : (id) adjustment;
@end

#endif /* OBGTK_HSCROLLBAR_H */
