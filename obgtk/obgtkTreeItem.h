#ifndef OBGTK_TREEITEM_H
#define OBGTK_TREEITEM_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkItem.h>
#include <gtk/gtktreeitem.h>

@interface Gtk_TreeItem : Gtk_Item
{
@public
  GtkTreeItem *gtktreeitem;
}
- castGtkTreeItem : (GtkTreeItem *) castitem;
+ (id)makeGtkTreeItem : (GtkTreeItem *) castitem;
- collapse;
- remove_subtree;
- initWithLabel
 : (gchar *) label;
- deselect;
- set_subtree
 : (id) subtree;
- select;
- expand;
- init;
@end

#endif /* OBGTK_TREEITEM_H */
