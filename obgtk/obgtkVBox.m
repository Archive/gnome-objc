#include "obgtkVBox.h"

@implementation Gtk_VBox

- castGtkVBox : (GtkVBox *) castitem
{
  gtkvbox = castitem;
  return [super castGtkBox:GTK_BOX(castitem)];
}

+ (id) makeGtkVBox : (GtkVBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkVBox:castitem];

  return retval;
}

- init
{
	return [self initWithVBoxInfo:FALSE :5];
}

- initWithVBoxInfo
 : (gboolean) homogeneous
 : (gint) spacing
{
  return [self castGtkVBox:GTK_VBOX(gtk_vbox_new(homogeneous, spacing))];
}

@end
