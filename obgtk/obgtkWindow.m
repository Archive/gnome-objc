#include "obgtkWindow.h"

@implementation Gtk_Window

- castGtkWindow : (GtkWindow *) castitem
{
  gtkwindow = castitem;
  return [super castGtkBin:GTK_BIN(castitem)];
}

+ (id) makeGtkWindow : (GtkWindow *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkWindow:castitem];

  return retval;
}

- init
{
	return [self initWithWindowInfo:GTK_WINDOW_TOPLEVEL];
}

- set_policy
 : (gint) allow_shrink
 : (gint) allow_grow
 : (gint) auto_shrink
{
  gtk_window_set_policy(gtkwindow, allow_shrink, allow_grow, auto_shrink);
  return self;
}

- set_transient_for
 : (GtkWindow *) parent
{
  gtk_window_set_transient_for(gtkwindow, parent);
  return self;
}

- set_focus
 : (id) focus
{
  g_return_val_if_fail([focus isKindOf: [Gtk_Widget class]], 0);
  gtk_window_set_focus(gtkwindow, ((Gtk_Widget *)focus)->gtkwidget);
  return self;
}

- set_default
 : (id) defaultw
{
  g_return_val_if_fail([defaultw isKindOf: [Gtk_Widget class]], 0);
  gtk_window_set_default(gtkwindow, ((Gtk_Widget *)defaultw)->gtkwidget);
  return self;
}

- add_embedded_xid
 : (guint) xid
{
  gtk_window_add_embedded_xid(gtkwindow, xid);
  return self;
}

- initWithWindowInfo
 : (GtkWindowType) type
{
  return [self castGtkWindow:GTK_WINDOW(gtk_window_new(type))];
}

- (gint) activate_focus
{
  return gtk_window_activate_focus(gtkwindow);
}

- set_title
 : (const gchar *) title
{
  gtk_window_set_title(gtkwindow, title);
  return self;
}

- add_accel_group
 : (id) accel_group
{
  g_return_val_if_fail([accel_group isKindOf: [Gtk_AccelGroup class]], 0);
  gtk_window_add_accel_group(gtkwindow, ((Gtk_AccelGroup *)accel_group)->gtkaccelgroup);
  return self;
}

- set_wmclass
 : (const gchar *) wmclass_name
 : (const gchar *) wmclass_class
{
  gtk_window_set_wmclass(gtkwindow, wmclass_name, wmclass_class);
  return self;
}

- remove_embedded_xid
 : (guint) xid
{
  gtk_window_remove_embedded_xid(gtkwindow, xid);
  return self;
}

- set_position
 : (GtkWindowPosition) position
{
  gtk_window_set_position(gtkwindow, position);
  return self;
}

- (gint) activate_default
{
  return gtk_window_activate_default(gtkwindow);
}

- remove_accel_group
 : (id) accel_group
{
  g_return_val_if_fail([accel_group isKindOf: [Gtk_AccelGroup class]], 0);
  gtk_window_remove_accel_group(gtkwindow, ((Gtk_AccelGroup *)accel_group)->gtkaccelgroup);
  return self;
}

- set_modal
 : (gboolean) modal
{
  gtk_window_set_modal(gtkwindow, modal);
  return self;
}

- set_default_size
 : (gint) width
 : (gint) height
{
  gtk_window_set_default_size(gtkwindow, width, height);
  return self;
}

- set_geometry_hints
 : (id) geometry_widget
 : (GdkGeometry *) geometry
 : (GdkWindowHints) geom_mask
{
  g_return_val_if_fail([geometry_widget isKindOf: [Gtk_Widget class]], 0);
  gtk_window_set_geometry_hints(gtkwindow, ((Gtk_Widget *)geometry_widget)->gtkwidget, geometry, geom_mask);
  return self;
}

@end
