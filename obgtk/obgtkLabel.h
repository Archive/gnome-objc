#ifndef OBGTK_LABEL_H
#define OBGTK_LABEL_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkMisc.h>
#include <gtk/gtklabel.h>

@interface Gtk_Label : Gtk_Misc
{
@public
  GtkLabel *gtklabel;
}
- castGtkLabel : (GtkLabel *) castitem;
+ (id)makeGtkLabel : (GtkLabel *) castitem;
- set_justify
 : (GtkJustification) jtype;
- set_text
 : (const char *) str;
- initWithLabelInfo
 : (const char *) str;
- (guint) parse_uline
 : (const gchar *) string;
- get
 : (char * *) str;
- set_pattern
 : (const gchar *) pattern;
- set_line_wrap
 : (gboolean) wrap;
@end

#endif /* OBGTK_LABEL_H */
