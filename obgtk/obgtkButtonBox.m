#include "obgtkButtonBox.h"

@implementation Gtk_ButtonBox

- castGtkButtonBox : (GtkButtonBox *) castitem
{
  gtkbuttonbox = castitem;
  return [super castGtkBox:GTK_BOX(castitem)];
}

+ (id) makeGtkButtonBox : (GtkButtonBox *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkButtonBox:castitem];

  return retval;
}

- set_spacing
 : (gint) spacing
{
  gtk_button_box_set_spacing(gtkbuttonbox, spacing);
  return self;
}

- set_child_size
 : (gint) min_width
 : (gint) min_height
{
  gtk_button_box_set_child_size(gtkbuttonbox, min_width, min_height);
  return self;
}

- (gint) get_spacing
{
  return gtk_button_box_get_spacing(gtkbuttonbox);
}

- set_child_ipadding
 : (gint) ipad_x
 : (gint) ipad_y
{
  gtk_button_box_set_child_ipadding(gtkbuttonbox, ipad_x, ipad_y);
  return self;
}

- (GtkButtonBoxStyle) get_layout
{
  return gtk_button_box_get_layout(gtkbuttonbox);
}

- get_child_size
 : (gint *) min_width
 : (gint *) min_height
{
  gtk_button_box_get_child_size(gtkbuttonbox, min_width, min_height);
  return self;
}

- get_child_ipadding
 : (gint *) ipad_x
 : (gint *) ipad_y
{
  gtk_button_box_get_child_ipadding(gtkbuttonbox, ipad_x, ipad_y);
  return self;
}

- set_layout
 : (GtkButtonBoxStyle) layout_style
{
  gtk_button_box_set_layout(gtkbuttonbox, layout_style);
  return self;
}

@end
