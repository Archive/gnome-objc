#ifndef OBGTK_RANGE_H
#define OBGTK_RANGE_H 1
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkrange.h>

@interface Gtk_Range : Gtk_Widget
{
@public
  GtkRange *gtkrange;
}
- castGtkRange : (GtkRange *) castitem;
+ (id)makeGtkRange : (GtkRange *) castitem;
- draw_step_back;
- (id) get_adjustment;
- (gint) default_vtrough_click
 : (gint) x
 : (gint) y
 : (gfloat *) jump_perc;
- draw_background;
- (gint) default_htrough_click
 : (gint) x
 : (gint) y
 : (gfloat *) jump_perc;
- default_vslider_update;
- draw_slider;
- default_vmotion
 : (gint) xdelta
 : (gint) ydelta;
- default_hslider_update;
- default_hmotion
 : (gint) xdelta
 : (gint) ydelta;
- (gint) trough_click
 : (gint) x
 : (gint) y
 : (gfloat *) jump_perc;
- set_update_policy
 : (GtkUpdateType) policy;
- set_adjustment
 : (id) adjustment;
- slider_update;
- draw_step_forw;
- clear_background;
- draw_trough;
@end

#endif /* OBGTK_RANGE_H */
