#ifndef OBGTK_HSCALE_H
#define OBGTK_HSCALE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkAdjustment.h>
#include <obgtk/obgtkScale.h>
#include <gtk/gtkhscale.h>

@interface Gtk_HScale : Gtk_Scale
{
@public
  GtkHScale *gtkhscale;
}
- castGtkHScale : (GtkHScale *) castitem;
+ (id)makeGtkHScale : (GtkHScale *) castitem;
- initWithHScaleInfo
 : (id) adjustment;
@end

#endif /* OBGTK_HSCALE_H */
