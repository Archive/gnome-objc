#include "obgtkVPaned.h"

@implementation Gtk_VPaned

- castGtkVPaned : (GtkVPaned *) castitem
{
  gtkvpaned = castitem;
  return [super castGtkPaned:GTK_PANED(castitem)];
}

+ (id) makeGtkVPaned : (GtkVPaned *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkVPaned:castitem];

  return retval;
}

- init
{
  return [self castGtkVPaned:GTK_VPANED(gtk_vpaned_new())];
}

@end
