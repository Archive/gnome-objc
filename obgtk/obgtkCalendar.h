#ifndef OBGTK_CALENDAR_H
#define OBGTK_CALENDAR_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkWidget.h>
#include <gtk/gtkcalendar.h>

@interface Gtk_Calendar : Gtk_Widget
{
@public
  GtkCalendar *gtkcalendar;
}
- castGtkCalendar : (GtkCalendar *) castitem;
+ (id)makeGtkCalendar : (GtkCalendar *) castitem;
- freeze;
- get_date
 : (guint *) year
 : (guint *) month
 : (guint *) day;
- (gint) mark_day
 : (guint) day;
- (gint) select_month
 : (guint) month
 : (guint) year;
- init;
- thaw;
- display_options
 : (GtkCalendarDisplayOptions) flags;
- clear_marks;
- (gint) unmark_day
 : (guint) day;
- select_day
 : (guint) day;
@end

#endif /* OBGTK_CALENDAR_H */
