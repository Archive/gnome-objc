#include "obgtkTipsQuery.h"

@implementation Gtk_TipsQuery

- castGtkTipsQuery : (GtkTipsQuery *) castitem
{
  gtktipsquery = castitem;
  return [super castGtkLabel:GTK_LABEL(castitem)];
}

+ (id) makeGtkTipsQuery : (GtkTipsQuery *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkTipsQuery:castitem];

  return retval;
}

- set_caller
 : (id) caller
{
  g_return_val_if_fail([caller isKindOf: [Gtk_Widget class]], 0);
  gtk_tips_query_set_caller(gtktipsquery, ((Gtk_Widget *)caller)->gtkwidget);
  return self;
}

- stop_query
{
  gtk_tips_query_stop_query(gtktipsquery);
  return self;
}

- start_query
{
  gtk_tips_query_start_query(gtktipsquery);
  return self;
}

- init
{
  return [self castGtkTipsQuery:GTK_TIPS_QUERY(gtk_tips_query_new())];
}

- set_labels
 : (const gchar *) label_inactive
 : (const gchar *) label_no_tip
{
  gtk_tips_query_set_labels(gtktipsquery, label_inactive, label_no_tip);
  return self;
}

@end
