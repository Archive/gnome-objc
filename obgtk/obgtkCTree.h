#ifndef OBGTK_CTREE_H
#define OBGTK_CTREE_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkCList.h>
#include <gtk/gtkctree.h>

@interface Gtk_CTree : Gtk_CList
{
@public
  GtkCTree *gtkctree;
}
- castGtkCTree : (GtkCTree *) castitem;
+ (id)makeGtkCTree : (GtkCTree *) castitem;
- set_line_style
 : (GtkCTreeLineStyle) line_style;
- sort_recursive
 : (GtkCTreeNode *) node;
- (gboolean) is_viewable
 : (GtkCTreeNode *) node;
- select
 : (GtkCTreeNode *) node;
- (GtkCTreeNode *) insert_node
 : (GtkCTreeNode *) parent
 : (GtkCTreeNode *) sibling
 : (gchar **) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap_closed
 : (GdkBitmap *) mask_closed
 : (GdkPixmap *) pixmap_opened
 : (GdkBitmap *) mask_opened
 : (gboolean) is_leaf
 : (gboolean) expanded;
- (GtkCTreeNode *) insert_gnode
 : (GtkCTreeNode *) parent
 : (GtkCTreeNode *) sibling
 : (GNode *) gnode
 : (GtkCTreeGNodeFunc) func
 : (gpointer) data;
- node_set_pixtext
 : (GtkCTreeNode *) node
 : (gint) column
 : (const gchar *) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask;
- node_set_text
 : (GtkCTreeNode *) node
 : (gint) column
 : (const gchar *) text;
- (gboolean) is_ancestor
 : (GtkCTreeNode *) node
 : (GtkCTreeNode *) child;
- post_recursive
 : (GtkCTreeNode *) node
 : (GtkCTreeFunc) func
 : (gpointer) data;
- initWithCTreeInfo
 : (gint) columns
 : (gint) tree_column;
- node_set_selectable
 : (GtkCTreeNode *) node
 : (gboolean) selectable;
- (GList *) find_all_by_row_data
 : (GtkCTreeNode *) node
 : (gpointer) data;
- construct
 : (gint) columns
 : (gint) tree_column
 : (gchar **) titles;
- (GtkStyle *) node_get_cell_style
 : (GtkCTreeNode *) node
 : (gint) column;
- set_indent
 : (gint) indent;
- (gboolean) is_hot_spot
 : (gint) x
 : (gint) y;
- node_set_foreground
 : (GtkCTreeNode *) node
 : (GdkColor *) color;
- real_select_recursive
 : (GtkCTreeNode *) node
 : (gint) state;
- (GtkCTreeNode *) find_by_row_data_custom
 : (GtkCTreeNode *) node
 : (gpointer) data
 : (GCompareFunc) func;
- (gint) node_get_pixtext
 : (GtkCTreeNode *) node
 : (gint) column
 : (gchar * *) text
 : (guint8 *) spacing
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask;
- (gint) node_get_text
 : (GtkCTreeNode *) node
 : (gint) column
 : (gchar * *) text;
- set_drag_compare_func
 : (GtkCTreeCompareDragFunc) cmp_func;
- initWithTitles
 : (gint) columns
 : (gint) tree_column
 : (gchar **) titles;
- expand
 : (GtkCTreeNode *) node;
- unselect
 : (GtkCTreeNode *) node;
- (GNode *) export_to_gnode
 : (GNode *) parent
 : (GNode *) sibling
 : (GtkCTreeNode *) node
 : (GtkCTreeGNodeFunc) func
 : (gpointer) data;
- expand_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth;
- (GtkVisibility) node_is_visible
 : (GtkCTreeNode *) node;
- set_expander_style
 : (GtkCTreeExpanderStyle) expander_style;
- (gpointer) node_get_row_data
 : (GtkCTreeNode *) node;
- (GtkCTreeNode *) last
 : (GtkCTreeNode *) node;
- sort_node
 : (GtkCTreeNode *) node;
- toggle_expansion_recursive
 : (GtkCTreeNode *) node;
- node_set_row_data
 : (GtkCTreeNode *) node
 : (gpointer) data;
- (gboolean) find
 : (GtkCTreeNode *) node
 : (GtkCTreeNode *) child;
- (GtkCTreeNode *) find_by_row_data
 : (GtkCTreeNode *) node
 : (gpointer) data;
- (gint) node_get_pixmap
 : (GtkCTreeNode *) node
 : (gint) column
 : (GdkPixmap * *) pixmap
 : (GdkBitmap * *) mask;
- remove_node
 : (GtkCTreeNode *) node;
- (gint) get_node_info
 : (GtkCTreeNode *) node
 : (gchar * *) text
 : (guint8 *) spacing
 : (GdkPixmap * *) pixmap_closed
 : (GdkBitmap * *) mask_closed
 : (GdkPixmap * *) pixmap_opened
 : (GdkBitmap * *) mask_opened
 : (gboolean *) is_leaf
 : (gboolean *) expanded;
- set_spacing
 : (gint) spacing;
- set_node_info
 : (GtkCTreeNode *) node
 : (const gchar *) text
 : (guint8) spacing
 : (GdkPixmap *) pixmap_closed
 : (GdkBitmap *) mask_closed
 : (GdkPixmap *) pixmap_opened
 : (GdkBitmap *) mask_opened
 : (gboolean) is_leaf
 : (gboolean) expanded;
- unselect_recursive
 : (GtkCTreeNode *) node;
- node_set_shift
 : (GtkCTreeNode *) node
 : (gint) column
 : (gint) vertical
 : (gint) horizontal;
- collapse_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth;
- node_set_row_data_full
 : (GtkCTreeNode *) node
 : (gpointer) data
 : (GtkDestroyNotify) destroy;
- (GtkCellType) node_get_cell_type
 : (GtkCTreeNode *) node
 : (gint) column;
- node_set_cell_style
 : (GtkCTreeNode *) node
 : (gint) column
 : (GtkStyle *) style;
- (gboolean) node_get_selectable
 : (GtkCTreeNode *) node;
- collapse
 : (GtkCTreeNode *) node;
- (GtkCTreeNode *) node_nth
 : (guint) row;
- toggle_expansion
 : (GtkCTreeNode *) node;
- node_set_row_style
 : (GtkCTreeNode *) node
 : (GtkStyle *) style;
- select_recursive
 : (GtkCTreeNode *) node;
- node_set_background
 : (GtkCTreeNode *) node
 : (GdkColor *) color;
- (GtkStyle *) node_get_row_style
 : (GtkCTreeNode *) node;
- set_show_stub
 : (gboolean) show_stub;
- move
 : (GtkCTreeNode *) node
 : (GtkCTreeNode *) new_parent
 : (GtkCTreeNode *) new_sibling;
- node_moveto
 : (GtkCTreeNode *) node
 : (gint) column
 : (gfloat) row_align
 : (gfloat) col_align;
- (GtkCTreeNode *) find_node_ptr
 : (GtkCTreeRow *) ctree_row;
- pre_recursive
 : (GtkCTreeNode *) node
 : (GtkCTreeFunc) func
 : (gpointer) data;
- collapse_recursive
 : (GtkCTreeNode *) node;
- node_set_pixmap
 : (GtkCTreeNode *) node
 : (gint) column
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask;
- post_recursive_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth
 : (GtkCTreeFunc) func
 : (gpointer) data;
- pre_recursive_to_depth
 : (GtkCTreeNode *) node
 : (gint) depth
 : (GtkCTreeFunc) func
 : (gpointer) data;
- expand_recursive
 : (GtkCTreeNode *) node;
- (GList *) find_all_by_row_data_custom
 : (GtkCTreeNode *) node
 : (gpointer) data
 : (GCompareFunc) func;
@end

#endif /* OBGTK_CTREE_H */
