#include "obgtkFontSelection.h"

@implementation Gtk_FontSelection

- castGtkFontSelection : (GtkFontSelection *) castitem
{
  gtkfontselection = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

+ (id) makeGtkFontSelection : (GtkFontSelection *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkFontSelection:castitem];

  return retval;
}

- initWithFontSelectionInfo
 : (const gchar *) title
{
  return [self castGtkFontSelection:GTK_FONT_SELECTION(gtk_font_selection_dialog_new(title))];
}

- init
{
  return [self castGtkFontSelection:GTK_FONT_SELECTION(gtk_font_selection_new())];
}

@end
