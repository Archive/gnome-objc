#ifndef OBGTK_APP_H
#define OBGTK_APP_H 1

#include <obgtk/obgtkWidget.h>
#include <gtk/gtkmain.h>

@interface Gtk_App : Object
- initApp:(int *)argcp
      :(char ***)argvp;
- run;
- quit;
- (guint) level;
- (gchar *) set_locale;
- (gint) events_pending;
- (gint) run_iteration;
- grab_add:(id)awidget;
- (id) grab_get_current;
- grab_remove:(id)awidget;

- init_add:(GtkFunction) function :(gpointer) data;

- quit_add_destroy:(guint) main_level :(id) anobject;
- (guint)quit_add:(guint) main_level
 callbackFunction:(GtkFunction) function
     callbackData:(gpointer) data;
- quit_remove:(guint) quit_handler_id;
- quit_remove_by_data:(gpointer) data;

- (guint) timeout_add:(guint32) interval
     callbackFunction:(GtkFunction) function
	 callbackData:(gpointer) data;
- timeout_remove:(guint) tag;

- (guint) idle_add:(GtkFunction) function
      callbackData:(gpointer) data;
- (guint) idle_add_priority:(gint)priority
	   callbackFunction:(GtkFunction) function
	      callbackData:(gpointer) data;
- idle_remove:(gint) tag;

/* These three expect methods of the form
   - amethod:(gpointer) data
   to be passed in */
- (guint)connect_quit:(guint) main_level
       callbackObject:(id) anObject
       callbackMethod:(SEL) method;
- (guint) connect_timeout:(guint32) interval
	   callbackObject:(id) anObject
	   callbackMethod:(SEL) method;
- (guint) connect_idle:(gint) priority
	callbackObject:(id) anObject
	callbackMethod:(SEL) method;

/* This expects a method of the form
   - amethod:(gpointer) data
     inputFD:(gint) source
   inputCondition:(GdkInputCondition) condition
*/
- (guint)connect_input:(gint) source
	inputCondition:(GdkInputCondition) condition
	callbackObject:(id) anObject
	callbackMethod:(SEL) method;
- input_remove:(guint) input_handler_id;

- (guint)key_snooper_install:(GtkKeySnoopFunc) snooper
		callbackData:(gpointer) func_data;
- key_snooper_remove:(guint) snooper_handler_id;
- (GdkEvent *) get_current_event;
- (id) get_event_widget:(GdkEvent *) event;
@end

#endif /* OBGTK_APP_H */

