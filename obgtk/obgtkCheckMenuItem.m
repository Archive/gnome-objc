#include "obgtkCheckMenuItem.h"

@implementation Gtk_CheckMenuItem

- castGtkCheckMenuItem : (GtkCheckMenuItem *) castitem
{
  gtkcheckmenuitem = castitem;
  return [super castGtkMenuItem:GTK_MENU_ITEM(castitem)];
}

+ (id) makeGtkCheckMenuItem : (GtkCheckMenuItem *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkCheckMenuItem:castitem];

  return retval;
}

- set_show_toggle
 : (gboolean) always
{
  gtk_check_menu_item_set_show_toggle(gtkcheckmenuitem, always);
  return self;
}

- init
{
  return [self castGtkCheckMenuItem:GTK_CHECK_MENU_ITEM(gtk_check_menu_item_new())];
}

- toggled
{
  gtk_check_menu_item_toggled(gtkcheckmenuitem);
  return self;
}

- set_active
 : (gboolean) is_active
{
  gtk_check_menu_item_set_active(gtkcheckmenuitem, is_active);
  return self;
}

- initWithLabel
 : (const gchar *) label
{
  return [self castGtkCheckMenuItem:GTK_CHECK_MENU_ITEM(gtk_check_menu_item_new_with_label(label))];
}

@end
