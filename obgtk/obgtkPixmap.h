#ifndef OBGTK_PIXMAP_H
#define OBGTK_PIXMAP_H 1
#include <obgtk/obgtkWidget.h>
#include <obgtk/obgtkMisc.h>
#include <gtk/gtkpixmap.h>

@interface Gtk_Pixmap : Gtk_Misc
{
@public
  GtkPixmap *gtkpixmap;
}
- castGtkPixmap : (GtkPixmap *) castitem;
+ (id)makeGtkPixmap : (GtkPixmap *) castitem;
- initWithPixmapInfo
 : (GdkPixmap *) pixmap
 : (GdkBitmap *) mask;
- set_build_insensitive
 : (guint) build;
- get
 : (GdkPixmap * *) val
 : (GdkBitmap * *) mask;
- set
 : (GdkPixmap *) val
 : (GdkBitmap *) mask;
@end

#endif /* OBGTK_PIXMAP_H */
