#include "obgtkArrow.h"

@implementation Gtk_Arrow

- castGtkArrow : (GtkArrow *) castitem
{
  gtkarrow = castitem;
  return [super castGtkMisc:GTK_MISC(castitem)];
}

+ (id) makeGtkArrow : (GtkArrow *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkArrow:castitem];

  return retval;
}

- initWithArrowInfo
 : (GtkArrowType) arrow_type
 : (GtkShadowType) shadow_type
{
  return [self castGtkArrow:GTK_ARROW(gtk_arrow_new(arrow_type, shadow_type))];
}

- set
 : (GtkArrowType) arrow_type
 : (GtkShadowType) shadow_type
{
  gtk_arrow_set(gtkarrow, arrow_type, shadow_type);
  return self;
}

@end
