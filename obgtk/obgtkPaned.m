#include "obgtkPaned.h"

@implementation Gtk_Paned

- castGtkPaned : (GtkPaned *) castitem
{
  gtkpaned = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkPaned : (GtkPaned *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkPaned:castitem];

  return retval;
}

- set_handle_size
 : (guint16) size
{
  gtk_paned_set_handle_size(gtkpaned, size);
  return self;
}

- compute_position
 : (gint) allocation
 : (gint) child1_req
 : (gint) child2_req
{
  gtk_paned_compute_position(gtkpaned, allocation, child1_req, child2_req);
  return self;
}

- pack1
 : (id) child
 : (gboolean) resize
 : (gboolean) shrink
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_paned_pack1(gtkpaned, ((Gtk_Widget *)child)->gtkwidget, resize, shrink);
  return self;
}

- pack2
 : (id) child
 : (gboolean) resize
 : (gboolean) shrink
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_paned_pack2(gtkpaned, ((Gtk_Widget *)child)->gtkwidget, resize, shrink);
  return self;
}

- add1
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_paned_add1(gtkpaned, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- add2
 : (id) child
{
  g_return_val_if_fail([child isKindOf: [Gtk_Widget class]], 0);
  gtk_paned_add2(gtkpaned, ((Gtk_Widget *)child)->gtkwidget);
  return self;
}

- set_position
 : (gint) position
{
  gtk_paned_set_position(gtkpaned, position);
  return self;
}

- set_gutter_size
 : (guint16) size
{
  gtk_paned_set_gutter_size(gtkpaned, size);
  return self;
}

@end
