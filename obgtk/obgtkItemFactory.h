/* obgtkITemFactory: Copyright (C) 1999 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/****  WARNING : IMMATURE BINDING *****/

#ifndef OBGTK_ITEMFACTORY_H
#define OBGTK_ITEMFACTORY_H 1

#include <obgtk/obgtk.h>

@interface Gtk_ItemFactory : Gtk_Object
{
  GtkItemFactory *gtkitemfactory;
}

+ parseRc:(const gchar *) filename;
+ parseRcString:(const gchar *) string;
+ parseRcScanner:(GScanner *) scanner;
+ fromWidget:(Gtk_Widget *)widget;
+ (gchar *) pathFromWidget:(Gtk_Widget *) widget;
+ dumpItemsWithPathSpec:(GtkPatternSpec *) path_pspec
	   modifiedOnly:(gboolean) modified_only
	      printFunc:(GtkPrintFunc) print_func
	       funcData:(gpointer) func_data;
+ dumpRc        :(const gchar *) filename
 pathPatternSpec:(GtkPatternSpec *) path_pspec
    modifiedOnly:(gboolean) modified_only;
+ printFunc:(gpointer) file_pointer
     String:(gchar *)string;
+ (gpointer) getDataFromWidget: (Gtk_Widget *)widget;


- castGtkItemFactory:(GtkItemFactory *) castitem;
- initWithContainerType:(GtkType) type
		   path:(const gchar *) path
	     accelGroup:(Gtk_AccelGroup *) accelerator;
- constructWithContainerType: (GtkType) type
			path:(const gchar *) path
		  accelGroup:(Gtk_AccelGroup *) accelerator;

- (Gtk_Widget *) getWidget:(const gchar *) path;
- createItemWithEntry: (GtkItemFactoryEntry *) entry
	 callbackData:(gpointer) callback_data
	 callbackType:(guint) callback_type;
- createItemsWithNEntries: (guint) n_entries
		  entries:(GtkItemFactoryEntry *) entries
	     callbackData:(gpointer) callback_data;
- deleteItemWithPath: (const gchar *) path;
- deleteEntry: (GtkItemFactoryEntry *) entry;
- deleteNEntries: (guint) n_entries
	 entries:(GtkItemFactoryEntry *) entries;
- popupWithX:(guint) x
	   y:(guint) y
 mouseButton:(guint) mouse_button
	time:(guint32) time;
- popupWithData:(gpointer) data
  destroyNotify:(GtkDestroyNotify) destroy
	      x:(guint) x
	      y:(guint) y
 mouseButton:(guint) mouse_button
	time:(guint32) time;
- (gpointer) getPopupData;
- setTranslateFunc:(GtkTranslateFunc) func
	     data:(gpointer) data
    destroyNotify:(GtkDestroyNotify) destroy;


@end

#endif /* OBGTK_ITEMFACTORY_H */


