#include "obgtkFixed.h"

@implementation Gtk_Fixed

- castGtkFixed : (GtkFixed *) castitem
{
  gtkfixed = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

+ (id) makeGtkFixed : (GtkFixed *) castitem
{
  id retval;
  retval = [[Gtk_Object class] getObjectForGtkObject:GTK_OBJECT(castitem)];
  if(!retval && castitem)
    retval = [[self alloc] castGtkFixed:castitem];

  return retval;
}

- move
 : (id) widget
 : (gint16) x
 : (gint16) y
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_fixed_move(gtkfixed, ((Gtk_Widget *)widget)->gtkwidget, x, y);
  return self;
}

- init
{
  return [self castGtkFixed:GTK_FIXED(gtk_fixed_new())];
}

- put
 : (id) widget
 : (gint16) x
 : (gint16) y
{
  g_return_val_if_fail([widget isKindOf: [Gtk_Widget class]], 0);
  gtk_fixed_put(gtkfixed, ((Gtk_Widget *)widget)->gtkwidget, x, y);
  return self;
}

@end
