#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Gnome Objective C Libraries"

(test -f $srcdir/configure.in \
  && test -d $srcdir/obgtk \
  && test -d $srcdir/obgnome) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome-objc directory"
    exit 1
}

. $srcdir/macros/autogen.sh
