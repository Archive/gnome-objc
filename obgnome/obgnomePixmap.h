/* obgnomePixmap.h: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME Pixmap routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef OBGNOME_PIXMAP_H
#define OBGNOME_PIXMAP_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"

@interface Gnome_Pixmap : Gtk_Widget
{
@public
  GnomePixmap *gnomepixmap;
}
- castGnomePixmap:(GnomePixmap *) castitem;
- initImageFromFile:(char *) filename;
- initImageFromFileAtSize:(char *) filename :(int) width :(int) height;
- initImageFromXpmData:(gchar **) xpm_data;
- initImageFromXpmDataAtSize:(gchar **) xpm_data
        :(gint) width
        :(gint) height;
- initImageFromRGBData:(guchar *) data
        :(guchar *) alpha
        :(gint) rgb_width
        :(gint) rgb_height;
- initImageFromRGBDataShaped:(guchar *) data
        :(guchar *) alpha
        :(gint) rgb_width
        :(gint) rgb_height
        :(GdkImlibColor *) shape_color;
- initImageFromRGBDataAtSize:(guchar *) data
        :(guchar *) alpha
        :(gint) rgb_width 
        :(gint) rgb_height
        :(gint) width
        :(gint) height;
- loadImageFromFile:(GnomePixmap *) gpixmap
        :(gchar *) filename;
@end

#endif /* OBGNOME_PIXMAP_H */
