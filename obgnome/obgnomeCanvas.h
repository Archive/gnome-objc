/* obgnomeCanvas.h: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#ifndef OBGNOME_CANVAS_H
#define OBGNOME_CANVAS_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"


#include <obgtk/obgtkObject.h>
#include <obgtk/obgtkLayout.h>
#include <libgnomeui/gnome-canvas.h>
@class Gnome_Canvas_Group;
@class Gnome_Canvas;

@interface Gnome_Canvas_Item : Gtk_Object
{
@public
GnomeCanvasItem *gnomecanvasitem;
}

 - castGnomeCanvasItem:(GnomeCanvasItem *) castitem;
- (GtkType)getItemType;
- castToGnomeCanvasItem: (id) castitem;
- initWithGroup: ( Gnome_Canvas_Group *) group
  : (const gchar *) first_arg_name, ... ;
- initWithGroup: ( Gnome_Canvas_Group *) group
	  nArgs: (guint) n
	   Args: (GtkArg *) args ;
- set: (const gchar *) first_arg_name, ...; 
- set: (guint) n
  Args: (GtkArg *) args ;
- move: (double) dx
    dy: (double) dy;
- raise: (int) positions;
- lower: (int) positions;
- raiseToTop;
- lowerToBottom;
- (int)grab: (unsigned int) event_mask
     cursor: (GdkCursor *) cursor
      etime: (guint32) etime;
- ungrab : (guint32) etime;
- w2i : (double *) x
    y : (double *) y ;
- i2w : (double *) x
    y : (double *) y ;
- (Gnome_Canvas_Item *)getParent;

@end


// ****************************************************

@interface Gnome_Canvas_Group : Gnome_Canvas_Item
{
@public
  GnomeCanvasGroup *gnomecanvasgroup;
}
- castGnomeCanvasGroup: (GnomeCanvasGroup *) castitem;
- initWithRootGroup: (Gnome_Canvas_Group *) rootGroup;
- initWithRootCanvas: (Gnome_Canvas *) rootCanvas;


@end



// ****************************************************

@interface Gnome_Canvas : Gtk_Layout
{
@public
  GnomeCanvas *gnomecanvas;
}
- castGnomeCanvas:(GnomeCanvas *) castitem;
- setScrollRegion: (double) x1
		 y1: (double) y1
		 x2: (double) x2
		 y2: (double) y2;
- getScrollRegion: (double *) x1
		 y1: (double *) y1
		 x2: (double *) x2
		 y2: (double *) y2;

- setPixelsPerUnit: (double) n;

- scrollTo: (int) cx
	cy: (int) cy;

- getScrollOffsets: (int*) cx
		cy: (int*) cy;
- updateNow;
- requestRedraw: (int) x1
	     y1: (int) y1
	     x2: (int) x2
	     y2: (int) y2;
- w2c: (double) wx
   wy: (double) wy
   cx: (int *) cx
   cy: (int *) cy ;
- c2w: (int ) cx
   cy: (int ) cy
   wx: (double *) wx
   wy: (double *) wy ;
- (int)getColor: (char *)spec
     color: (GdkColor *)color ;

@end


#endif /* OBGNOME_CANVAS_H */
