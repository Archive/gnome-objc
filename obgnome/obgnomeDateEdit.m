/* obgnomeDateEdit.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeDateEdit.h"

@implementation Gnome_Date_Edit
- initWithDate:(time_t) the_time
	      :(gint) show_time
	      :(gint) use_24_format
{
  return [self castGnomeDateEdit:GNOME_DATE_EDIT(gnome_date_edit_new(the_time, show_time, use_24_format))];
}

- castGnomeDateEdit:(GnomeDateEdit *) castitem
{
  gnomedateedit = castitem;
  return [super castGtkHBox:GTK_HBOX(castitem)];
}

- set_time:(time_t) the_time
{
  gnome_date_edit_set_time(gnomedateedit, the_time);
  return self;
}

- set_popup_range:(gint) low_hour
		 :(gint) up_hour
{
  gnome_date_edit_set_popup_range(gnomedateedit, low_hour, up_hour);
  return self;
}

- (time_t) get_date
{
  return (gnome_date_edit_get_date(gnomedateedit));
}
@end
