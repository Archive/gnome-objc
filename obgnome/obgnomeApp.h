#ifndef OBGNOME_APP_H
#define OBGNOME_APP_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"

@interface Gnome_App : Gtk_App
- initApp:(const char *)app_id
	 :(const char *)app_version
	 :(int)argc
	 :(char **)argv;
- initAppWithPoptTable:(const char *)app_id
		      :(const char *)app_version
		      :(int)argc
		      :(char **)argv
		      :(struct poptOption *) options
		      :(int) poptFlags
		      :(poptContext *) return_ctx;
@end

#endif /* OBGNOME_APP_H */
