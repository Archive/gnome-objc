#include "obgnomeTTY.h"

@implementation Gtk_TTY
- castGtkTTY:(GtkTty *) castitem
{
  gtktty = castitem;
  return [super castGtkTerm:GTK_TERM(castitem)];
}

- initWithTTYInfo:(guint) width
		 :(guint) height
		 :(guint) scrollback
{
  return [self castGtkTTY:GTK_TTY(gtk_tty_new(width,
					      height,
					      scrollback))];
}

- put_out:(const guchar *) buffer
	 :(guint) count
{
  gtk_tty_put_out(gtktty, buffer, count);
  return self;
}

- put_in:(const guchar *) buffer
	:(guint) count
{
  gtk_tty_put_in(gtktty, buffer, count);
  return self;
}

- leds_changed;
{
  gtk_tty_leds_changed(gtktty);
  return self;
}

- add_update_led:(Gtk_LED *) led
		:(GtkTtyKeyStateBits) mask;
{
  gtk_tty_add_update_led(gtktty, GTK_LED(led), mask);
  return self;
}

- execute:(const gchar *) prg_name
	 :(gchar **) argv
	 :(gchar **) envp;
{
  gtk_tty_execute(gtktty, prg_name, argv, envp);
  return self;
}

- change_emu:(Gtk_VtEmu *) vtemu
{
  gtk_tty_change_emu(gtktty, vtemu->gtkvtemu);
  return self;
}
@end
