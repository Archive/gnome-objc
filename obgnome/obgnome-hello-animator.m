#include "obgnome-hello-animator.h"

@implementation animatedButton
-init
{
  id vbox;
  char *s;
  [super init];
  vbox = [[Gtk_VBox new] show];
  theanimator = [ [Gnome_Animator alloc] initWithWidth:48 height:48];
  //[self set_border_width:4];

  //s = gnome_pixmap_file ("mailcheck/email.png");
  /*gnome_animator_append_frames_from_file (GNOME_ANIMATOR (the_third_animator),
    s, 0, 0, 150, 48);*/

  [theanimator  appendFramesFromFile:"toto.png" xOffest:0 yOffset:0
		  interval:100 xUnit:48 ];
  [self add:vbox ];
  [vbox pack_start:theanimator :FALSE :TRUE :2];
  [theanimator setLoopType:GNOME_ANIMATOR_LOOP_PING_PONG];
  
  //[theanimator start];
  [theanimator show];
  [self connectObjMethod:"enter" :self :@selector(anim_start:)];
  [self connectObjMethod:"leave" :self :@selector(anim_stop:)];


  return self;
}

-anim_start:(id) myObj
{
  [theanimator start];
  return self;
}

-anim_stop:(id) myObj
{
  [theanimator stop];
  return self;
}


@end
