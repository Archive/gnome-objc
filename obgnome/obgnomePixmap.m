/* obgnomePixmap.m: Copyright (C) 1998 Free Software Foundation
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomePixmap.h"

@implementation Gnome_Pixmap
- init
{
  [self error:"Gnome_Pixmap: attempt to call \"init\" method on a class requiring parameters for initialization.."];
  return self;
}

- castGnomePixmap:(GnomePixmap *) castitem
{
  gnomepixmap = castitem;
  return [super castGtkWidget:GTK_WIDGET(gnomepixmap)];
}

- initImageFromFile:(gchar *) filename
{
  return [self castGnomePixmap:GNOME_PIXMAP(gnome_pixmap_new_from_file(filename))];
}

- initImageFromFileAtSize:(gchar *) filename
			:(gint) width
			:(gint) height
{
  return [self castGnomePixmap:GNOME_PIXMAP(gnome_pixmap_new_from_file_at_size(filename, width, height))];
}

- initImageFromXpmData:(gchar **) xpm_data
{
  return [self castGnomePixmap:GNOME_PIXMAP(gnome_pixmap_new_from_xpm_d(xpm_data))];
}

- initImageFromXpmDataAtSize:(gchar **) xpm_data
	:(gint) width
	:(gint) height
{
  return [self castGnomePixmap:GNOME_PIXMAP(gnome_pixmap_new_from_xpm_d_at_size(xpm_data, width, height))];
}

- initImageFromRGBData:(guchar *) data
	:(guchar *) alpha
	:(gint) rgb_width
	:(gint) rgb_height
{
  return [self castGnomePixmap:GNOME_PIXMAP(gnome_pixmap_new_from_rgb_d(data, alpha, rgb_width, rgb_height))];
}

- initImageFromRGBDataShaped:(guchar *) data
	:(guchar *) alpha
	:(gint) rgb_width
	:(gint) rgb_height
	:(GdkImlibColor *) shape_color
{
  return [self castGnomePixmap:GNOME_PIXMAP(gnome_pixmap_new_from_rgb_d_shaped(data,
                alpha, rgb_width, rgb_height, shape_color))];
}

- initImageFromRGBDataAtSize:(guchar *) data
	:(guchar *) alpha
	:(gint) rgb_width
	:(gint) rgb_height
	:(gint) width
	:(gint) height
{
  return [self castGnomePixmap:GNOME_PIXMAP(gnome_pixmap_new_from_rgb_d_at_size(data,
                alpha, rgb_width, rgb_height, width, height))];
}

- loadImageFromFile:(GnomePixmap *) gpixmap
	:(gchar *) filename
{
  gnome_pixmap_load_file (gpixmap, filename);
  return self;
}
@end
