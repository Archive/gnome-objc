#include <config.h>
#include <gnome.h>
#include "obgnome.h"
#include "obgnome-hello-canvas.h"

#ifndef PACKAGE
#define PACKAGE "gnome-objc"
#endif

GnomeUIInfo filemenu[] = {
  GNOMEUIINFO_MENU_PROPERTIES_ITEM("propbox",NULL),
  GNOMEUIINFO_MENU_EXIT_ITEM("quit", NULL),
  GNOMEUIINFO_END
};

GnomeUIInfo helpmenu[] = {
  GNOMEUIINFO_HELP("obHello"),
  GNOMEUIINFO_MENU_ABOUT_ITEM("about", NULL),
  GNOMEUIINFO_END
};

GnomeUIInfo mainmenu[] = {
  GNOMEUIINFO_MENU_FILE_TREE(&filemenu),
  GNOMEUIINFO_MENU_HELP_TREE(&helpmenu),
  GNOMEUIINFO_END
};

@interface PropertyDialogPopup : Gnome_PropertyBox
- init;
- buttonClicked1:(id) myObj;
@end

@implementation PropertyDialogPopup
- init
{
	Gtk_Button *button;

	self = [super init];

	[self connect:"apply"];

	button = [[Gtk_Button alloc] initWithLabel:_("Click me!")];
	[button connectObj:"clicked" :self];

	[self append_page:[button show]
		:[[[Gtk_Label alloc] initWithLabelInfo:_("Page #1")] show]];

	button = [[Gtk_Button alloc] initWithLabel:_("Button Fun")];
	[button connectObj:"clicked" :self];

	[self append_page:[button show]
		:[[[Gtk_Label alloc] initWithLabelInfo:_("Page #2")] show]];

	button = [[[Gtk_Button alloc] initWithLabel:_("Button1")] show];
	[button connectObj:"clicked" :self];
	[button connectObjMethod:"clicked" :self :@selector(buttonClicked1:)];

	/* This button is special, it connects to its own callback and also
the generic clicked so it also affects the "apply" signal in the property
box. cool. useful? no :P -Michael */

	[self append_page:[button show]
		:[[[Gtk_Label alloc] initWithLabelInfo:_("Page #3")] show]];

	[self show];

	return self;
}

- apply:(id) myObj :(gint) n
{
/* This is right out of stock_demo.c -Michael */
        char s[256];

        if (n != -1) {
                sprintf(s, _("Applied changed on page #%d"), n + 1);
                gtk_widget_show(gnome_message_box_new(s, "info",
                                GNOME_STOCK_BUTTON_OK, NULL));
        }
	return self;
}

- clicked:(id) myObj
{
        [self changed];
        return self;
}

- buttonClicked1:(id) myObj
{
	printf(_("Button1\n"));
	return self;
}
@end




// Main window
@interface HelloWindow : Gnome_AppWin
{
id myapp;
}
- initForApp: (id) myApp;
- clicked: (id) myObj;
- buttonClicked1:(id) myObj;
- buttonClicked2:(id) myObj;
- buttonClicked3:(id) myObj;
- buttonClickedForAACanvas:(id) myObj;
- buttonClickedForCanvas:(id) myObj;

@end

@implementation HelloWindow
- initForApp: (id) myApp
{
	id vbox2, vbox, label, pix;
	Gtk_Button *button;
	Gtk_Frame *frame;

	myapp = myApp; /* FIXME: [need better explanation] pass 
				along main Object to new Object */

	self = [super initWithAppWinInfo:"obhello" :_("GNOME Obj-C Hello")];

	vbox = [[Gtk_VBox new] show];
	
	label = [[[Gtk_Label alloc] initWithLabelInfo:_("GNOME is cool. yup.")] show];
	button = [[[Gtk_Button alloc] initWithLabel:_("Font Selection Test")] show];
	[button connectObj:"clicked" :self];

	[vbox pack_start:label :TRUE :TRUE :5];
	[vbox pack_start:button :TRUE :TRUE :5];

	button = [[[Gnome_StockButton alloc] initStock:GNOME_STOCK_PIXMAP_PREFERENCES] show];
	[vbox pack_start:button :TRUE :TRUE :5];

	vbox2 = [[Gtk_HBox new] show];
	[vbox pack_start:vbox2 :TRUE :TRUE :5];

	pix = [[Gnome_Stock alloc] initWithImage:myapp :GNOME_STOCK_PIXMAP_PREFERENCES];
	[vbox2 pack_start:[pix show] :TRUE :TRUE :5];

	pix = [[Gnome_Stock alloc] initWithImage:myapp :GNOME_STOCK_PIXMAP_BOOK_GREEN];
	[vbox2 pack_start:[pix show] :TRUE :TRUE :5];

	pix = [[Gnome_Stock alloc] initWithImage:myapp :GNOME_STOCK_PIXMAP_MAIL];
	[vbox2 pack_start:[pix show] :TRUE :TRUE :5];

	frame = [[[Gtk_Frame alloc] initWithFrameInfo:_("connectObjMethod")] show];
	[vbox pack_start:frame :TRUE :TRUE :5];

	vbox2 = [[Gtk_HBox new] show];
	[frame add:vbox2];

	button = [[[Gtk_Button alloc] initWithLabel:_("Button1")] show];
	[button connectObjMethod:"clicked" :self :@selector(buttonClicked1:)];
	[vbox2 pack_start:button :TRUE :TRUE :5];

	button = [[[Gtk_Button alloc] initWithLabel:_("Button2")] show];
	[button connectObjMethod:"clicked" :self :@selector(buttonClicked2:)];
	[vbox2 pack_start:button :TRUE :TRUE :5];

	button = [[[Gtk_Button alloc] initWithLabel:_("Button3")] show];
	[button connectObjMethod:"clicked" :self :@selector(buttonClicked3:)];
	[vbox2 pack_start:button :TRUE :TRUE :5];

	vbox2 = [[Gtk_HBox new] show];
	[vbox pack_start:vbox2 :TRUE :TRUE :5];
	button = [[[Gtk_Button alloc] initWithLabel:_("Show a Great Canvas")] show];
	[button connectObjMethod:"clicked" :self :@selector(buttonClickedForCanvas:)];
	[vbox2 pack_start:button :TRUE :TRUE :5];

	button = [[[Gtk_Button alloc] initWithLabel:_("Show a Great AntiAliased Canvas")] show];
	[button connectObjMethod:"clicked" :self :@selector(buttonClickedForAACanvas:)];
	[vbox2 pack_start:button :TRUE :TRUE :5];

	[self create_menus:mainmenu];
	[self set_contents:vbox];
	[self connect:"delete_event"];

	return self;
}

- buttonClicked1:(id) myObj
{
	printf(_("Button1\n"));
	return self;
}

- buttonClicked2:(id) myObj
{
	printf(_("Button2\n"));
	return self;
}

- buttonClicked3:(id) myObj
{
	printf(_("Button3\n"));
	return self;
}

- buttonClickedForAACanvas:(id) myObj
{
  Gnome_Hello_Canvas_Window *canvasWindow;
  
  
  canvasWindow = [ [Gnome_Hello_Canvas_Window alloc] initWithAA:1 ];
  [canvasWindow show];
  
  return self;
}

- buttonClickedForCanvas:(id) myObj
{
  Gnome_Hello_Canvas_Window *canvasWindow;
  
  
  canvasWindow = [ [Gnome_Hello_Canvas_Window alloc] initWithAA:0 ];
  [canvasWindow show];
  
  return self;
}

- clicked:(id) myObj
{
	id hi;
	char *woop;

	hi = [Gnome_Font_Selector alloc];

	woop = [hi initModalWithReturn];

	printf(_("font: %s\n"), woop);

	free(woop);

	return self;
}

#define VERSION "0.0.2"

- about:(id) myObj
{
	char *author[] = {"Michael Hanni <mhanni@sprintmail.com>", "Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>", NULL};

	[[[Gnome_About alloc] initWithAboutInfo:_("obHello") 
		version:VERSION 
		copyright:_("Copyright (C) 1998 the Free Software Foundation") 
		authors:author
		comments:_("The Civilized GNOME Objective-C Hello...") 
		logo:NULL] show];

	return self;
}

- quit:(id) myObj
{
	[myapp quit];
	return self;
}

- propbox:(id) myObj
{
	[PropertyDialogPopup new];
	return self;
}

- delete_event:(id) myObj :(GdkEventAny *) event
{
	return [self quit:myObj];
}
@end

@interface HelloObject : Gnome_App
{
HelloObject *window;
}
-initHello:(char *)app_id :(int) argc :(char **) argv;
@end

@implementation HelloObject
-initHello:(char *)app_id
	:(int) argc
	:(char **) argv
{
	self = [super initApp:app_id :VERSION :argc :argv];

	window = [[[HelloWindow alloc] initForApp:self] show];

	return self;
}
@end

gint
main(int argc, char *argv[])
{

	id myapp;

	bindtextdomain(PACKAGE, GNOMELOCALEDIR);
	textdomain(PACKAGE);

	myapp = [HelloObject alloc];
	[myapp initHello:"obHello" :argc :argv];
	[myapp run];
	[myapp free];

	return 0;

}
