#include <gnome.h>
#include "obgtk/obgtk.h"

@interface Gnome_Stock : Gtk_VBox
{
@public
GnomeStock *gnomestock;
}
- initWithImage:(Gtk_Widget *) window
	       :(gchar *) icon;
- castGnomeStock:(GnomeStock *) castitem;
- set_icon:(gchar *) icon;
- stock_button:(gchar *) type;
- stock_or_ordinary_button:(gchar *) type;
@end
