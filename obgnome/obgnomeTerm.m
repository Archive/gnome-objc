#include "obgnomeTerm.h"

@implementation Gtk_Term
- castGtkTerm:(GtkTerm *) castitem
{
  gtkterm = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

- setup:(guint) width
       :(guint) height
       :(guint) max_width
       :(guint) scrollback
{
  gtk_term_construct(gtkterm, width, height, max_width, scrollback);
  return self;
}

- set_scroll_offset:(gint) offset
{
  gtk_term_set_scroll_offset(gtkterm, offset);
  return self;
}

- set_fonts:(GdkFont *) font_normal
	   :(GdkFont *) font_dim
	   :(GdkFont *) font_bold
	   :(gboolean) overstrike_bold
	   :(GdkFont *) font_underline
	   :(gboolean) draw_underline
	   :(GdkFont *) font_reverse
	   :(gboolean) colors_reversed
{
#if 0
  gtk_term_set_fonts(gtkterm, font_normal, font_dim, font_bold,
		     overstrike_bold, font_underline,
		     draw_underline, font_reverse, colors_reversed);
#endif
  return self;
}

- block_refresh
{
#if 0
  gtk_term_block_refresh(gtkterm);
#endif
  return self;
}

- unblock_refresh
{
#if 0
  gtk_term_unblock_refresh(gtkterm);
#endif
  return self;
}

- set_color:(guint) index
	   :(gulong) back
	   :(gulong) fore
	   :(gulong) fore_dim
	   :(gulong) fore_bold
{
  gtk_term_set_color(gtkterm, index, back, fore, fore_dim,
		     fore_bold);
  return self;
}

- select_color:(guint) fore_index
	      :(guint) back_index
{
  gtk_term_select_color(gtkterm, fore_index, back_index);
  return self;
}

- set_dim:(gboolean) dim
{
  gtk_term_set_dim(gtkterm, dim);
  return self;
}

- set_bold:(gboolean) bold
{
  gtk_term_set_bold(gtkterm, bold);
  return self;
}

- set_underline:(gboolean) underline
{
  gtk_term_set_underline(gtkterm, underline);
  return self;
}

- set_reverse:(gboolean) reverse
{
  gtk_term_set_reverse(gtkterm, reverse);
  return self;
}

- invert
{
  gtk_term_invert(gtkterm);
  return self;
}

- insert_lines:(guint) n
{
  gtk_term_insert_lines(gtkterm, n);
  return self;
}

- delete_lines:(guint) n
{
  gtk_term_delete_lines(gtkterm, n);
  return self;
}

- scroll:(guint) n
	:(gboolean) downwards
{
  gtk_term_scroll(gtkterm, n, downwards);
  return self;
}

- clear_line:(gboolean) before_cursor
	    :(gboolean) after_cursor
{
  gtk_term_clear_line(gtkterm, before_cursor, after_cursor);
  return self;
}

- insert_chars:(guint) n
{
  gtk_term_insert_chars(gtkterm, n);
  return self;
}

- delete_chars:(guint) n
{
  gtk_term_delete_chars(gtkterm, n);
  return self;
}

- bell
{
  gtk_term_bell(gtkterm);
  return self;
}

- clear_tty:(gboolean) before_cursor
	   :(gboolean) after_cursor
{
  gtk_term_clear(gtkterm, before_cursor, after_cursor);
  return self;
}

- set_cursor:(guint) x :(guint) y
{
  gtk_term_set_cursor(gtkterm, x, y);
  return self;
}

- get_cursor:(guint *) x :(guint *) y
{
  gtk_term_get_cursor(gtkterm, x, y);
  return self;
}

- set_cursor_mode:(GtkCursorMode) mode
		 :(gboolean) blinking
{
  gtk_term_set_cursor_mode(gtkterm, mode, blinking);
  return self;
}

- save_cursor
{
  gtk_term_save_cursor(gtkterm);
  return self;
}

- restore_cursor
{
  gtk_term_restore_cursor(gtkterm);
  return self;
}

- set_scroll_reg:(guint) top
		:(guint) bottom
{
  gtk_term_set_scroll_reg(gtkterm, top, bottom);
  return self;
}

- reset
{
  gtk_term_reset(gtkterm);
  return self;
}

- get_size:(guint *) width :(guint *) height
{
  gtk_term_get_size(gtkterm, width, height);
  return self;
}

- erase_chars:(guint) n
{
  gtk_term_erase_chars(gtkterm, n);
  return self;
}

- putc:(guchar) ch :(gboolean) insert
{
  gtk_term_putc(gtkterm, ch, insert);
  return self;
}
@end
