/* obgnomeColorpicker: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef OBGNOME_COLOR_PICKER_H
#define OBGNOME_COLOR_PICKER_H 1

#include "obgnome/obgnome.h"

@interface Gnome_Colorpicker : Gtk_Button
{
  GnomeColorPicker *gnomecolorpicker;
}

- init;
- castGnomeColorPicker:(GnomeColorPicker *) castitem;
- setDr:(gdouble) r
      g:(gdouble) g 
      b:(gdouble) b
      a:(gdouble) a;
- getDr:(gdouble *) r
      g:(gdouble *) g 
      b:(gdouble *) b
      a:(gdouble *) a;
- setI8r:(guint8) r
       g:(guint8) g 
       b:(guint8) b
       a:(guint8) a;
- getI8r:(guint8 *) r
       g:(guint8 *) g 
       b:(guint8 *) b
       a:(guint8 *) a;
- setI16r:(gushort) r
	g:(gushort) g 
	b:(gushort) b
	a:(gushort) a;
- getI16r:(gushort *) r
	g:(gushort *) g 
	b:(gushort *) b
	a:(gushort *) a;
- setDither:(gboolean) dither;
- setUseAlpha:(gboolean) use_alpha;
- setTittle:(const char *) title;

@end



#endif /* OBGNOME_COLOR_PICKER_H */
