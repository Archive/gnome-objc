/* obgnomeAppBar: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef OBGNOME_APP_BAR_H
#define OBGNOME_APP_BAR_H 1

#include "obgnome/obgnome.h"

@interface Gnome_AppBar : Gtk_HBox
{
  GnomeAppBar *gnomeappbar;
}

- initWithProgress:(gboolean) progress
	    status:(gboolean) status
     interactivity:(GnomePreferencesType) interactivity;

- castGnomeAppBar:(GnomeAppBar *) castitem;
- setStatus:(const gchar *) status;
- setDefault:(const gchar *) deflt;
- push:(const gchar *) status;
- pop;
- clearStack;
- setProgress:(gfloat) percentage;
- (Gtk_Progress *)getProgress;
- refresh;
- setPrompt:(const gchar *) prompt
      modal:(gboolean) modal;
- clearPrompt;
- (gchar *)getResponse;


@end



#endif /* OBGNOME_APP_BAR_H */
