#ifndef OBGNOME_PROPERTYBOX_H
#define OBGNOME_PROPERTYBOX_H 1
#include <obgnome/obgnomeDialog.h>
#include <libgnomeui/gnome-propertybox.h>

@interface Gnome_PropertyBox : Gnome_Dialog
{
  @public
     GnomePropertyBox *gnomepropertybox;
}
- castGnomePropertyBox:(GnomePropertyBox *) castitem;
- changed;
- append_page:(Gtk_Widget *) child
	     :(Gtk_Widget *) tab_label;
@end

#endif /* OBGNOME_PROPERTYBOX_H */
