/* obgnome-hello-canvas.h: Copyright (C) 1998 Free Software Foundation
 *
 * gnome-objc "hello world" program : canvas demo main window
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#ifndef OBGNOME_HELLO_CANVAS_H
#define OBGNOME_HELLO_CANVAS_H 1

#include <config.h>
#include <gnome.h>
#include "obgtk/obgtk.h"
#include "obgnome.h"

@interface Koch_Curve : Gnome_Canvas_Line

- initWithGroup: (Gnome_Canvas_Group *)group;

- createKochPoints : (int) nbLevel
       currentLevel: (int) level
		 x1: (double) x1
		 x2: (double) x2
		 y1: (double) y1
		 y2: (double) y2
	    points : (double **) points
  ;

- (GnomeCanvasPoints *)CreateKochCurve : (int) nbLevel;

@end


@interface Gnome_Hello_Canvas_Window : Gtk_Window
{
  Koch_Curve *kochcurve;
  Gnome_Canvas *canvas;
  
}
-initWithAA: (guint)aa;
- createCanvas;


@end



#endif /* OBGNOME_HELLO_CANVAS_H */
