/* obgnomeFontSelector.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeFontSelector.h"

@implementation Gnome_Font_Selector
- castGnomeFontSelector:(GnomeFontSelector *) castitem
{
  gnomefontsel = castitem;
  return [super castGtkDialog:GTK_DIALOG(castitem)];
}

- init
{
  return [self castGnomeFontSelector:GNOME_FONT_SELECTOR(gnome_font_selector_new())];
}

- (gchar *) get_selected
{
  return (gnome_font_selector_get_selected(gnomefontsel));
}

- (gchar *) initModalWithReturn
{
  return (gnome_font_select());
}
@end
