#ifndef OBGNOME_VTEMU_H
#define OBGNOME_VTEMU_H 1
#include "obgtk/obgtk.h"
#include "obgnomeTerm.h"
#include <gtktty/libgtktty.h>

@interface Gtk_VtEmu : Gtk_Term
{
@public
   GtkVtEmu *gtkvtemu;
}
- castGtkVtEmu:(GtkVtEmu *) castitem;
- initWithVtEmuInfo:(Gtk_Term *) term
		   :(gchar *) terminal_type;
- input:(const guchar *) buffer
       :(guint) count;
- report:(guchar *) buffer
	:(guint) count;
- set_reporter:(GtkVtEmuReporter *) callback
	      :(gpointer) user_data;
- reset:(gboolean) blank_screen;
@end

#endif
