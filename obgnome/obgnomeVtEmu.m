#include "obgnomeVtEmu.h"

@implementation Gtk_VtEmu
- castGtkVtEmu:(GtkVtEmu *) castitem
{
  gtkvtemu = castitem;
  return self;
}

- initWithVtEmuInfo:(Gtk_Term *) term
		   :(gchar *) terminal_type
{
  return [self castGtkVtEmu:(GtkVtEmu *)gtk_vtemu_new(term->gtkterm,
						      terminal_type)];
}

- input:(const guchar *) buffer
       :(guint) count
{
  gtk_vtemu_input(gtkvtemu, buffer, count);
  return self;
}

- report:(guchar *) buffer
	:(guint) count
{
  gtk_vtemu_report(gtkvtemu, buffer, count);
  return self;
}

- set_reporter:(GtkVtEmuReporter *) callback
	      :(gpointer) user_data
{
  gtk_vtemu_set_reporter(gtkvtemu, callback, user_data);
  return self;
}

- reset:(gboolean) blank_screen
{
  gtk_vtemu_reset(gtkvtemu, blank_screen);
  return self;
}

- free
{
  gtk_vtemu_destroy(gtkvtemu);
  return [super free];
}
@end
