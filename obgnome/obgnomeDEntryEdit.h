/* obgnomeDEntryEdit: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef OBGNOME_DENTRY_EDIT_H
#define OBGNOME_DENTRY_EDIT_H 1

#include "obgnome/obgnome.h"

@interface Gnome_DEntryEdit : Gtk_Object
{
  GnomeDEntryEdit *gnomedentryedit;
}
+ newNotebook:(Gtk_Notebook *) notebook;

- init;
- castGnomeDEntryEdit:(GnomeDEntryEdit *) castitem;
- clear;
- loadFile:(const gchar *) path;
- setDEntry:(GnomeDesktopEntry *) dentry;
- (GnomeDesktopEntry *) getDEntry;
- (gchar *) getIcon;
- (gchar *) getName;

@end



#endif /* OBGNOME_DENTRY_EDIT_H */
