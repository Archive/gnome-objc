#include "obgnomeStock.h"

@implementation Gnome_Stock
- initWithImage:(Gtk_Widget *) window
	       :(gchar *) icon
{
  return [self castGnomeStock:GNOME_STOCK(gnome_stock_pixmap_widget_new(window->gtkwidget, icon))];
}

- castGnomeStock:(GnomeStock *) castitem
{
  gnomestock = castitem;
  return [super castGtkWidget:GTK_WIDGET(castitem)];
}

- set_icon:(gchar *) icon
{
  gnome_stock_pixmap_widget_set_icon(gnomestock, icon);
  return self;
}

- stock_button:(gchar *) type
{
  return [super castGtkWidget:GTK_WIDGET(gnome_stock_button(type))];
}

/* I have no idea why you'd go through the trouble of writting out the next
   method, as there are shorter methods to do both types of creation, but
   hey! You want it? You got it. -Michael */

- stock_or_ordinary_button:(gchar *) type
{
  return [super castGtkWidget:GTK_WIDGET(gnome_stock_or_ordinary_button(type))];
}

/* stock_menu_items, are they needed? */
@end
