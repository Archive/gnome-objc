/* obgnomeAnimator.h: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#ifndef OBGNOME_ANIMATOR_H
#define OBGNOME_ANIMATOR_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"
#include <obgtk/obgtkObject.h>
#include <obgtk/obgtkWidget.h>
#include "obgnome/obgnomePixmap.h"

#include <libgnomeui/gnome-animator.h>
#include <gdk_imlib.h>

// ****************************************************

@interface Gnome_Animator : Gtk_Widget
{
@public
  GnomeAnimator *gnomeanimator;
}
- castGnomeAnimator:(GnomeAnimator *) castitem;
- initWithWidth: (guint)w
	 height: (guint)h;
- setLoopType: (GnomeAnimatorLoopType) lt;
- (GnomeAnimatorLoopType) getLoopType;
- setPlaybackDirection: (int)d;
- (int)getPlaybackDirection;
- (gboolean)appendFrameFromImlib: (GdkImlibImage *)im
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv;
- (gboolean)appendFrameFromImlib: (GdkImlibImage *)im
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv
			   width: (guint)w
			  height: (guint)h;
- (gboolean)appendFrameFromFile: (const gchar *)n
			xOffest: (int)xofs
			yOffset: (int)yofs
		       interval: (guint32)iv;
- (gboolean)appendFrameFromFile: (const gchar *)n
			xOffest: (int)xofs
			yOffset: (int)yofs
		       interval: (guint32)iv
			  width: (guint)w
			 height: (guint)h;

- (gboolean)appendFramesFromImlib: (GdkImlibImage *)im
			  xOffest: (int)xofs
			  yOffset: (int)yofs
			 interval: (guint32)iv
			    xUnit: (guint)xu;
- (gboolean)appendFramesFromImlib: (GdkImlibImage *)im
			  xOffest: (int)xofs
			  yOffset: (int)yofs
			 interval: (guint32)iv
			   xUnit: (guint)xu
			    width: (guint)w
			   height: (guint)h;
- (gboolean)appendFramesFromFile: (const gchar *)n
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv
			   xUnit: (guint)xu;
- (gboolean)appendFramesFromFile: (const gchar *)n
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv
			   xUnit: (guint)xu
			   width: (guint)w
			  height: (guint)h;
- (gboolean)appendFrameFromGnomePixmap: (Gnome_Pixmap *) gp
			       xOffest: (int)xofs
			       yOffset: (int)yofs
			      interval: (guint32)iv;

- start;
- stop;
- advance: (gint) num_frame;
- goToFrame: (gint) num_frame;
- (guint)getCurrentFrameNumber;
- (GnomeAnimatorStatus) getStatus;
- setPlaybackSpeed: (gdouble)s;
- (gdouble)getPlaybackSpeed;

@end


#endif /* OBGNOME_ANIMATOR_H */
