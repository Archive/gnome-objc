#include "obgnomeFileEntry.h"

@implementation Gnome_FileEntry
- initWithFileEntryInfo:(char *) history_id :(char *) browse_dialog_title
{
  return [self castGnomeFileEntry:GNOME_FILE_ENTRY(gnome_file_entry_new(history_id, browse_dialog_title))];
}

- castGnomeFileEntry:(GnomeFileEntry *) castitem
{
  gnomefileentry = castitem;
  return [super castGtkHBox:GTK_HBOX(castitem)];
}

- gnome_entry
{
  gnome_file_entry_gnome_entry(gnomefileentry);
  return self;
}

- gtk_entry
{
  gnome_file_entry_gtk_entry(gnomefileentry);
  return self;
}

- (void) set_title:(char *) browse_dialog_title
{
  gnome_file_entry_set_title (gnomefileentry, browse_dialog_title);
}
@end  
