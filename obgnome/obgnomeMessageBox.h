/* obgnomeMessageBox.h: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef OBGNOME_MESSAGE_BOX_H
#define OBGNOME_MESSAGE_BOX_H 1

#include <gnome.h>
#include <obgnome/obgnomeDialog.h>

@interface Gnome_Message_Box : Gnome_Dialog
{
@public
GnomeMessageBox *gnomemessagebox;
}
/* Same as in obgnomeDialog.h */
/* - initWithMessageBoxInfo:(const gchar *) message
                        :(const gchar *) messagebox_type, ...; */
- castGnomeMessageBox:(GnomeMessageBox *) castitem;
@end

#endif /* OBGNOME_MESSAGE_BOX_H */

