#!/bin/sh
grep Class ../../gnome-libs/libgnomeui/*.h | \
	grep typedef | grep -v gpointer | awk '{print $4}'| \
	sed -e 's,Class;,,' > /tmp/.$$
for I in `cat /tmp/.$$ | sed -e 's,^Gnome,,'`; do
	[ ! -f obgnome$I.h ] && echo "Gnome$I object needs an obgnome wrapper"
done
rm -f /tmp/.$$
