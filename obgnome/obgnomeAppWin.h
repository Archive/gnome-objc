#ifndef OBGNOME_APP_WIN_H
#define OBGNOME_APP_WIN_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"

@interface Gnome_AppWin : Gtk_Window
{
@public
  GnomeApp *gnomeapp;
  /* These should be set or created using the appropriate messages,
     not by just simply assigning to them */
  Gtk_Toolbar *toolbar;
  Gtk_MenuBar *menubar;
  Gtk_Widget *contents, *statusbar;
}
- initWithAppWinInfo:(char *)name :(char*)title;
- castGnomeApp:(GnomeApp *) castitem;
/* In this case, the meaning of the moreinfo field for
   regular menu items is the name of the message to send
   to 'self' (make sure that it takes the correct # of params
   for the 'activate' signal from Gtk) */
- create_menus:(GnomeUIInfo *) menuinfo;
/* In this case, the meaning of the moreinfo field for
   regular menu items is the name of the message to send
   to 'self' (make sure that it takes the correct # of params
   for the 'clicked' signal from Gtk) */
- create_toolbar:(GnomeUIInfo *) tbinfo;
- set_menus:(id) setmenubar;
- set_toolbar:(id) settoolbar;
- set_contents:(id) setcontents;
- set_statusbar:(id) setstatusbar;
@end

#endif /* OBGNOME_APP_WIN_H */
