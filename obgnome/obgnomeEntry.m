/* obgnomeEntry.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeEntry.h"

@implementation Gnome_Entry
- initGnomeEntry:(gchar *) history_id
{
  return [self castGnomeEntry:GNOME_ENTRY(gnome_entry_new(history_id))];
}

- castGnomeEntry:(GnomeEntry *) castitem
{
  gnomeentry = castitem;
  return [super castGtkCombo:GTK_COMBO(castitem)];
}

- gtk_entry
{
  return [self castGnomeEntry:GNOME_ENTRY(gnome_entry_gtk_entry(gnomeentry))];
}

- set_history_id:(gchar *) history_id
{
  gnome_entry_set_history_id(gnomeentry, history_id);
  return self;
}

- prepend_history:(gint) save
		 :(gchar *) text
{
  gnome_entry_prepend_history(gnomeentry, save, text);
  return self;
}

- append_history:(gint) save
		:(gchar *) text
{
  gnome_entry_append_history(gnomeentry, save, text);
  return self;
}

- load_history
{
  gnome_entry_load_history(gnomeentry);
  return self;
}

- save_history
{
  gnome_entry_save_history(gnomeentry);
  return self;
}
@end
