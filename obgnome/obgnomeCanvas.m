/* obgnomeCanvas.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * gnome-canvas has been written by Frederico Mena <federico@nuclecu.unam.mx>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <stdarg.h>

#include <obgnomeCanvas.h>

@implementation Gnome_Canvas_Item



- castGnomeCanvasItem:(GnomeCanvasItem *) castitem
{
  gnomecanvasitem = castitem;
  return [super castGtkObject:GTK_OBJECT(castitem)];
}

// ** the next two methods must be overloaded in the 
// misc derived canvas item classes.
- castToGnomeCanvasItem: (id) castitem
{
  g_error("castToGnomeCanvasItem method can not be used on the abstract class  Gnome_Canvas_Item\n");
  return self;
}

- (GtkType)getItemType
{
  g_error("The getItemType method can not be called on non subclassed Gnome_Canvas_Item\n");
  return (GtkType)NULL;
}


- initWithType: (GtkType) type
{
  // FIXME : there should be exception handling here
  // see gnome-canvas.c : gnome_canvas_item_new
  GnomeCanvasItem *theitem;
  theitem = GNOME_CANVAS_ITEM (gtk_type_new (type));

  return [self castToGnomeCanvasItem:(id)theitem ];
}

- initWithGroup: ( Gnome_Canvas_Group *) group 
  : (const gchar *) first_arg_name, ... 
{
  va_list args;
  GtkType type;
  
  // the getItemType is overloaded in the derived classes
  // This how we know which item to create.
  type = [self getItemType];
  // create the item and cast.
  [self initWithType:type];

  // then construct it, ie set its propoerties.
  va_start(args, first_arg_name);
  
  gnome_canvas_item_construct (gnomecanvasitem, group->gnomecanvasgroup , first_arg_name, args);
    
  va_end (args);
  return self ;
}


- initWithGroup: ( Gnome_Canvas_Group *) group 
	  nArgs: (guint) n
	   Args: (GtkArg *) args
{
  GnomeCanvasItem *theitem;
  GtkType type;
  

  // get the type of the item and create it
  type = [self getItemType];
  theitem = GNOME_CANVAS_ITEM( gnome_canvas_item_newv (group->gnomecanvasgroup, type, n, args) );

  return [self castToGnomeCanvasItem:(id)theitem ];
}



- set: (const gchar *) first_arg_name, ...
{
  
  va_list args;
  
  va_start(args, first_arg_name);
  gnome_canvas_item_set_valist (gnomecanvasitem, first_arg_name, args);
  va_end (args);
  return self;
}

- set: (guint) n
 Args: (GtkArg *) args
{
  gnome_canvas_item_setv (gnomecanvasitem, n, args);
  return self;
}

- move: (double) dx
    dy: (double) dy
{
  gnome_canvas_item_move (gnomecanvasitem, dx, dy);
  return self;
}

- raise : (int) positions
{
  gnome_canvas_item_raise (gnomecanvasitem, positions );
  return self;
}

- lower : (int) positions
{
  gnome_canvas_item_lower (gnomecanvasitem, positions );
  return self;
}

- raiseToTop
{
  gnome_canvas_item_raise_to_top (gnomecanvasitem);
  return self;
}

- lowerToBottom
{
  gnome_canvas_item_lower_to_bottom (gnomecanvasitem);
  return self;
}

- (int) grab   : (unsigned int) event_mask
	cursor : (GdkCursor *) cursor
	 etime : (guint32) etime
{
  return gnome_canvas_item_grab (gnomecanvasitem, event_mask, cursor, etime);
}

- ungrab : (guint32) etime
{
  gnome_canvas_item_ungrab (gnomecanvasitem, etime);
  return self;
}

- w2i : (double *) x
    y : (double *) y
{
  gnome_canvas_item_w2i (gnomecanvasitem, x, y);
  return self;
}

- i2w : (double *) x
    y : (double *) y
{
  gnome_canvas_item_i2w (gnomecanvasitem, x, y);
  return self;
}

// return the parent of the item.
// this is a specific obgnome method to
// avoid accessing directy the underlying C item structure
- (Gnome_Canvas_Item *)getParent
{
  Gnome_Canvas_Item *objcParent;
  GnomeCanvasItem *parent;
  
  parent = gnomecanvasitem->parent;
  
  objcParent = gtk_object_get_data_by_id( GTK_OBJECT(parent), OBJC_ID() );
  if (!objcParent) 
    {
      objcParent = [[Gnome_Canvas_Item alloc] castGnomeCanvasItem:parent];
    }
  return objcParent;
}


@end

// ****************************************************

@implementation Gnome_Canvas_Group 


- castGnomeCanvasGroup:(GnomeCanvasGroup *) castitem
{
  gnomecanvasgroup = castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}

- (GtkType)getItemType
{
  return (GtkType) gnome_canvas_group_get_type ();
}


- initWithRootGroup: (Gnome_Canvas_Group *) rootGroup
{
  
  GnomeCanvasGroup *newGroup;
  newGroup = GNOME_CANVAS_GROUP (gnome_canvas_item_new (rootGroup->gnomecanvasgroup,
						     gnome_canvas_group_get_type (),
						     "x", 0.0,
						     "y", 0.0,
						     NULL));
  
  return [self castGnomeCanvasGroup:newGroup ];
}





- initWithRootCanvas : (Gnome_Canvas *) rootCanvas
{
  //GnomeCanvasGroup *rootItem;
  GnomeCanvasGroup *rootGroup, *group;


  rootGroup = gnome_canvas_root ( rootCanvas->gnomecanvas );
  //rootGroup = GNOME_CANVAS_GROUP( rootItem );  

  
  group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (rootGroup,
						     gnome_canvas_group_get_type (),
						     "x", 0.0,
						     "y", 0.0,
						     NULL));
  
  
  return [self castGnomeCanvasGroup:group ];
}


@end



// ****************************************************

@implementation Gnome_Canvas

- castGnomeCanvas:(GnomeCanvas *) castitem
{
  gnomecanvas = castitem;
  return [super castGtkLayout:GTK_LAYOUT(castitem)];
}

- init
{
  return [self castGnomeCanvas:GNOME_CANVAS(gnome_canvas_new())];
}

// FIXME : This is buggy. Root canvas is not created by objc routines, so 
// there is no objc object associated with it. We should create on, but
// the user ought to know it hqs to dstroy it. This is a problem much with 
// this objc binding more than with the canvas. 
// One way to solve that is to create the root object when the objc canvas 
// object is created. with init. 
- (Gnome_Canvas_Item *)root
{
  return (Gnome_Canvas_Item *)
    gtk_object_get_data_by_id(GTK_OBJECT( gnome_canvas_root(gnomecanvas) ),
 			      OBJC_ID());
    
}

- setScrollRegion: (double) x1
		 y1: (double) y1
		 x2: (double) x2
		 y2: (double) y2
{
  gnome_canvas_set_scroll_region (gnomecanvas, x1, y1, x2, y2);
  return self;
}

- getScrollRegion: (double *) x1
		 y1: (double *) y1
		 x2: (double *) x2
		 y2: (double *) y2
{
  gnome_canvas_get_scroll_region (gnomecanvas, x1, y1, x2, y2);
  return self;
}

- setPixelsPerUnit: (double) n
{
  gnome_canvas_set_pixels_per_unit (gnomecanvas, n);
  return self;
}

- scrollTo: (int) cx
	cy: (int) cy
{
  gnome_canvas_scroll_to (gnomecanvas, cx, cy);
  return self;
}

- getScrollOffsets: (int*) cx
		cy: (int*) cy
{
  gnome_canvas_get_scroll_offsets (gnomecanvas, cx, cy);
  return self;
}

- updateNow
{
  gnome_canvas_update_now (gnomecanvas);
  return self;
}

- requestRedraw: (int) x1
	     y1: (int) y1
	     x2: (int) x2
	     y2: (int) y2
{
  gnome_canvas_request_redraw (gnomecanvas, x1, y1, x2, y2);
  return self; 
}

- w2c: (double) wx
   wy: (double) wy
   cx: (int *) cx
   cy: (int *) cy
{
  gnome_canvas_w2c (gnomecanvas, wx, wy, cx, cy);
  return self; 
}

- c2w: (int ) cx
   cy: (int ) cy
   wx: (double *) wx
   wy: (double *) wy
{
  gnome_canvas_c2w (gnomecanvas, cx, cy, wx, wy);
  return self; 
}

- (int)getColor: (char *)spec
     color: (GdkColor *)color
{
  return  gnome_canvas_get_color (gnomecanvas, spec, color);
}

@end
























