#include "obgnomeLED.h"

@implementation Gtk_LED
- init
{
  return [self castGtkLED:GTK_LED(gtk_led_new())];
}

- castGtkLED:(GtkLed *) castitem
{
  gtkled = castitem;
  return [super castGtkMisc:GTK_MISC(castitem)];
}

- set_state_LED:(GtkStateType) widget_state
	       :(gboolean) on_off
{
  gtk_led_set_state(gtkled, widget_state, on_off);
  return self;
}

- switch:(gboolean) on_off
{
  gtk_led_switch(gtkled, on_off);
  return self;
}

- (gboolean)is_on
{
  return gtk_led_is_on(gtkled);
}

@end
