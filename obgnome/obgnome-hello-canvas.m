/* obgnome-hello-canvas.m: Copyright (C) 1998 Free Software Foundation
 *
 * gnome-objc "hello world" program : canvas demo main window
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#include "obgnome-hello-canvas.h"

#include <math.h>




@implementation Koch_Curve


- initWithGroup: (Gnome_Canvas_Group *)group
{
  GnomeCanvasPoints *points;
 
  points = [self CreateKochCurve : 8];
  [self  initWithGroup: group  
	 : "points", points,
	 "fill_color", "black",
	 "width_pixels", 1,
	 NULL];


  [self connectObjMethod:"event" :self	:@selector(itemEvent:::)];
  return self;

}

- itemEvent:(id) anObj :(GdkEvent *) event  :(gpointer )data
{
  // completely taken from frederico gnome-canvas example
  static double x, y;
  double new_x, new_y;
  GdkCursor *fleur;
  static int dragging;
  
  switch (event->type) {
  case GDK_BUTTON_PRESS:
    switch (event->button.button) {
    case 1:
      if (event->button.state & GDK_SHIFT_MASK)
	[self destroy];
      else {
	x = event->button.x;
	y = event->button.y;
	
	fleur = gdk_cursor_new (GDK_FLEUR);
	
	[self grab: (GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK)
	      cursor:fleur
	      etime: event->button.time ];
	
	gdk_cursor_destroy (fleur);
	dragging = TRUE;
      }
      break;
      
    case 2:
      if (event->button.state & GDK_SHIFT_MASK)
	[self lowerToBottom];
      else
	[self lower:1];
      break;
      
    case 3:
      if (event->button.state & GDK_SHIFT_MASK)
	[self raiseToTop];
      else
	[self raise:1];
      break;
      
    default:
      break;
    }
    
  case GDK_MOTION_NOTIFY:
    if (dragging && (event->motion.state & GDK_BUTTON1_MASK)) {
      new_x = event->motion.x;
      new_y = event->motion.y;
      
      [self move: (new_x - x) dy: (new_y - y)];
      x = new_x;
      y = new_y;
    }
    break;
    
  case GDK_BUTTON_RELEASE:
    [self ungrab:event->button.time];
    dragging = FALSE;
    break;
    
  default:
    break;
  }

  return self;
}


- createKochPoints : (int) nbLevel
      currentLevel: (int) level
		x1: (double) x1
		x2: (double) x2
		y1: (double) y1
		y2: (double) y2
	   points : (double **) points
{
  double dx,dy;
  double px1,px2,py1,py2;
  
  *( (*points)++ ) = x1;
  *( (*points)++ ) = y1;
  if (level == nbLevel)
    return self;
  
  dx = (x2-x1);
  dy = (y2-y1);

  px1 = x1 + dx/3.0;
  py1 = y1 + dy/3.0;
    
  px2 = x1 + dx/2.0 - dy/3.0;
  py2 = y1 + dy/2.0 + dx/3.0;
  
  [self createKochPoints : nbLevel currentLevel: (level+1) x1:px1 x2:px2 y1:py1 y2:py2 points:points];
  
  px1 = x1 + 2.0*dx/3.0;
  py1 = y1 + 2.0*dy/3.0;
  
  [self createKochPoints : nbLevel currentLevel: (level+1) x1:px2 x2:px1 y1:py2 y2:py1 points:points];

  *( (*points)++ ) = px1;
  *( (*points)++ ) = py1;
  

  return self;

}
  

- (GnomeCanvasPoints *)CreateKochCurve : (int) nbLevel
{
  int nbPoints;
  GnomeCanvasPoints *points;
  double *coords;


  nbPoints = 3*( (int)floor(3*pow(2,nbLevel-1)) -2 )+1;
  points = gnome_canvas_points_new (nbPoints);
  coords = (points->coords);
  
  [self createKochPoints : nbLevel currentLevel:1 x1:50 x2:250 y1:250 y2:250 points:&coords];
  [self createKochPoints : nbLevel currentLevel:1 x1:250 x2:150 y1:250 y2:50 points:&coords];
  [self createKochPoints : nbLevel currentLevel:1 x1:150 x2:50 y1:50 y2:250 points:&coords];

  *(coords++)=50; *(coords++)=250;
 
  return points;
}
  


@end








// the toplevel canvas window
@implementation Gnome_Hello_Canvas_Window 


-initWithAA: (guint)aa
{
  id vbox, label;
  GdkColor gdk_color;
  GtkStyle *style;
  Gtk_Frame *frame;
  
  Gtk_Adjustment *adj;
  Gtk_SpinButton *spin;

  Gtk_Table *table;
  Gtk_HScrollbar *hScrollBar;
  Gtk_VScrollbar *vScrollBar;

  [super init];
  vbox = [[Gtk_VBox new] show];
  [vbox set_border_width:4];

  [self add:vbox];

  adj = [[Gtk_Adjustment alloc] initWithAdjustmentInfo:1.0   
				:0.05
				:100.00
				:0.05 
				:0.50
				:0.50 ];
			      
  spin = [[Gtk_SpinButton alloc] initWithSpinButtonInfo: adj
				 :0.0
				 :2 ];
 
  [vbox pack_start:spin :TRUE :TRUE :5];
  [spin show];
  
  [adj connectObjMethod:"value_changed" :self :@selector(zoom_changed:)];

  label = [[[Gtk_Label alloc] initWithLabelInfo:_("Click on the curve to move it")] show];
  [vbox pack_start:label :TRUE :TRUE :5];
 

  frame = [[[Gtk_Frame alloc] initWithFrameInfo:""] show];
  [frame set_shadow_type: GTK_SHADOW_IN];
  if (aa) {
    gtk_widget_push_visual (gdk_rgb_get_visual ());
    gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  } else {
    gtk_widget_push_visual (gdk_imlib_get_visual ());
    gtk_widget_push_colormap (gdk_imlib_get_colormap ());
  }
  canvas = [[Gnome_Canvas new] show];
  GNOME_CANVAS(canvas->gnomecanvas)->aa=aa;
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  // set up some scroll bars
  table = [[Gtk_Table alloc] initWithTableInfo:2 :2 :FALSE];
  [table show];
  [[table set_row_spacings:4] set_col_spacings:4];

  hScrollBar = [[Gtk_HScrollbar alloc] initWithHScrollbarInfo:[canvas get_hadjustment] ];
  vScrollBar = [[Gtk_VScrollbar alloc] initWithVScrollbarInfo:[canvas get_vadjustment]];
  [hScrollBar show];
  [vScrollBar show];
  [table attach:canvas 
	 :0
	 :1
	 :0
	 :1
	 :GTK_EXPAND | GTK_FILL | GTK_SHRINK
	 :GTK_EXPAND | GTK_FILL | GTK_SHRINK
	 :0
	 :0 ];

  [table attach:hScrollBar
	 :0
	 :1
	 :1
	 :2
	 :GTK_EXPAND | GTK_FILL | GTK_SHRINK
	 :GTK_FILL
	 :0
	 :0];

  [table attach:vScrollBar
	 :1
	 :2
	 :0
	 :1
	 :GTK_FILL
	 :GTK_EXPAND | GTK_FILL | GTK_SHRINK
	 :0
	 :0];

  // let's go.
  [frame add:table];
  [vbox pack_start:frame :TRUE :TRUE :5];


/* Set canvas background color */

  gdk_color.red = (guint16)(65535.0);
  gdk_color.green = (guint16)(65535.0);
  gdk_color.blue = (guint16)(65535.0);
      
  style = gtk_style_new ();
  style->bg[GTK_STATE_NORMAL] = gdk_color;


  [canvas set_style:style]; 

  [self createCanvas];
  
  
  return self;
}

    
- zoom_changed: (Gtk_Adjustment *)obj  
{
  [canvas setPixelsPerUnit:(obj->gtkadjustment)->value ];

  return self;
}

  
- createCanvas
{
	Gnome_Canvas_Group *group;

	[canvas set_usize:500 :400];
	[canvas setScrollRegion:0 y1:0 x2:400 y2:400]; 
	group = [[Gnome_Canvas_Group alloc] initWithRootCanvas: canvas];
	
	kochcurve = [ [Koch_Curve alloc] initWithGroup:group] ;

#ifdef OLD_STUFF
	[[ Gnome_Canvas_Rect alloc] initWithGroup: group
				    : "x1", 50.0,
				    "x2", 100.0,
				    "y1", 50.0,
				    "y2", 100.0,
				    "outline_color", "green",
				    "fill_color", NULL,
				    "width_units", 2.0,
				    NULL];

	[[ Gnome_Canvas_Ellipse alloc] initWithGroup: group
				    : "x1", 100.0,
				    "x2", 150.0,
				    "y1", 50.0,
				    "y2", 100.0,
				    "outline_color", "red",
				    "fill_color", "black",
				    "width_units", 2.0,
				    NULL];

	[[ Gnome_Canvas_Text alloc] initWithGroup: group
				    : "x", 150.0,
				    "y", 140.0,
				    "text", _("This is a simple Koch Curve"),
				    NULL];


#endif

	//[group setX:10];
	//
	//gnome_canvas_item_set( group->gnomecanvasgroup, "x", 10.0, NULL);
	//printf("root %p\n",[canvas root]);

	return self;
}


@end
