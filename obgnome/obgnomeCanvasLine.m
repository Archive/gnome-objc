#include "obgnomeCanvasLine.h"


@implementation Gnome_Canvas_Line : Gnome_Canvas_Item

- castGnomeCanvasLine: (GnomeCanvasLine *) castitem
{
  gnomecanvasline = castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}

- (GtkType)getItemType
{
  
  return (GtkType) gnome_canvas_line_get_type ();;
}

-  castToGnomeCanvasItem: (id) castitem
{
  gnomecanvasline = (GnomeCanvasLine *) castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}



@end
