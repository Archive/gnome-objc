#ifndef OBGNOME_ABOUT_H
#define OBGNOME_ABOUT_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"
#include <obgnome/obgnomeDialog.h>

@interface Gnome_About : Gnome_Dialog
{
  @public
     GnomeAbout *gnomeabout;
}
- initWithAboutInfo:(gchar *) title     /* Name of the application. */
	version:(gchar *) version   /* Version. */
      copyright:(gchar *)copyright  /* Copyright notice (one line.) */
	authors:(gchar **)authors   /* NULL terminated list of authors. */
       comments:(gchar *)comments   /* Other comments. */
	   logo:(gchar *)logo       /* A logo pixmap file. */
  ;
- castGnomeAbout:(GnomeAbout *) castitem;
@end

#endif /* OBGNOME_ABOUT_H */
