/* obgnomeCanvasRectEllipse.h: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#ifndef OBGNOME_CANVAS_RECT_ELLIPSE_H
#define OBGNOME_CANVAS_RECT_ELLIPSE_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"
#include <obgnome/obgnomeCanvas.h>
#include <libgnomeui/gnome-canvas-rect-ellipse.h>


// this is the class from which derives 
// the ellipse and rectangle items
@interface Gnome_Canvas_RE : Gnome_Canvas_Item
{
@public
  GnomeCanvasRE *gnomecanvasre;
}
- castGnomeCanvasRE: (GnomeCanvasRE *) castitem;

@end


// RECTANGLE
@interface Gnome_Canvas_Rect : Gnome_Canvas_RE
{
@public
  GnomeCanvasRect *gnomecanvasrect;
}
- castGnomeCanvasRect: (GnomeCanvasRect *) castitem;

@end


// ELLIPSE
@interface Gnome_Canvas_Ellipse : Gnome_Canvas_RE
{
@public
  GnomeCanvasEllipse *gnomecanvasellipse;
}
- castGnomeCanvasEllipse: (GnomeCanvasEllipse *) castitem;

@end

#endif /* OBGNOME_CANVAS_ELLIPSE_ELLIPSE_H */
