#include "obgnomeApp.h"

@implementation Gnome_App
- initApp:(const char *)app_id
	 :(const char *)app_version
	 :(int)argc
	 :(char **)argv
{
  self = [super init];
  gnome_init(app_id, app_version, argc, argv);

  return self;
}

- initAppWithPoptTable:(const char *)app_id
		      :(const char *)app_version
		      :(int)argc
		      :(char **)argv
		      :(struct poptOption *) options
		      :(int) poptFlags
		      :(poptContext *) return_ctx
{
  self = [super init];

  gnome_init_with_popt_table(app_id, app_version, argc, argv,
			     options, poptFlags, return_ctx);

  return self;
}
@end
