#include "obgnomeStockButton.h"

/* This may seem like overwrapping, but it *does* make the end coders job
a little easier and the code a little more elegant. However, Elliot, if
you like feel free to nuke this stuff. :-) -Michael */

@implementation Gnome_StockButton
- initStock:(gchar *) type
{
  return [super castGtkButton:GTK_BUTTON(gnome_stock_button(type))];
}

/* I have no idea why you'd go through the trouble of writting out the next
   method, as there are shorter methods to do both types of creation, but
   hey! You want it? You got it. -Michael */

- initStockOrNormal:(gchar *) type
{
  return [super castGtkButton:GTK_BUTTON(gnome_stock_or_ordinary_button(type))];
}
@end
