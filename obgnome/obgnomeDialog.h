#ifndef OBGNOME_DIALOG_H
#define OBGNOME_DIALOG_H 1
#include <obgtk/obgtkWindow.h>
#include <libgnomeui/gnome-dialog.h>

@interface Gnome_Dialog : Gtk_Window
{
  @public
     GnomeDialog *gnomedialog;
}
- castGnomeDialog:(GnomeDialog *) castitem;

/* Arguments: Title and button names, then NULL */
/* call it directly, then pass it to [[Gnome_Dialog alloc] castGnomeDialog:foo]
   - initWithDialogInfo:(const gchar *) title, ...; */

/* Connect to the "clicked" signal of a single button */
- button_connect:(gint) button 
		:(GtkSignalFunc) callback :(gpointer) data;

/* Set the default button. - it will have a little highlight, 
   (and maybe pressing return will activate it, eventually, not 
   coded yet.) */
- set_default:(gint) button;

/* Set sensitivity of a button */
- set_sensitive:(gint) button 
	       :(gboolean) setting;

/* Set the accelerator for a button. Note that there are two
   default accelerators: "Return" will be the same as clicking
   the default button, and "Escape" will emit delete_event. 
   (Note: neither of these is in the accelerator table,
   Return is a Gtk default and Escape comes from a key press event
   handler.) */
- set_accelerator:(gint) button 
		 :(const guchar) accelerator_key
		 :(guint8) accelerator_mode;

/* Hide and optionally destroy. Destroys by default, use close_hides() 
   to change this. */
- close;

/* Make _close just hide, not destroy. */
- close_hides:(gboolean) just_hide;

/* Whether to close after emitting clicked signal - default is
   FALSE. If clicking *any* button should close the dialog, set it to
   TRUE.  */
- set_close:(gboolean) click_closes;

/* Use of append_buttons is discouraged, it's really
   meant for subclasses. */
- append_buttons:(const gchar *) first , ...;

@end

#endif /* OBGNOME_DIALOG_H */
