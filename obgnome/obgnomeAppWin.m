#include "obgnomeAppWin.h"

static void
obgnome_do_signal_connection(GnomeUIInfo *info_item,
			     gchar *signal_name,
			     GnomeUIBuilderData *uidata)
{
  /* Copied almost verbatrim from obgtk/obgtkObject.m - connectObj */
  int i;
  GString *foo;
  GtkSignalQuery *siginfo;
  ObgtkRelayInfo mydata;

  siginfo = gtk_signal_query(gtk_signal_lookup(signal_name,
						GTK_OBJECT(info_item->widget)->klass->type));
  if(!siginfo)
    {
      g_warning("obgnome_do_signal_connection: Couldn't lookup signale %s\n", signal_name);
      return;
    }

  foo = g_string_new(info_item->moreinfo);

  mydata = g_new(struct _ObgtkRelayInfo, 1);
  mydata->object = uidata->data;
  g_string_append_c(foo, ':'); /* For signalled object parameter */
  for(i = 0; i < siginfo->nparams; i++)
    g_string_append_c(foo, ':'); /* For other gtk-specific params */
  mydata->method = sel_get_uid(foo->str);
  g_string_free(foo, TRUE);
  g_free(siginfo);
  gtk_signal_connect_full(GTK_OBJECT(info_item->widget),
			    signal_name, NULL,
			    uidata->relay_func,
			    mydata,
			    uidata->destroy_func,
			    FALSE, FALSE);
}

@implementation Gnome_AppWin
- initWithAppWinInfo:(char *) name
                    :(char *) title
{
  return [self castGnomeApp:GNOME_APP(gnome_app_new(name, title))];
}

- castGnomeApp:(GnomeApp *) castitem
{
  gnomeapp = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

/* Need to find a way to make these send ObjC messages */

- create_menus:(GnomeUIInfo *) menuinfo
{
  struct _GnomeUIBuilderData uibdata =
  {
    (GnomeUISignalConnectFunc)obgnome_do_signal_connection,
    self,
    TRUE,
    (GtkCallbackMarshal)obgtk_signal_relay,
    g_free
  };

  gnome_app_create_menus_custom(gnomeapp,
				menuinfo,
				&uibdata);
  return self;
}

- create_toolbar:(GnomeUIInfo *) tbinfo
{
  struct _GnomeUIBuilderData uibdata =
  {
    (GnomeUISignalConnectFunc)obgnome_do_signal_connection,
    self,
    TRUE,
    (GtkCallbackMarshal)obgtk_signal_relay,
    g_free
  };

  gnome_app_create_toolbar_custom(gnomeapp,
				  tbinfo,
				  &uibdata);
  return self;
}

- set_menus:(id) setmenubar
{
  menubar = setmenubar;
  gnome_app_set_menus(gnomeapp, ((Gtk_MenuBar *)menubar)->gtkmenubar);
  return self;
}

- set_toolbar:(id) settoolbar
{
  toolbar = settoolbar;
  gnome_app_set_toolbar(gnomeapp, ((Gtk_Toolbar *)toolbar)->gtktoolbar);
  return self;
}

- set_contents:(id) setcontents
{
  contents = setcontents;
  gnome_app_set_contents(gnomeapp, ((Gtk_Widget *)setcontents)->gtkwidget);
  return self;
}

- set_statusbar:(id) setstatusbar
{
  statusbar = setstatusbar;
  gnome_app_set_statusbar(gnomeapp, ((Gtk_Widget *)setstatusbar)->gtkwidget);
  return self;
}

@end
