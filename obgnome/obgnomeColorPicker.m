/* obgnomeColorpicker: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeColorPicker.h"

@implementation Gnome_Colorpicker

- init
{
  GnomeColorPicker *gcp;
  
  gcp = GNOME_COLOR_PICKER (gnome_color_picker_new ());
  return [self castGnomeColorPicker:gcp];
}

- castGnomeColorPicker:(GnomeColorPicker *) castitem
{
  gnomecolorpicker = castitem;
  return [super castGtkButton:GTK_BUTTON (castitem)];
}

- setDr:(gdouble) r
      g:(gdouble) g 
      b:(gdouble) b
      a:(gdouble) a
{
  gnome_color_picker_set_d (gnomecolorpicker, r, g, b, a);
  return self;
}

- getDr:(gdouble *) r
      g:(gdouble *) g 
      b:(gdouble *) b
      a:(gdouble *) a
{
  gnome_color_picker_get_d (gnomecolorpicker, r, g, b, a);
  return self;
}

- setI8r:(guint8) r
       g:(guint8) g 
       b:(guint8) b
       a:(guint8) a
{
  gnome_color_picker_set_i8 (gnomecolorpicker, r, g, b, a);
  return self;
}

- getI8r:(guint8 *) r
       g:(guint8 *) g 
       b:(guint8 *) b
       a:(guint8 *) a
{
  gnome_color_picker_get_i8 (gnomecolorpicker, r, g, b, a);
    return self;
}

- setI16r:(gushort) r
	g:(gushort) g 
	b:(gushort) b
	a:(gushort) a
{
  gnome_color_picker_set_i16 (gnomecolorpicker, r, g, b, a);
  return self;
}

- getI16r:(gushort *) r
	g:(gushort *) g 
	b:(gushort *) b
	a:(gushort *) a
{
  gnome_color_picker_get_i16 (gnomecolorpicker, r, g, b, a);
  return self;
}

- setDither:(gboolean) dither
{
  gnome_color_picker_set_dither (gnomecolorpicker, dither);
  return self;
}

- setUseAlpha:(gboolean) use_alpha
{
  gnome_color_picker_set_use_alpha  (gnomecolorpicker, use_alpha);
  return self;
}

- setTittle:(const char *) title
{
  gnome_color_picker_set_title (gnomecolorpicker, title);
  return self;
}

@end


