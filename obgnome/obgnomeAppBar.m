/* obgnomeAppBar: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeAppBar.h"

@implementation Gnome_AppBar

- initWithProgress:(gboolean) progress
	    status:(gboolean) status
     interactivity:(GnomePreferencesType) interactivity
{
  GnomeAppBar *ga;
  
  ga = GNOME_APPBAR (gnome_appbar_new(progress, status, interactivity) );
  return [self castGnomeAppBar:ga ];
}

- castGnomeAppBar: (GnomeAppBar *) castitem
{
  gnomeappbar = castitem;
  return [super castGtkHBox:(GtkHBox *) castitem];
}

- setStatus:(const gchar *) status
{
  gnome_appbar_set_status (gnomeappbar, status);
  return self;
}

- setDefault:(const gchar *) deflt
{
  gnome_appbar_set_default (gnomeappbar, deflt);
  return self;
}

- push:(const gchar *) status
{
  gnome_appbar_push (gnomeappbar, status);
  return self;
}

- pop
{
  gnome_appbar_pop (gnomeappbar);
  return self;
}

- clearStack
{
  gnome_appbar_clear_stack (gnomeappbar);
  return self;
}

- setProgress:(gfloat) percentage
{
  gnome_appbar_set_progress (gnomeappbar, percentage);
  return self;
}

- (Gtk_Progress *)getProgress
{
  GtkProgress *progress;
  Gtk_Progress *objcProgress;

  progress = gnome_appbar_get_progress (gnomeappbar);
  objcProgress = gtk_object_get_data_by_id( GTK_OBJECT(progress), OBJC_ID() );
  if (!objcProgress) 
    {
      objcProgress = [[Gtk_Progress alloc] castGtkProgress:progress];
    }
  return objcProgress;
}

- refresh
{
  gnome_appbar_refresh (gnomeappbar);
  return self;
}

- setPrompt:(const gchar *) prompt
      modal:(gboolean) modal
{
  gnome_appbar_set_prompt (gnomeappbar, prompt, modal);
  return self;
}

- clearPrompt
{
  gnome_appbar_clear_prompt (gnomeappbar);
  return self;
}

- (gchar *)getResponse
{
  return gnome_appbar_get_response (gnomeappbar);
}

@end

