#ifndef OBGNOME_TERM_H
#define OBGNOME_TERM_H 1
#include "obgtk/obgtk.h"
#include <gtktty/libgtktty.h>

@interface Gtk_Term : Gtk_Widget
{
@public
  GtkTerm *gtkterm;
}

- castGtkTerm:(GtkTerm *) castitem;
- setup:(guint) width
       :(guint) height
       :(guint) max_width
       :(guint) scrollback;
- set_scroll_offset:(gint) offset;
- set_fonts:(GdkFont *) font_normal
	   :(GdkFont *) font_dim
	   :(GdkFont *) font_bold
	   :(gboolean) overstrike_bold
	   :(GdkFont *) font_underline
	   :(gboolean) draw_underline
	   :(GdkFont *) font_reverse
	   :(gboolean) colors_reversed;
- block_refresh;
- unblock_refresh;
- set_color:(guint) index
	   :(gulong) back
	   :(gulong) fore
	   :(gulong) fore_dim
	   :(gulong) fore_bold;
- select_color:(guint) fore_index
	      :(guint) back_index;
- set_dim:(gboolean) dim;
- set_bold:(gboolean) bold;
- set_underline:(gboolean) underline;
- set_reverse:(gboolean) reverse;
- invert;
- insert_lines:(guint) n;
- delete_lines:(guint) n;
- scroll:(guint) n
	:(gboolean) downwards;
- clear_line:(gboolean) before_cursor
	    :(gboolean) after_cursor;
- insert_chars:(guint) n;
- delete_chars:(guint) n;
- bell;
- clear_tty:(gboolean) before_cursor
	   :(gboolean) after_cursor;
- set_cursor:(guint) x :(guint) y;
- get_cursor:(guint *) x :(guint *) y;
- set_cursor_mode:(GtkCursorMode) mode
		 :(gboolean) blinking;
- save_cursor;
- restore_cursor;
- set_scroll_reg:(guint) top
		:(guint) bottom;
- reset;
- get_size:(guint *) width :(guint *) height;
- erase_chars:(guint) n;
- putc:(guchar) ch :(gboolean) insert;
@end
#endif
