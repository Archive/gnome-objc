#ifndef OBGNOME_FILEENTRY_H
#define OBGNOME_FILEENTRY_H 1
#include <obgtk/obgtkHBox.h>
#include <libgnomeui/gnome-file-entry.h>

@interface Gnome_FileEntry : Gtk_HBox
{
  @public
     GnomeFileEntry *gnomefileentry;
}
- initWithFileEntryInfo:(char *) history_id :(char *) browse_dialog_title;
- castGnomeFileEntry:(GnomeFileEntry *) castitem;
- gnome_entry;
- gtk_entry;
- (void)set_title:(char *) browse_dialog_title;
@end

#endif /* OBGNOME_FILEENTRY_H */
