#include <gnome.h>
#include "obgtk/obgtk.h"

@interface Gnome_StockButton : Gtk_Button
- initStock:(gchar *) type;
- initStockOrNormal:(gchar *) type;
@end
