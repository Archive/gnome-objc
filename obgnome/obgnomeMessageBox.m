/* obgnomeMessageBox.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdarg.h>
#include "obgnomeMessageBox.h"

@implementation Gnome_Message_Box
#if 0
- initWithMessageBoxInfo:(const gchar *) message
			:(const gchar *) messagebox_type, ...
{
  va_list ap;
  va_start(ap, messagebox_type);

  return [self castGnomeMessageBox:GNOME_MESSAGE_BOX(gnome_message_box_new(message, messagebox_type, ap))];
}
#endif

- castGnomeMessageBox:(GnomeMessageBox *) castitem
{
  gnomemessagebox = castitem;
  return [super castGnomeDialog:GNOME_DIALOG(castitem)];
}
@end
