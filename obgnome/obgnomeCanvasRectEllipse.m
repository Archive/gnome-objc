#include "obgnomeCanvasRectEllipse.h"


@implementation Gnome_Canvas_RE : Gnome_Canvas_Item

- castGnomeCanvasRE: (GnomeCanvasRE *) castitem
{
  gnomecanvasre = castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}

- (GtkType)getItemType
{
  
  return (GtkType) gnome_canvas_re_get_type ();
}

-  castToGnomeCanvasItem:  (id) castitem
{
  gnomecanvasre = (GnomeCanvasRE *)castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}




///////////////////////////////////////////////

@implementation Gnome_Canvas_Rect : Gnome_Canvas_RE

- castGnomeCanvasRect: (GnomeCanvasRect *) castitem
{
  gnomecanvasrect = castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}

- (GtkType)getItemType
{
  
  return (GtkType) gnome_canvas_rect_get_type ();
}

-  castToGnomeCanvasItem:  (id) castitem
{
  gnomecanvasrect = (GnomeCanvasRect *)castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}


///////////////////////////////////////////////

@implementation Gnome_Canvas_Ellipse : Gnome_Canvas_RE

- castGnomeCanvasEllipse: (GnomeCanvasEllipse *) castitem
{
  gnomecanvasellipse = castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}

- (GtkType)getItemType
{
  
  return (GtkType) gnome_canvas_ellipse_get_type ();
}

-  castToGnomeCanvasItem:  (id) castitem
{
  gnomecanvasellipse = (GnomeCanvasEllipse *)castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}


@end
