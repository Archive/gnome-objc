#include "obgnomeCanvasWidget.h"


@implementation Gnome_Canvas_Widget : Gnome_Canvas_Item

- castGnomeCanvasWidget: (GnomeCanvasWidget *) castitem
{
  gnomecanvaswidget = castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}

- (GtkType)getItemType
{
  
  return (GtkType) gnome_canvas_widget_get_type ();;
}

-  castToGnomeCanvasItem: (id) castitem
{
  gnomecanvaswidget = (GnomeCanvasWidget *) castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}



@end
