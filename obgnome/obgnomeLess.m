/* obgnomeLess.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeLess.h"

@implementation Gnome_Less
- init
{
  return [self castGnomeLess:GNOME_LESS(gnome_less_new())];
}

- castGnomeLess:(GnomeLess *) castitem
{
  gnomeless = castitem;
  return [super castGtkVBox:GTK_VBOX(castitem)];
}

- show_file:(const gchar *) path
{
  gnome_less_show_file(gnomeless, path);
  return self;
}

- show_command:(const gchar *) command_line
{
  gnome_less_show_command(gnomeless, command_line);
  return self;
}

- show_string:(const gchar *) s
{
  gnome_less_show_string(gnomeless, s);
  return self;
}

- show_filestream:(FILE *) f
{
  gnome_less_show_filestream(gnomeless, f);
  return self;
}

- fixed_font
{
  gnome_less_fixed_font(gnomeless);
  return self;
}
@end
