#include "obgnomeAbout.h"

@implementation Gnome_About
- initWithAboutInfo:(gchar *) title     /* Name of the application. */
	version:(gchar *) version   /* Version. */
      copyright:(gchar *)copyright  /* Copyright notice (one line.) */
	authors:(gchar **)authors   /* NULL terminated list of authors. */
       comments:(gchar *)comments   /* Other comments. */
	   logo:(gchar *)logo       /* A logo pixmap file. */
{
  
  return [self castGnomeAbout:GNOME_ABOUT(gnome_about_new(title, version, copyright, (const char **)authors, comments, logo))];
}

- castGnomeAbout:(GnomeAbout *) castitem
{
  gnomeabout = castitem;
  return [super castGnomeDialog:GNOME_DIALOG(castitem)];
}

@end  
