#include "obgnomeCanvasText.h"


@implementation Gnome_Canvas_Text : Gnome_Canvas_Item

- castGnomeCanvasText: (GnomeCanvasText *) castitem
{
  gnomecanvastext = castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}

- (GtkType)getItemType
{
  
  return (GtkType) gnome_canvas_text_get_type ();;
}

-  castToGnomeCanvasItem: (id) castitem
{
  gnomecanvastext = (GnomeCanvasText *) castitem;
  return [super castGnomeCanvasItem:GNOME_CANVAS_ITEM(castitem)];
}



@end
