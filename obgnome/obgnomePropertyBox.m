#include "obgnomePropertyBox.h"

@implementation Gnome_PropertyBox
- init 
{
  return [self castGnomePropertyBox:GNOME_PROPERTY_BOX(gnome_property_box_new())];
}

- castGnomePropertyBox:(GnomePropertyBox *) castitem
{
  gnomepropertybox = castitem;
  return [super castGnomeDialog:GNOME_DIALOG(castitem)];
}

- changed
{
  gnome_property_box_changed(gnomepropertybox);
  return self;
}

- append_page:(Gtk_Widget *) child
	     :(Gtk_Widget *) tab_label;
{
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(gnomepropertybox),
                        child->gtkwidget,
                        tab_label->gtkwidget);
  return self;
}
@end  
