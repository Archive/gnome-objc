/* obgnomeCalculator.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeCalculator.h"

@implementation Gnome_Calculator
- init
{
  return [self castGnomeCalculator:GNOME_CALCULATOR(gnome_calculator_new())];
}

- castGnomeCalculator:(GnomeCalculator *) castitem
{
  gnomecalculator = castitem;
  return [super castGtkVBox:GTK_VBOX(castitem)];
}

/* This isn't in libgnomeui? */ /*
- clear
{
  gnome_calculator_clear();
  return self;
}
*/
@end
