/* obgnomeDateEdit.h: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef OBGNOME_DATEEDIT_H
#define OBGNOME_DATEEDIT_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"

@interface Gnome_Date_Edit : Gtk_HBox
{
@public
GnomeDateEdit *gnomedateedit;
}
- initWithDate:(time_t) the_time
              :(gint) show_time
              :(gint) use_24_format;
- castGnomeDateEdit:(GnomeDateEdit *) castitem;
- set_time:(time_t) the_time;
- set_popup_range:(gint) low_hour
                 :(gint) up_hour;
- (time_t) get_date;
@end

#endif /* OBGNOME_DATEEDIT_H */
