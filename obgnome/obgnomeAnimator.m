/* obgnomeAnimator.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#include "obgnomeAnimator.h"

// ****************************************************

@implementation Gnome_Animator : Gtk_Widget


- init
{
  [self error:"Gnome_Animator: attempt to call \"init\" \
method on a class requiring parameters for initialization.."];
  return self;
}


- castGnomeAnimator:(GnomeAnimator *) castitem
{
  
  gnomeanimator = castitem;
  return [super castGtkWidget:GTK_WIDGET(gnomeanimator)];
  
}

- initWithWidth: (guint)w
	 height: (guint)h
{
  return [self castGnomeAnimator:GNOME_ANIMATOR(gnome_animator_new_with_size(w,h))];
  
}

- setLoopType: (GnomeAnimatorLoopType) lt
{
  gnome_animator_set_loop_type( gnomeanimator, lt );
  return self;
}

- (GnomeAnimatorLoopType) getLoopType
{
  return( (GnomeAnimatorLoopType)gnome_animator_get_loop_type(gnomeanimator) );
}

- setPlaybackDirection: (int)d
{
  gnome_animator_set_playback_direction( gnomeanimator, d );
  return self;
}

- (int)getPlaybackDirection
{
  return((int) gnome_animator_get_playback_direction( gnomeanimator ) );
}


- (gboolean)appendFrameFromImlib: (GdkImlibImage *)im
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv
{
  return (gboolean)gnome_animator_append_frame_from_imlib( gnomeanimator, im, xofs, yofs, iv);
}

- (gboolean)appendFrameFromImlib: (GdkImlibImage *)im
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv
			   width: (guint)w
			  height: (guint)h
{
  return (gboolean)gnome_animator_append_frame_from_imlib_at_size( gnomeanimator, im, xofs, yofs, iv, w, h);
}

- (gboolean)appendFrameFromFile: (const gchar *)n
			xOffest: (int)xofs
			yOffset: (int)yofs
		       interval: (guint32)iv
{
  return (gboolean)gnome_animator_append_frame_from_file( gnomeanimator, n, xofs, yofs, iv);
}

- (gboolean)appendFrameFromFile: (const gchar *)n
			xOffest: (int)xofs
			yOffset: (int)yofs
		       interval: (guint32)iv
			  width: (guint)w
			 height: (guint)h
{
  return (gboolean)gnome_animator_append_frame_from_file_at_size( gnomeanimator, n, xofs, yofs, iv, w, h);
}



- (gboolean)appendFramesFromImlib: (GdkImlibImage *)im
			  xOffest: (int)xofs
			  yOffset: (int)yofs
			 interval: (guint32)iv
			    xUnit: (guint)xu
{
  return (gboolean)gnome_animator_append_frames_from_imlib( gnomeanimator, im, xofs, yofs, iv, xu);
}

- (gboolean)appendFramesFromImlib: (GdkImlibImage *)im
			  xOffest: (int)xofs
			  yOffset: (int)yofs
			 interval: (guint32)iv
			    xUnit: (guint)xu
			    width: (guint)w
			   height: (guint)h
{
  return (gboolean)gnome_animator_append_frames_from_imlib_at_size( gnomeanimator, im, xofs, yofs, iv, xu, w, h);
}

- (gboolean)appendFramesFromFile: (const gchar *)n
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv
			   xUnit: (guint)xu
{
  return (gboolean)gnome_animator_append_frames_from_file( gnomeanimator, n, xofs, yofs, iv, xu);
}

- (gboolean)appendFramesFromFile: (const gchar *)n
			 xOffest: (int)xofs
			 yOffset: (int)yofs
			interval: (guint32)iv
			   xUnit: (guint)xu
			   width: (guint)w
			  height: (guint)h
{
  return (gboolean)gnome_animator_append_frames_from_file_at_size( gnomeanimator, n, xofs, yofs, iv, xu, w, h);
}


- (gboolean)appendFrameFromGnomePixmap: (Gnome_Pixmap *) gp
			       xOffest: (int)xofs
			       yOffset: (int)yofs
			      interval: (guint32)iv
{
  return (gboolean)gnome_animator_append_frame_from_gnome_pixmap( gnomeanimator, gp->gnomepixmap, xofs, yofs, iv);
}


- start
{
  gnome_animator_start( gnomeanimator );
  return self;
}

- stop
{
  gnome_animator_stop( gnomeanimator );
  return self;
}

- advance: (gint) num_frame
{
  gnome_animator_advance( gnomeanimator, num_frame );
  return self;
}

- goToFrame: (gint) num_frame
{
  gnome_animator_goto_frame( gnomeanimator, num_frame );
  return self;
}

- (guint)getCurrentFrameNumber
{
  return( gnome_animator_get_current_frame_number(gnomeanimator) );
}

- (GnomeAnimatorStatus) getStatus
{
  return( (GnomeAnimatorStatus)gnome_animator_get_status(gnomeanimator));
}

- setPlaybackSpeed: (gdouble)s
{
  gnome_animator_set_playback_speed( gnomeanimator, s);
  return self;
}
- (gdouble)getPlaybackSpeed
{
  return (gnome_animator_get_playback_speed(gnomeanimator) );
}

@end
