#ifndef OBGNOME_LED_H
#define OBGNOME_LED_H 1
#include "obgtk/obgtk.h"
#include <gtktty/libgtktty.h>

@interface Gtk_LED : Gtk_Misc
{
@public
  GtkLed *gtkled;
}
- castGtkLED:(GtkLed *) castitem;
- set_state_LED:(GtkStateType) widget_state
	       :(gboolean) on_off;
- switch:(gboolean) on_off;
- (gboolean)is_on;
@end
#endif
