/* obgnomeIconList.m: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Michael Hanni <mhanni@sprintmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "obgnomeIconList.h"

@implementation Gnome_Icon_List
- init
{
  return [self castGnomeIconList:GNOME_ICON_LIST(gnome_icon_list_new())];
}

- castGnomeIconList:(GnomeIconList *) castitem
{
  gnomeiconlist = castitem;
  return [super castGtkContainer:GTK_CONTAINER(castitem)];
}

- set_selection_mode:(GtkSelectionMode) mode
{
  gnome_icon_list_set_selection_mode(gnomeiconlist, mode);
  return self;
}

- set_policy:(GtkPolicyType) vscrollbar_policy
	    :(GtkPolicyType) hscrollbar_policy
{
  gnome_icon_list_set_policy(gnomeiconlist, vscrollbar_policy, hscrollbar_policy);
  return self;
}

- append:(gchar *) icon_filename
	:(gchar *) text
{
  gnome_icon_list_append(gnomeiconlist, icon_filename, text);
  return self;
}

