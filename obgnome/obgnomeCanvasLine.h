/* obgnomeCanvasLine.h: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */



#ifndef OBGNOME_CANVAS_LINE_H
#define OBGNOME_CANVAS_LINE_H 1

#include <gnome.h>
#include "obgtk/obgtk.h"
#include <obgnome/obgnomeCanvas.h>
#include <libgnomeui/gnome-canvas-line.h>


// the line item uses points. Look in gtk/gnome-canvas-util.h for 
// definition and use.

@interface Gnome_Canvas_Line : Gnome_Canvas_Item
{
@public
  GnomeCanvasLine *gnomecanvasline;
}
- castGnomeCanvasLine: (GnomeCanvasLine *) castitem;

@end

#endif /* OBGNOME_CANVAS_LINE_H */
