/* obgnomeDEntryEdit: Copyright (C) 1998 Free Software Foundation
 * Objective-C wrappings for GNOME UI routines.
 * Written by: Bertrand Guiheneuf <Bertrand.Guiheneuf@inria.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include "obgnomeDEntryEdit.h"

@implementation Gnome_DEntryEdit

+ newNotebook:(Gtk_Notebook *) notebook
{
  GnomeDEntryEdit *dee;
  Gnome_DEntryEdit *objcDee;
  
  dee = GNOME_DENTRY_EDIT (gnome_dentry_edit_new_notebook(notebook->gtknotebook));
  objcDee = [[Gnome_DEntryEdit alloc] castGnomeDEntryEdit: dee];
  return objcDee;
}

- init
{
  GnomeDEntryEdit *dee;
 
  dee = GNOME_DENTRY_EDIT (gnome_dentry_edit_new ());
  return [self castGnomeDEntryEdit:dee];

}

- castGnomeDEntryEdit:(GnomeDEntryEdit *) castitem
{
  gnomedentryedit = castitem;
  return [super castGtkObject:GTK_OBJECT (castitem)];

}

- clear
{
  gnome_dentry_edit_clear (gnomedentryedit);
  return self;
}

- loadFile:(const gchar *) path
{
  gnome_dentry_edit_load_file (gnomedentryedit, path);
  return self;
}

- setDEntry:(GnomeDesktopEntry *) dentry
{
  gnome_dentry_edit_set_dentry (gnomedentryedit, dentry);
  return self;
}

- (GnomeDesktopEntry *) getDEntry
{
  return gnome_dentry_get_dentry (gnomedentryedit);
}

- (gchar *) getIcon
{
  return gnome_dentry_edit_get_icon (gnomedentryedit);
}

- (gchar *) getName;
{
  return gnome_dentry_edit_get_name (gnomedentryedit);
}

@end

