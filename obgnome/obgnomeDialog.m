#include <stdarg.h>
#include "obgnomeDialog.h"

@implementation Gnome_Dialog
- castGnomeDialog:(GnomeDialog *) castitem
{
  gnomedialog = castitem;
  return [super castGtkWindow:GTK_WINDOW(castitem)];
}

- button_connect:(gint) button 
		:(GtkSignalFunc) callback :(gpointer) data
{
  gnome_dialog_button_connect(gnomedialog, button, callback, data);
  return self;
}

- set_default:(gint) button;
{
  gnome_dialog_set_default(gnomedialog, button);
  return self;
}

- set_sensitive:(gint) button 
	       :(gboolean) setting
{
  gnome_dialog_set_sensitive(gnomedialog, button, setting);
  return self;
}

- set_accelerator:(gint) button 
		 :(const guchar) accelerator_key
		 :(guint8) accelerator_mode
{
  gnome_dialog_set_accelerator (gnomedialog, button, accelerator_key, accelerator_mode);
  return self;
}

- close
{
  gnome_dialog_close (gnomedialog);
  return self;
}

- close_hides:(gboolean) just_hide
{
  gnome_dialog_close_hides (gnomedialog, just_hide); 
  return self;
}

- set_close:(gboolean) click_closes
{
  gnome_dialog_set_close (gnomedialog, click_closes);
  return self;
}

- append_buttons:(const gchar *) first , ...
{
  va_list ap;

  va_start(ap, first);
  gnome_dialog_append_buttons (gnomedialog, first, ap);
  va_end(ap);
  return self;
}
@end  
