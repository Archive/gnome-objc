#ifndef OBGNOME_TTY_H
#define OBGNOME_TTY_H 1
#include "obgtk/obgtk.h"
#include "obgnomeTerm.h"
#include "obgnomeVtEmu.h"
#include "obgnomeLED.h"
#include <gtktty/libgtktty.h>

@interface Gtk_TTY : Gtk_Term
{
@public
  GtkTty *gtktty;
}
- castGtkTTY:(GtkTty *) castitem;
- initWithTTYInfo:(guint) width
		 :(guint) height
		 :(guint) scrollback;
- put_out:(const guchar *) buffer
	 :(guint) count;
- put_in:(const guchar *) buffer
	:(guint) count;
- leds_changed;
- add_update_led:(Gtk_LED *) led
		:(GtkTtyKeyStateBits) mask;
- execute:(const gchar *) prg_name
	 :(gchar **) argv
	 :(gchar **) envp;
- change_emu:(Gtk_VtEmu *) vtemu;
@end

#endif
